package com.example.frontend.Adapter;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.example.frontend.LogicaNegocios.Inventario;
import com.example.frontend.R;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;

public class InventarioAdapter extends RecyclerView.Adapter<InventarioAdapter.MyViewHolder> implements Filterable {
    private List<Inventario> InventarioList;
    private List<Inventario> InventarioListFiltered;
    private InventarioAdapter.InventarioAdapterListener listener;
    private Inventario deletedItem;

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView titulo1, titulo2, description;
        //two layers
        public RelativeLayout viewForeground, viewBackgroundDelete, viewBackgroundEdit;

        public MyViewHolder(View view) {
            super(view);
            titulo1 = view.findViewById(R.id.titleFirstLbl);
            titulo2 = view.findViewById(R.id.titleSecLbl);
            description = view.findViewById(R.id.descriptionLbl);
            viewBackgroundDelete = view.findViewById(R.id.view_background_delete);
            viewBackgroundEdit = view.findViewById(R.id.view_background_edit);
            viewForeground = view.findViewById(R.id.view_foreground);
            view.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    // send selected contact in callback
                    listener.onContactSelected(InventarioListFiltered.get(getAdapterPosition()));
                }
            });
        }
    }

    public InventarioAdapter(List<Inventario> InventarioList, InventarioAdapterListener listener) {
        this.InventarioList = InventarioList;
        this.listener = listener;
        //init filter
        this.InventarioListFiltered = InventarioList;
    }

    @Override
    public InventarioAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_list_row, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(InventarioAdapter.MyViewHolder holder, int position) {
        // rendering view
        final Inventario inventario = InventarioListFiltered.get(position);
        holder.titulo1.setText(inventario.getMarca());
        holder.titulo2.setText(inventario.getModelo());
        holder.description.setText("Número de Serie: " + inventario.getNoSerial());
    }

    @Override
    public int getItemCount() {
        return InventarioListFiltered.size();
    }

    public void removeItem(int position) {
        deletedItem = InventarioListFiltered.remove(position);
        Iterator<Inventario> iter = InventarioList.iterator();
        while (iter.hasNext()) {
            Inventario aux = iter.next();
            if (deletedItem.equals(aux))
                iter.remove();
        }
        // notify item removed
        notifyItemRemoved(position);
    }

    public void restoreItem(int position) {

        if (InventarioListFiltered.size() == InventarioList.size()) {
            InventarioListFiltered.add(position, deletedItem);
        } else {
            InventarioListFiltered.add(position, deletedItem);
            InventarioList.add(deletedItem);
        }
        notifyDataSetChanged();
        // notify item added by position
        notifyItemInserted(position);
    }

    public Inventario getSwipedItem(int index) {
        if (this.InventarioList.size() == this.InventarioListFiltered.size()) { //not filtered yet
            return InventarioList.get(index);
        } else {
            return InventarioListFiltered.get(index);
        }
    }

    public void onItemMove(int fromPosition, int toPosition) {
        if (InventarioList.size() == InventarioListFiltered.size()) { // without filter
            if (fromPosition < toPosition) {
                for (int i = fromPosition; i < toPosition; i++) {
                    Collections.swap(InventarioList, i, i + 1);
                }
            } else {
                for (int i = fromPosition; i > toPosition; i--) {
                    Collections.swap(InventarioList, i, i - 1);
                }
            }
        } else {
            if (fromPosition < toPosition) {
                for (int i = fromPosition; i < toPosition; i++) {
                    Collections.swap(InventarioListFiltered, i, i + 1);
                }
            } else {
                for (int i = fromPosition; i > toPosition; i--) {
                    Collections.swap(InventarioListFiltered, i, i - 1);
                }
            }
        }
        notifyItemMoved(fromPosition, toPosition);
    }

    @Override
    public Filter getFilter() {
        return new Filter() {
            @Override
            protected FilterResults performFiltering(CharSequence charSequence) {
                String charString = charSequence.toString();
                if (charString.isEmpty()) {
                    InventarioListFiltered = InventarioList;
                } else {
                    List<Inventario> filteredList = new ArrayList<>();
                    for (Inventario row : InventarioList) {
                        // filter use two parameters
                        if (row.getNoSerial().toLowerCase().contains(charString.toLowerCase())) {
                            filteredList.add(row);
                        }
                    }

                    InventarioListFiltered = filteredList;
                }

                FilterResults filterResults = new FilterResults();
                filterResults.values = InventarioListFiltered;
                return filterResults;
            }

            @Override
            protected void publishResults(CharSequence charSequence, FilterResults filterResults) {
                InventarioListFiltered = (ArrayList<Inventario>) filterResults.values;
                notifyDataSetChanged();
            }
        };
    }

    public interface InventarioAdapterListener {
        void onContactSelected(Inventario inventario);
    }
}
