package com.example.frontend.Adapter;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.example.frontend.LogicaNegocios.Tipo;
import com.example.frontend.R;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;

public class TipoAdapter extends RecyclerView.Adapter<TipoAdapter.MyViewHolder> implements Filterable {
    private List<Tipo> TipoList;
    private List<Tipo> TipoListFiltered;
    private TipoAdapter.TipoAdapterListener listener;
    private Tipo deletedItem;

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView titulo1, titulo2, description;
        //two layers
        public RelativeLayout viewForeground, viewBackgroundDelete, viewBackgroundEdit;

        public MyViewHolder(View view) {
            super(view);
            titulo1 = view.findViewById(R.id.titleFirstLbl);
            titulo2 = view.findViewById(R.id.titleSecLbl);
            description = view.findViewById(R.id.descriptionLbl);
            viewBackgroundDelete = view.findViewById(R.id.view_background_delete);
            viewBackgroundEdit = view.findViewById(R.id.view_background_edit);
            viewForeground = view.findViewById(R.id.view_foreground);
            view.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    // send selected contact in callback
                    listener.onContactSelected(TipoListFiltered.get(getAdapterPosition()));
                }
            });
        }
    }


    public TipoAdapter(List<Tipo> TipoList, TipoAdapter.TipoAdapterListener listener) {
        this.TipoList = TipoList;
        this.listener = listener;
        //init filter
        this.TipoListFiltered = TipoList;
    }

    @Override
    public TipoAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_list_row, parent, false);

        return new TipoAdapter.MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(TipoAdapter.MyViewHolder holder, int position) {
        // rendering view
        final Tipo tipo = TipoListFiltered.get(position);
        holder.description.setText("Nombre: " + tipo.getNombre());
    }

    @Override
    public int getItemCount() {
        return TipoListFiltered.size();
    }

    public void removeItem(int position) {
        deletedItem = TipoListFiltered.remove(position);
        Iterator<Tipo> iter = TipoList.iterator();
        while (iter.hasNext()) {
            Tipo aux = iter.next();
            if (deletedItem.equals(aux))
                iter.remove();
        }
        // notify item removed
        notifyItemRemoved(position);
    }

    public void restoreItem(int position) {

        if (TipoListFiltered.size() == TipoList.size()) {
            TipoListFiltered.add(position, deletedItem);
        } else {
            TipoListFiltered.add(position, deletedItem);
            TipoList.add(deletedItem);
        }
        notifyDataSetChanged();
        // notify item added by position
        notifyItemInserted(position);
    }

    public Tipo getSwipedItem(int index) {
        if (this.TipoList.size() == this.TipoListFiltered.size()) { //not filtered yet
            return TipoList.get(index);
        } else {
            return TipoListFiltered.get(index);
        }
    }

    public void onItemMove(int fromPosition, int toPosition) {
        if (TipoList.size() == TipoListFiltered.size()) { // without filter
            if (fromPosition < toPosition) {
                for (int i = fromPosition; i < toPosition; i++) {
                    Collections.swap(TipoList, i, i + 1);
                }
            } else {
                for (int i = fromPosition; i > toPosition; i--) {
                    Collections.swap(TipoList, i, i - 1);
                }
            }
        } else {
            if (fromPosition < toPosition) {
                for (int i = fromPosition; i < toPosition; i++) {
                    Collections.swap(TipoListFiltered, i, i + 1);
                }
            } else {
                for (int i = fromPosition; i > toPosition; i--) {
                    Collections.swap(TipoListFiltered, i, i - 1);
                }
            }
        }
        notifyItemMoved(fromPosition, toPosition);
    }

    @Override
    public Filter getFilter() {
        return new Filter() {
            @Override
            protected FilterResults performFiltering(CharSequence charSequence) {
                String charString = charSequence.toString();
                if (charString.isEmpty()) {
                    TipoListFiltered = TipoList;
                } else {
                    List<Tipo> filteredList = new ArrayList<>();
                    for (Tipo row : TipoList) {
                        // filter use two parameters
                        if (row.getNombre().toLowerCase().contains(charString.toLowerCase())) {
                            filteredList.add(row);
                        }
                    }

                    TipoListFiltered = filteredList;
                }

                FilterResults filterResults = new FilterResults();
                filterResults.values = TipoListFiltered;
                return filterResults;
            }

            @Override
            protected void publishResults(CharSequence charSequence, FilterResults filterResults) {
                TipoListFiltered = (ArrayList<Tipo>) filterResults.values;
                notifyDataSetChanged();
            }
        };
    }

    public interface TipoAdapterListener {
        void onContactSelected(Tipo tipo);
    }
}
