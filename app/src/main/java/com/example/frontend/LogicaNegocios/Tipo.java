package com.example.frontend.LogicaNegocios;

import java.io.Serializable;

public class Tipo implements Serializable {
    private String nombre;

    public Tipo(String nombre) {
        this.nombre = nombre;
    }

    public Tipo() {
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    @Override
    public String toString() {
        return "Tipo{" +
                "nombre='" + nombre + '\'' +
                '}';
    }
}
