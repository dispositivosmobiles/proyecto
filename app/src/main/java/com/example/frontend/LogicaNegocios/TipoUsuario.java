package com.example.frontend.LogicaNegocios;


import java.io.Serializable;

public class TipoUsuario  implements Serializable {
    private String nombre;

    public TipoUsuario() {
        this.nombre = "none";
    }

    public TipoUsuario(String nombre) {
        this.nombre = nombre;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    @Override
    public String toString() {
        return "TipoUsuario{" +
                "nombre='" + nombre + '\'' +
                '}';
    }
}
