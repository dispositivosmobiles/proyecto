package com.example.frontend.LogicaNegocios;

import java.io.Serializable;


public class Solicitud implements Serializable {
    private int id;
    private String razonRechazo;
    private String adquisicion;
    private Estado estado;
    private Tipo tipo;

    public Solicitud() {
    }

    public Solicitud(int id, String razonRechazo, String adquisicion, Estado estado, Tipo tipo) {
        this.id = id;
        this.razonRechazo = razonRechazo;
        this.adquisicion = adquisicion;
        this.estado = estado;
        this.tipo = tipo;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getRazonRechazo() {
        return razonRechazo;
    }

    public void setRazonRechazo(String razonRechazo) {
        this.razonRechazo = razonRechazo;
    }

    public String getAdquisicion() {
        return adquisicion;
    }

    public void setAdquisicion(String adquisicion) {
        this.adquisicion = adquisicion;
    }

    public Estado getEstado() {
        return estado;
    }

    public void setEstado(Estado estado) {
        this.estado = estado;
    }

    public Tipo getTipo() {
        return tipo;
    }

    public void setTipo(Tipo tipo) {
        this.tipo = tipo;
    }

    @Override
    public String toString() {
        return "Solicitud{" +
                "id=" + id +
                ", razonRechazo='" + razonRechazo + '\'' +
                ", adquisicion=" + adquisicion +
                ", estado=" + estado +
                ", tipo=" + tipo +
                '}';
    }
}
