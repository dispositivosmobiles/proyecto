package com.example.frontend.LogicaNegocios;

import java.io.Serializable;


/**
 * Created by Luis Carrillo Rodriguez on 26/3/2018.
 */

public class Usuario implements Serializable {
    private String cedula;
    private String nombre,apellido1,apellido2,telefono,email,clave;
    private TipoUsuario tipoUsuario;
    private Sede sede;
    private String nacimiento;


    public String getCedula() {
        return cedula;
    }

    public void setCedula(String cedula) {
        this.cedula = cedula;
    }

    public Usuario(String cedula, String nombre, String apellido1, String apellido2, String telefono, String email, String clave, TipoUsuario tipoUsuario, Sede sede, String nacimiento) {
        this.cedula = cedula;
        this.nombre = nombre;
        this.apellido1 = apellido1;
        this.apellido2 = apellido2;
        this.telefono = telefono;
        this.email = email;
        this.clave = clave;
        this.tipoUsuario = tipoUsuario;
        this.sede = sede;
        this.nacimiento = nacimiento;
    }

    public Usuario() {
        this.email = "none";
        this.apellido1 = "none";
        this.apellido2 = "none";
        this.telefono = "none";
        this.clave = "none";
        this.nombre = "none";
        this.sede = null;
        this.tipoUsuario = null;

    }

    public String getNombre() {
        return nombre;
    }

    public String getApellido1() {
        return apellido1;
    }

    public String getApellido2() {
        return apellido2;
    }

    public String getTelefono() {
        return telefono;
    }

    public String getEmail() {
        return email;
    }

    public String getClave() {
        return clave;
    }

    public TipoUsuario getTipoUsuario() {
        return tipoUsuario;
    }

    public Sede getSede() {
        return sede;
    }

    public String getNacimiento() {
        return nacimiento;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public void setApellido1(String apellido1) {
        this.apellido1 = apellido1;
    }

    public void setApellido2(String apellido2) {
        this.apellido2 = apellido2;
    }

    public void setTelefono(String telefono) {
        this.telefono = telefono;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public void setClave(String clave) {
        this.clave = clave;
    }

    public void setTipoUsuario(TipoUsuario tipoUsuario) {
        this.tipoUsuario = tipoUsuario;
    }

    public void setSede(Sede sede) {
        this.sede = sede;
    }

    public void setNacimiento(String nacimiento) {
        this.nacimiento = nacimiento;
    }

    @Override
    public String toString() {
        return "Usuario{" +
                ", cedula='" + cedula + '\'' +
                ", nombre='" + nombre + '\'' +
                ", apellido1='" + apellido1 + '\'' +
                ", apellido2='" + apellido2 + '\'' +
                ", telefono='" + telefono + '\'' +
                ", email='" + email + '\'' +
                ", clave='" + clave + '\'' +
                ", tipoUsuario=" + tipoUsuario +
                ", sede=" + sede +
                ", nacimiento=" + nacimiento +
                '}';
    }
}
