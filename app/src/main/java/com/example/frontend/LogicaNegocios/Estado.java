package com.example.frontend.LogicaNegocios;

import java.io.Serializable;

public class Estado implements Serializable {
    private String nombre;

    public Estado(String nombre) {
        this.nombre = nombre;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public Estado() {
        this.nombre = "none";
    }

    @Override
    public String toString() {
        return "Estado{" +
                "nombre='" + nombre + '\'' +
                '}';
    }
}
