package com.example.frontend.LogicaNegocios;

import java.io.Serializable;

public class Sede implements Serializable {
    private String nombre;
    private Canton canton;

    public Sede() {
        this.nombre = "none";
        this.canton = null;
    }

    public Sede(String nombre, Canton canton) {
        this.nombre = nombre;
        this.canton = canton;
    }

    public Sede(String nombre) {
        this.nombre = nombre;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public Canton getCanton() {
        return canton;
    }

    public void setCanton(Canton canton) {
        this.canton = canton;
    }

    @Override
    public String toString() {
        return "Sede{" +
                "nombre='" + nombre + '\'' +
                ", canton=" + canton +
                '}';
    }
}
