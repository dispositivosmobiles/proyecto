package com.example.frontend.LogicaNegocios;

import java.io.Serializable;

public class Canton implements Serializable {
    private String nombre;
    private Provincia provincia;

    public Canton() {
        this.nombre = "none";
        this.provincia = null;
    }

    public Canton(String nombre, Provincia provincia) {
        this.nombre = nombre;
        this.provincia = provincia;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public Provincia getProvincia() {
        return provincia;
    }

    public void setProvincia(Provincia provincia) {
        this.provincia = provincia;
    }

    @Override
    public String toString() {
        return "Canton{" +
                "nombre='" + nombre + '\'' +
                ", provincia=" + provincia +
                '}';
    }
}
