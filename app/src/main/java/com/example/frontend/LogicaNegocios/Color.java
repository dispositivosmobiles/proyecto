package com.example.frontend.LogicaNegocios;

import java.io.Serializable;

public class Color implements Serializable {
    private String nombre;

    public Color() {
        this.nombre = "none";
    }

    public Color(String nombre) {
        this.nombre = nombre;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    @Override
    public String toString() {
        return "Color{" +
                "nombre='" + nombre + '\'' +
                '}';
    }
}
