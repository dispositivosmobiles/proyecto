package com.example.frontend.LogicaNegocios;

import java.io.Serializable;

public class Provincia implements Serializable {
    private String nombre;

    public Provincia() {
        this.nombre = "none";
    }

    public Provincia(String nombre) {
        this.nombre = nombre;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    @Override
    public String toString() {
        return "Provincia{" +
                "nombre='" + nombre + '\'' +
                '}';
    }
}
