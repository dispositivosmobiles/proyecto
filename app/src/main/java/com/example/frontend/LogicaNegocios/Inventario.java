package com.example.frontend.LogicaNegocios;

import java.io.Serializable;

public class Inventario implements Serializable {
    private int id,Solicitud;
    private Sede sede;
    private Tipo tipo;
    private Color color;
    private String marca,modelo,NoSerial;
    private boolean approved;

    public Inventario() {
    }

    public Inventario(int id, int solicitud, Sede sede, Tipo tipo, Color color, String marca, String modelo, String noSerial, boolean approved) {
        this.id = id;
        Solicitud = solicitud;
        this.sede = sede;
        this.tipo = tipo;
        this.color = color;
        this.marca = marca;
        this.modelo = modelo;
        NoSerial = noSerial;
        this.approved = approved;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getSolicitud() {
        return Solicitud;
    }

    public void setSolicitud(int solicitud) {
        Solicitud = solicitud;
    }

    public Sede getSede() {
        return sede;
    }

    public void setSede(Sede sede) {
        this.sede = sede;
    }

    public Tipo getTipo() {
        return tipo;
    }

    public void setTipo(Tipo tipo) {
        this.tipo = tipo;
    }

    public Color getColor() {
        return color;
    }

    public void setColor(Color color) {
        this.color = color;
    }

    public String getMarca() {
        return marca;
    }

    public void setMarca(String marca) {
        this.marca = marca;
    }

    public String getModelo() {
        return modelo;
    }

    public void setModelo(String modelo) {
        this.modelo = modelo;
    }

    public String getNoSerial() {
        return NoSerial;
    }

    public void setNoSerial(String noSerial) {
        NoSerial = noSerial;
    }

    public boolean isApproved() {
        return approved;
    }

    public void setApproved(boolean approved) {
        this.approved = approved;
    }

    @Override
    public String toString() {
        return "Inventario{" +
                "id=" + id +
                ", Solicitud=" + Solicitud +
                ", sede=" + sede +
                ", tipo=" + tipo +
                ", color=" + color +
                ", marca='" + marca + '\'' +
                ", modelo='" + modelo + '\'' +
                ", NoSerial='" + NoSerial + '\'' +
                ", approved=" + approved +
                '}';
    }
}
