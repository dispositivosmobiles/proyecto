package com.example.frontend.Activity;

import android.app.SearchManager;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.support.v7.widget.Toolbar;
import android.support.v7.widget.helper.ItemTouchHelper;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Toast;

import com.example.frontend.Adapter.TipoAdapter;
import com.example.frontend.Helper.RecyclerItemTouchHelper;
import com.example.frontend.LogicaNegocios.Tipo;
import com.example.frontend.ModelData.ModelData;
import com.example.frontend.R;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

public class AdmTipoActivity extends AppCompatActivity implements RecyclerItemTouchHelper.RecyclerItemTouchHelperListener, TipoAdapter.TipoAdapterListener {
    private RecyclerView mRecyclerView;
    private TipoAdapter mAdapter;
    private List<Tipo> tipoList;
    private CoordinatorLayout coordinatorLayout;
    private SearchView searchView;
    private FloatingActionButton fab;
    private ModelData model;

    //private String apiUrl = "http://192.168.100.167:8080/WebService";
    private String apiUrl = "http://192.168.1.129:8080/WebService";

    private String tempUrl,selectedUrl,parametersUrl;
    int deletedIndex;
    static public String CREATE="/createTipo",READ="/readTipo",DELETE="/deleteTipo",READ_ALL="/readAllTipo";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_adm_tipo);
        Toolbar toolbar = findViewById(R.id.toolbarP);
        setSupportActionBar(toolbar);

        //toolbar fancy stuff
        getSupportActionBar().setTitle(getString(R.string.MyTipo));

        mRecyclerView = findViewById(R.id.recycler_tipoFld);
        tipoList = new ArrayList<>();
        model = new ModelData();
        /************************************DATABASE CALL****/
        selectedUrl=READ_ALL;
        parametersUrl="";
        MyAsyncTasks myAsyncTasks = new MyAsyncTasks();
        myAsyncTasks.execute();
        /*****************************************************/
        tipoList = model.getTipos();
        mAdapter = new TipoAdapter(tipoList, this);
        coordinatorLayout = findViewById(R.id.coordinator_layoutP);

        // white background notification bar
        whiteNotificationBar(mRecyclerView);

        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getApplicationContext());
        mRecyclerView.setLayoutManager(mLayoutManager);
        mRecyclerView.setItemAnimator(new DefaultItemAnimator());
        mRecyclerView.addItemDecoration(new DividerItemDecoration(this, DividerItemDecoration.VERTICAL));
        mRecyclerView.setAdapter(mAdapter);

        // go to update or add career
        fab = findViewById(R.id.addBtnP);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                goToAddUpdCuenta();
            }
        });

        //delete swiping left and right
        ItemTouchHelper.SimpleCallback itemTouchHelperCallback = new RecyclerItemTouchHelper(0, ItemTouchHelper.LEFT | ItemTouchHelper.RIGHT, this);
        new ItemTouchHelper(itemTouchHelperCallback).attachToRecyclerView(mRecyclerView);

        // Receive the User sent by AddUpdUsuarioActivity
        checkIntentInformation();

        //refresh view
        mAdapter.notifyDataSetChanged();
    }

    @Override
    public void onSwiped(RecyclerView.ViewHolder viewHolder, int direction, int position) {
        if (direction == ItemTouchHelper.START) {
            if (viewHolder instanceof TipoAdapter.MyViewHolder) {
                // get the removed item name to display it in snack bar
                Tipo tmpTipo = tipoList.get(viewHolder.getAdapterPosition());

                // save the index deleted
                final int deletedIndex = viewHolder.getAdapterPosition();
                /***********************************DATABASE CALL****/
                parametersUrl="?nombre="+tmpTipo.getNombre();
                selectedUrl=DELETE;
                MyAsyncTasks myAsyncTasks = new MyAsyncTasks();
                myAsyncTasks.execute();
                /*****************************************************/
                // remove the item from recyclerView
                mAdapter.removeItem(viewHolder.getAdapterPosition());

                // showing snack bar with Undo option
                Snackbar snackbar = Snackbar.make(coordinatorLayout, tmpTipo.getNombre() + " removido!", Snackbar.LENGTH_LONG);
                snackbar.setAction("UNDO", new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        // undo is selected, restore the deleted item from adapter
                        mAdapter.restoreItem(deletedIndex);
                    }
                });
                snackbar.setActionTextColor(android.graphics.Color.YELLOW);
                snackbar.show();
            }
        } else {
            //If is editing a row object
            Tipo aux = mAdapter.getSwipedItem(viewHolder.getAdapterPosition());
            //send data to Edit Activity
            Intent intent = new Intent(this, AddUpdTipoActivity.class);
            intent.putExtra("editable", true);
            intent.putExtra("tipo", aux);
            mAdapter.notifyDataSetChanged(); //restart left swipe view
            startActivity(intent);
        }
    }

    @Override
    public void onItemMove(int source, int target) {
        mAdapter.onItemMove(source, target);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds profesorList to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_search, menu);

        // Associate searchable configuration with the SearchView   !IMPORTANT
        SearchManager searchManager = (SearchManager) getSystemService(Context.SEARCH_SERVICE);
        searchView = (SearchView) menu.findItem(R.id.action_search)
                .getActionView();
        searchView.setSearchableInfo(searchManager
                .getSearchableInfo(getComponentName()));
        searchView.setMaxWidth(Integer.MAX_VALUE);

        // listening to search query text change, every type on input
        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                // filter recycler view when query submitted
                mAdapter.getFilter().filter(query);
                return false;
            }

            @Override
            public boolean onQueryTextChange(String query) {
                // filter recycler view when text is changed
                mAdapter.getFilter().filter(query);
                return false;
            }
        });
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_search) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        if (!searchView.isIconified()) {
            searchView.setIconified(true);
            return;
        }
        Intent a = new Intent(this, MainActivity.class);
        a.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(a);
        super.onBackPressed();
    }

    private void whiteNotificationBar(View view) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            int flags = view.getSystemUiVisibility();
            flags |= View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR;
            view.setSystemUiVisibility(flags);
            getWindow().setStatusBarColor(android.graphics.Color.WHITE);
        }
    }

    @Override
    public void onContactSelected(Tipo tip) { //TODO get the select item of recycleView
        Toast.makeText(getApplicationContext(), "Selected: " + tip.getNombre(), Toast.LENGTH_LONG).show();
    }

    private void checkIntentInformation() {
        Bundle extras = getIntent().getExtras();
        if (extras != null) {
            Tipo aux;
            aux = (Tipo) getIntent().getSerializableExtra("addTipo");
            if (aux == null) {
                Toast.makeText(getApplicationContext(), "No se puede editar", Toast.LENGTH_LONG).show();
            } else {
                //found a new Profesor Object
                tipoList.add(aux);
                /*********************************DATABASE CALL****/
                parametersUrl="?nombre="+aux.getNombre();
                selectedUrl=CREATE;
                MyAsyncTasks SmyAsyncTasks = new MyAsyncTasks();
                SmyAsyncTasks.execute();
                /*****************************************************/
                Toast.makeText(getApplicationContext(), aux.getNombre() + " agregado correctamente", Toast.LENGTH_LONG).show();
            }
        }
    }

    private void goToAddUpdCuenta() {
        Intent intent = new Intent(this, AddUpdTipoActivity.class);
        intent.putExtra("editable", false);
        startActivity(intent);
    }

    public class MyAsyncTasks extends AsyncTask<String, String, String> {


        @Override
        protected void onPreExecute() {
            super.onPreExecute();

        }

        @Override
        protected String doInBackground(String... params) {

            // implement API in background and store the response in current variable
            String current = "";
            tempUrl=apiUrl+selectedUrl+parametersUrl;
            try {
                URL url;
                HttpURLConnection urlConnection = null;
                try {
                    url = new URL(tempUrl);

                    urlConnection = (HttpURLConnection) url
                            .openConnection();

                    InputStream in = urlConnection.getInputStream();

                    InputStreamReader isw = new InputStreamReader(in);

                    int data = isw.read();
                    while (data != -1) {
                        current += (char) data;
                        data = isw.read();
                    }
                    Log.w("", current);

                    return current;

                } catch (Exception e) {
                    e.printStackTrace();
                } finally {
                    if (urlConnection != null) {
                        urlConnection.disconnect();
                    }
                }

            } catch (Exception e) {
                e.printStackTrace();
                return "Exception: " + e.getMessage();
            }
            return current;
        }

        @Override
        protected void onPostExecute(String s) {

            try {
                JSONObject obj = new JSONObject(s);

                AlertDialog alertDialog = new AlertDialog.Builder(AdmTipoActivity.this).create();
                alertDialog.setTitle("Alert");
                //alertDialog.setMessage(obj.toString());
                boolean estado;

                estado = obj.getBoolean("estado");
                if (estado){
                    if (selectedUrl== AdmTipoActivity.READ_ALL){
                        JSONArray objList = obj.getJSONArray("listaDeResultados");
                        int length = objList.length();
                        for (int i=0;i<length;i++){
                            Tipo tmp = new Tipo();
                            tmp.setNombre(objList.getJSONObject(i).getString("nombre"));
                            model.getTipos().add(tmp);
                        }

                        AdmTipoActivity.this.mAdapter.notifyDataSetChanged();
                        //alertDialog.setMessage(obj.getString("mensaje")+". "+obj.getString("mensajeInterno"));
                    }
                    else{
                        AdmTipoActivity.this.mAdapter.notifyDataSetChanged();
                        alertDialog.setMessage(obj.getString("mensaje")+". "+obj.getString("mensajeInterno"));
                        alertDialog.setButton(AlertDialog.BUTTON_NEUTRAL, "OK",
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int which) {
                                        dialog.dismiss();
                                    }
                                });
                        alertDialog.show();
                    }

                }
                else
                {
                    alertDialog.setMessage(obj.getString("mensaje")+". "+obj.getString("mensajeInterno"));
                    alertDialog.setButton(AlertDialog.BUTTON_NEUTRAL, "OK",
                            new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int which) {
                                    dialog.dismiss();
                                }
                            });
                    alertDialog.show();
                }
                //alertDialog.setMessage(obj.getJSONArray("listaDeResultados").length()+"");
                //for (int i=0;i<)


            }
            catch (Exception ex){
                Toast.makeText(getApplicationContext(), ex.toString(), Toast.LENGTH_LONG).show();
            }
        }
    }
}
