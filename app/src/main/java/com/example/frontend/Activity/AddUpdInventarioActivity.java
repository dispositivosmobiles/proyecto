package com.example.frontend.Activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import com.example.frontend.LogicaNegocios.Color;
import com.example.frontend.LogicaNegocios.Inventario;
import com.example.frontend.LogicaNegocios.Sede;
import com.example.frontend.LogicaNegocios.Tipo;
import com.example.frontend.R;

import java.util.ArrayList;
import java.util.List;

public class AddUpdInventarioActivity extends AppCompatActivity {
    private FloatingActionButton fBtn;
    private boolean editable = true;
    private EditText modeloFld;
    private EditText marcaFld;
    private EditText noserieFld;
    private EditText colorFld;
    private EditText solicitudFld;
    private EditText tipoFld;
    private EditText sedeFld;
    private Spinner spinner;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_upd_inventario);
        editable = true;

        // button check
        fBtn = findViewById(R.id.addUpdInventarioBtn);

        //cleaning stuff
        modeloFld = findViewById(R.id.modeloAddUpdInventario);
        marcaFld = findViewById(R.id.marcaAddUpdInventario);
        noserieFld = findViewById(R.id.noserialAddUpdInventario);
        colorFld = findViewById(R.id.colorAddUpdInventario);
        solicitudFld = findViewById(R.id.idSolicitudAddUpdInventario);
        tipoFld = findViewById(R.id.tipoAddUpdInventario);
        sedeFld = findViewById(R.id.sedeAddUpdInventario);

        //spinner
        initSpinner();

        //receiving data from admProfesorActivity
        Bundle extras = getIntent().getExtras();
        if (extras != null) {

            editable = extras.getBoolean("editable");
            if (editable) {   // is editing some row
                Inventario aux = (Inventario) getIntent().getSerializableExtra("inventario");
                marcaFld.setText(aux.getMarca());
                modeloFld.setText(aux.getModelo());
                noserieFld.setText(aux.getNoSerial());
                colorFld.setText(aux.getColor().getNombre());
                solicitudFld.setText(String.valueOf(aux.getSolicitud()));
                tipoFld.setText(aux.getTipo().getNombre());
                sedeFld.setText(aux.getSede().getNombre());
                //edit action
                fBtn.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        editInventario();
                    }
                });
            } else {         // is adding new Carrera object
                //add new action
                fBtn.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        addInventario();
                    }
                });
            }
        }
    }

    public void initSpinner() {
        spinner = (Spinner) findViewById(R.id.privilegioSpinnerFld);
        List<String> list = new ArrayList<String>();
        list.add("administrador");
        list.add("matriculador");
        list.add("ninguno");
        ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(this,
                android.R.layout.simple_spinner_item, list);
        dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner.setAdapter(dataAdapter);
    }

    public void addInventario() {
        if (validateForm()) {
            //do something
            Inventario invent = new Inventario();
            invent.setModelo(modeloFld.getText().toString());
            invent.setSede(new Sede(sedeFld.getText().toString()));
            invent.setNoSerial(noserieFld.getText().toString());
            invent.setMarca(marcaFld.getText().toString());
            invent.setSolicitud(Integer.parseInt(solicitudFld.getText().toString()));
            invent.setColor(new Color(colorFld.getText().toString()));
            invent.setTipo(new Tipo(tipoFld.getText().toString()));
            invent.setApproved(false);

            Intent intent = new Intent(getBaseContext(), AdmInventarioActivity.class);
            //sending Usuario data
            intent.putExtra("addInventario", invent);
            startActivity(intent);
            finish(); //prevent go back
        }
    }

    public void editInventario() {
        if (validateForm()) {
            Inventario invent = new Inventario();
            invent.setModelo(modeloFld.getText().toString());
            invent.setSede(new Sede(sedeFld.getText().toString()));
            invent.setNoSerial(noserieFld.getText().toString());
            invent.setMarca(marcaFld.getText().toString());
            invent.setSolicitud(Integer.parseInt(solicitudFld.getText().toString()));
            invent.setColor(new Color(colorFld.getText().toString()));
            invent.setTipo(new Tipo(tipoFld.getText().toString()));
            invent.setApproved(false);

            Intent intent = new Intent(getBaseContext(), AdmInventarioActivity.class);
            //sending User data
            intent.putExtra("editInventario", invent);
            startActivity(intent);
            finish(); //prevent go back
        }
    }

    public boolean validateForm() {
        int error = 0;
        if (TextUtils.isEmpty(this.noserieFld.getText())) {
            noserieFld.setError("Se requiere número de serie");
            error++;
        }
        if (TextUtils.isEmpty(this.tipoFld.getText())) {
            tipoFld.setError("Tipo Requerida");
            error++;
        }
        if (TextUtils.isEmpty(this.marcaFld.getText())) {
            marcaFld.setError("Marca Requerida");
            error++;
        }
        if(TextUtils.isEmpty(this.colorFld.getText())){
            colorFld.setError("Color Requerido");
            error++;
        }
        if(TextUtils.isEmpty(this.modeloFld.getText())){
            modeloFld.setError("Modelo requerido");
            error++;
        }
        if(TextUtils.isEmpty(this.solicitudFld.getText())){
            solicitudFld.setError("Solicitud Requerida");
        }
        if (spinner.getSelectedItem().toString().equals("")) {
            error++;
        }
        if (error > 0) {
            Toast.makeText(getApplicationContext(), "Algunos errores", Toast.LENGTH_LONG).show();
            return false;
        }
        return true;
    }
}
