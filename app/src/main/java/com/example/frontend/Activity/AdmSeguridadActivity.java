package com.example.frontend.Activity;

import android.app.SearchManager;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.icu.util.Calendar;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.support.v7.widget.Toolbar;
import android.support.v7.widget.helper.ItemTouchHelper;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Toast;

import com.example.frontend.LogicaNegocios.Sede;
import com.example.frontend.LogicaNegocios.TipoUsuario;
import com.example.frontend.R;
import com.example.frontend.ModelData.ModelData;
import com.example.frontend.Adapter.SeguridadAdapter;
import com.example.frontend.Helper.RecyclerItemTouchHelper;
import com.example.frontend.LogicaNegocios.Usuario;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Serializable;
import java.net.HttpURLConnection;
import java.net.URL;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

public class AdmSeguridadActivity extends AppCompatActivity implements RecyclerItemTouchHelper.RecyclerItemTouchHelperListener, SeguridadAdapter.SeguridadAdapterListener {

    private RecyclerView mRecyclerView;
    private SeguridadAdapter mAdapter;
    private List<Usuario> usuarioList;
    private List<TipoUsuario> tipoUsuarioList;
    private List<Sede> sedeList;
    private CoordinatorLayout coordinatorLayout;
    private SearchView searchView;
    private FloatingActionButton fab;
    private ModelData model;

    //private String apiUrl = "http://192.168.100.167:8080/WebService";
    private String apiUrl = "http://192.168.1.129:8080/WebService";

    private String tempUrl,selectedUrl,selectedUrl2,parametersUrl;
    int deletedIndex;
    static public String CREATE="/createUsuario",READ="/readUsuario",UPDATE="/updateUsuario",DELETE="/deleteUsuario",READ_ALL="/readAllUsuario",readAllTipoUsuario="/readAllTipoUsuario",readAllSede="/readAllSede";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_adm_seguridad);
        Toolbar toolbar = findViewById(R.id.toolbarP);
        setSupportActionBar(toolbar);

        //toolbar fancy stuff
        getSupportActionBar().setTitle(getString(R.string.my_seguridad));

        mRecyclerView = findViewById(R.id.recycler_seguridadFld);
        usuarioList = new ArrayList<>();
        sedeList = new ArrayList<>();
        tipoUsuarioList= new ArrayList<>();
        model = new ModelData();


        usuarioList = model.getUsers();
        mAdapter = new SeguridadAdapter(usuarioList, this);
        coordinatorLayout = findViewById(R.id.coordinator_layoutP);

        // white background notification bar
        whiteNotificationBar(mRecyclerView);

        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getApplicationContext());
        mRecyclerView.setLayoutManager(mLayoutManager);
        mRecyclerView.setItemAnimator(new DefaultItemAnimator());
        mRecyclerView.addItemDecoration(new DividerItemDecoration(this, DividerItemDecoration.VERTICAL));
        mRecyclerView.setAdapter(mAdapter);

        // go to update or add career
        fab = findViewById(R.id.addBtnP);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                goToAddUpdCuenta();
            }
        });

        //delete swiping left and right
        ItemTouchHelper.SimpleCallback itemTouchHelperCallback = new RecyclerItemTouchHelper(0, ItemTouchHelper.LEFT | ItemTouchHelper.RIGHT, this);
        new ItemTouchHelper(itemTouchHelperCallback).attachToRecyclerView(mRecyclerView);

        // Receive the User sent by AddUpdUsuarioActivity
        checkIntentInformation();

        //refresh view
        mAdapter.notifyDataSetChanged();
    }

    @Override
    public void onSwiped(RecyclerView.ViewHolder viewHolder, int direction, int position) {
        if (direction == ItemTouchHelper.START) {
            if (viewHolder instanceof SeguridadAdapter.MyViewHolder) {
                // get the removed item name to display it in snack bar
                Usuario tmpUsuario = usuarioList.get(viewHolder.getAdapterPosition());

                // save the index deleted
                final int deletedIndex = viewHolder.getAdapterPosition();
                /***********************************DATABASE CALL****/
                parametersUrl="?cedula="+tmpUsuario.getCedula();
                selectedUrl=DELETE;
                MyAsyncTasks myAsyncTasks = new MyAsyncTasks();
                myAsyncTasks.execute();
                /*****************************************************/
                // remove the item from recyclerView
                mAdapter.removeItem(viewHolder.getAdapterPosition());

                // showing snack bar with Undo option
                Snackbar snackbar = Snackbar.make(coordinatorLayout, tmpUsuario.getCedula() + " removido!", Snackbar.LENGTH_LONG);
                snackbar.setAction("UNDO", new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        // undo is selected, restore the deleted item from adapter
                        mAdapter.restoreItem(deletedIndex);
                    }
                });
                snackbar.setActionTextColor(Color.YELLOW);
                snackbar.show();
            }
        } else {
            //If is editing a row object
            Usuario aux = mAdapter.getSwipedItem(viewHolder.getAdapterPosition());
            //send data to Edit Activity
            Intent intent = new Intent(this, AddUpdSeguridadActivity.class);
            intent.putExtra("editable", true);
            intent.putExtra("usuario", aux);
            intent.putExtra("tipoUsuarioList", (Serializable) this.tipoUsuarioList);
            intent.putExtra("sedeList", (Serializable) this.sedeList);
            mAdapter.notifyDataSetChanged(); //restart left swipe view
            startActivity(intent);
        }
    }

    @Override
    public void onItemMove(int source, int target) {
        mAdapter.onItemMove(source, target);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds profesorList to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_search, menu);

        // Associate searchable configuration with the SearchView   !IMPORTANT
        SearchManager searchManager = (SearchManager) getSystemService(Context.SEARCH_SERVICE);
        searchView = (SearchView) menu.findItem(R.id.action_search)
                .getActionView();
        searchView.setSearchableInfo(searchManager
                .getSearchableInfo(getComponentName()));
        searchView.setMaxWidth(Integer.MAX_VALUE);

        // listening to search query text change, every type on input
        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                // filter recycler view when query submitted
                mAdapter.getFilter().filter(query);
                return false;
            }

            @Override
            public boolean onQueryTextChange(String query) {
                // filter recycler view when text is changed
                mAdapter.getFilter().filter(query);
                return false;
            }
        });
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_search) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        if (!searchView.isIconified()) {
            searchView.setIconified(true);
            return;
        }
        Intent a = new Intent(this, MainActivity.class);
        a.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(a);
        super.onBackPressed();
    }

    private void whiteNotificationBar(View view) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            int flags = view.getSystemUiVisibility();
            flags |= View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR;
            view.setSystemUiVisibility(flags);
            getWindow().setStatusBarColor(Color.WHITE);
        }
    }

    @Override
    public void onContactSelected(Usuario user) { //TODO get the select item of recycleView
        Toast.makeText(getApplicationContext(), "Selected: " + user.getEmail() + ", " + user.getTipoUsuario().getNombre(), Toast.LENGTH_LONG).show();
    }

    private void checkIntentInformation() {
        /************************************DATABASE CALL****/
        selectedUrl2=readAllTipoUsuario;
        MyAsyncTasks2 myAsyncTasks2 = new MyAsyncTasks2();
        myAsyncTasks2.execute();
        /*****************************************************/
        /************************************DATABASE CALL****/
        selectedUrl2=readAllTipoUsuario;
        MyAsyncTasks3 myAsyncTasks3 = new MyAsyncTasks3();
        myAsyncTasks3.execute();
        /*****************************************************/
        /************************************DATABASE CALL****/
        selectedUrl=READ_ALL;
        parametersUrl="";
        MyAsyncTasks myAsyncTasks = new MyAsyncTasks();
        myAsyncTasks.execute();
        /*****************************************************/

        Bundle extras = getIntent().getExtras();
        if (extras != null) {
            Usuario aux;
            aux = (Usuario) getIntent().getSerializableExtra("addUsuario");
            if (aux == null) {
                aux = (Usuario) getIntent().getSerializableExtra("editUsuario");
                if (aux != null) {
                    //found an item that can be updated
                    boolean founded = false;
                    model.getUsers().add(aux);
                    /*********************************DATABASE CALL****/
                    parametersUrl="?cedula="+aux.getCedula()
                            +"&nombre="+aux.getNombre()
                            +"&apellido1="+aux.getApellido1()
                            +"&apellido2="+aux.getApellido2()
                            +"&clave="+aux.getClave()
                            +"&email="+aux.getEmail()
                            +"&telefono="+aux.getTelefono()
                            +"&sede="+aux.getSede()
                            +"&TipoUsuario="+aux.getTipoUsuario().getNombre()
                            +"&nacimiento="+aux.getClave();
                    selectedUrl=UPDATE;
                    MyAsyncTasks SmyAsyncTasks = new MyAsyncTasks();
                    SmyAsyncTasks.execute();
                    /*****************************************************/
                    /*
                    for (Usuario usuario : usuarioList) {
                        if (usuario.getEmail().equals(aux.getEmail())) {
                            usuario.setTipoUsuario(aux.getTipoUsuario());
                            usuario.setClave(aux.getClave());
                            founded = true;
                            break;
                        }
                    }*/
                    //check if exist
                    if (founded) {
                        Toast.makeText(getApplicationContext(), aux.getEmail() + " editado correctamente", Toast.LENGTH_LONG).show();
                    } else {
                        Toast.makeText(getApplicationContext(), aux.getEmail() + " no encontrado", Toast.LENGTH_LONG).show();
                    }
                }
            } else {
                //found a new Profesor Object
                usuarioList.add(aux);
                /*********************************DATABASE CALL****/
                parametersUrl="?cedula="+aux.getCedula()
                        +"&nombre="+aux.getNombre()
                        +"&apellido1="+aux.getApellido1()
                        +"&apellido2="+aux.getApellido2()
                        +"&clave="+aux.getClave()
                        +"&email="+aux.getEmail()
                        +"&telefono="+aux.getTelefono()
                        +"&sede="+aux.getSede()
                        +"&TipoUsuario="+aux.getTipoUsuario().getNombre()
                        +"&nacimiento="+aux.getClave();
                selectedUrl=CREATE;
                MyAsyncTasks SmyAsyncTasks = new MyAsyncTasks();
                SmyAsyncTasks.execute();
                /*****************************************************/
                Toast.makeText(getApplicationContext(), aux.getEmail() + " agregado correctamente", Toast.LENGTH_LONG).show();
            }
        }
    }

    private void goToAddUpdCuenta() {
        Intent intent = new Intent(this, AddUpdSeguridadActivity.class);
        intent.putExtra("editable", false);
        intent.putExtra("tipoUsuarioList", (Serializable) this.tipoUsuarioList);
        intent.putExtra("sedeList", (Serializable) this.sedeList);
        startActivity(intent);
    }
    public class MyAsyncTasks extends AsyncTask<String, String, String> {


        @Override
        protected void onPreExecute() {
            super.onPreExecute();

        }

        @Override
        protected String doInBackground(String... params) {

            // implement API in background and store the response in current variable
            String current = "";
            tempUrl=apiUrl+selectedUrl+parametersUrl;
            try {
                URL url;
                HttpURLConnection urlConnection = null;
                try {
                    url = new URL(tempUrl);

                    urlConnection = (HttpURLConnection) url
                            .openConnection();

                    InputStream in = urlConnection.getInputStream();

                    InputStreamReader isw = new InputStreamReader(in);

                    int data = isw.read();
                    while (data != -1) {
                        current += (char) data;
                        data = isw.read();
                    }
                    Log.w("", current);

                    return current;

                } catch (Exception e) {
                    e.printStackTrace();
                } finally {
                    if (urlConnection != null) {
                        urlConnection.disconnect();
                    }
                }

            } catch (Exception e) {
                e.printStackTrace();
                return "Exception: " + e.getMessage();
            }
            return current;
        }

        @Override
        protected void onPostExecute(String s) {

            try {
                JSONObject obj = new JSONObject(s);

                AlertDialog alertDialog = new AlertDialog.Builder(AdmSeguridadActivity.this).create();
                alertDialog.setTitle("Alert");
                //alertDialog.setMessage(obj.toString());
                boolean estado;

                estado = obj.getBoolean("estado");
                if (estado){
                    String yyyy,mm,dd;
                    if (selectedUrl==AdmSeguridadActivity.readAllTipoUsuario){
                        JSONArray objList = obj.getJSONArray("listaDeResultados");
                        int length = objList.length();
                        for (int i=0;i<length;i++){
                            TipoUsuario tmp= new TipoUsuario();
                            tmp.setNombre(objList.getJSONObject(i).getString("nombre"));
                            model.getTipoUsuarios().add(tmp);
                        }

                    }
                    else if (selectedUrl==AdmSeguridadActivity.READ_ALL){
                        JSONArray objList = obj.getJSONArray("listaDeResultados");
                        int length = objList.length();
                        for (int i=0;i<length;i++){
                            yyyy = objList.getJSONObject(i).getJSONObject("nacimiento").getString("year");
                            mm = objList.getJSONObject(i).getJSONObject("nacimiento").getString("month");
                            dd = objList.getJSONObject(i).getJSONObject("nacimiento").getString("day");
                            if (mm.length()==1){
                                mm='0'+mm;
                            }
                            if (dd.length()==1){
                                dd='0'+dd;
                            }
                            Usuario tmp = new Usuario();
                            tmp.setCedula(objList.getJSONObject(i).getString("cedula"));
                            tmp.setTipoUsuario(new TipoUsuario(objList.getJSONObject(i).getJSONObject("tipoUsuario").getString("nombre")));
                            tmp.setEmail(objList.getJSONObject(i).getString("email"));
                            //tmp.setClave(objList.getJSONObject(i).getString("clave"));
                            tmp.setApellido1(objList.getJSONObject(i).getString("apellido1"));
                            tmp.setApellido2(objList.getJSONObject(i).getString("apellido2"));
                            tmp.setNacimiento(dd+"/"+mm+"/"+yyyy);
                            tmp.setNombre(objList.getJSONObject(i).getString("nombre"));
                            tmp.setSede(new Sede(objList.getJSONObject(i).getJSONObject("sede").getString("nombre")));
                            tmp.setTelefono(objList.getJSONObject(i).getString("telefono"));
                            model.getUsers().add(tmp);
                        }

                        AdmSeguridadActivity.this.mAdapter.notifyDataSetChanged();
                        //alertDialog.setMessage(obj.getString("mensaje")+". "+obj.getString("mensajeInterno"));
                    }
                    else{
                        AdmSeguridadActivity.this.mAdapter.notifyDataSetChanged();
                        alertDialog.setMessage(obj.getString("mensaje")+". "+obj.getString("mensajeInterno"));
                        alertDialog.setButton(AlertDialog.BUTTON_NEUTRAL, "OK",
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int which) {
                                        dialog.dismiss();
                                    }
                                });
                        alertDialog.show();
                    }

                }
                else
                {
                    alertDialog.setMessage(obj.getString("mensaje")+". "+obj.getString("mensajeInterno"));
                    alertDialog.setButton(AlertDialog.BUTTON_NEUTRAL, "OK",
                            new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int which) {
                                    dialog.dismiss();
                                }
                            });
                    alertDialog.show();
                }
                //alertDialog.setMessage(obj.getJSONArray("listaDeResultados").length()+"");
                //for (int i=0;i<)


            }
            catch (Exception ex){
                //Toast.makeText(getApplicationContext(), ex.toString(), Toast.LENGTH_LONG).show();
                AlertDialog alertDialog = new AlertDialog.Builder(AdmSeguridadActivity.this).create();
                alertDialog.setTitle("Alert");
                alertDialog.setMessage(ex.toString()+" "+ex.getMessage()+" "+ex.getLocalizedMessage());
                alertDialog.setButton(AlertDialog.BUTTON_NEUTRAL, "OK",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.dismiss();
                            }
                        });
                alertDialog.show();
            }
        }
    }



    public class MyAsyncTasks2 extends AsyncTask<String, String, String> {


        @Override
        protected void onPreExecute() {
            super.onPreExecute();

        }

        @Override
        protected String doInBackground(String... params) {

            // implement API in background and store the response in current variable
            String current = "";

            try {
                URL url;
                HttpURLConnection urlConnection = null;
                try {
                    url = new URL(apiUrl+AdmSeguridadActivity.readAllTipoUsuario);

                    urlConnection = (HttpURLConnection) url
                            .openConnection();

                    InputStream in = urlConnection.getInputStream();

                    InputStreamReader isw = new InputStreamReader(in);

                    int data = isw.read();
                    while (data != -1) {
                        current += (char) data;
                        data = isw.read();
                    }
                    Log.w("", current);

                    return current;

                } catch (Exception e) {
                    e.printStackTrace();
                } finally {
                    if (urlConnection != null) {
                        urlConnection.disconnect();
                    }
                }

            } catch (Exception e) {
                e.printStackTrace();
                return "Exception: " + e.getMessage();
            }
            return current;
        }

        @Override
        protected void onPostExecute(String s) {

            try {
                JSONObject obj = new JSONObject(s);

                AlertDialog alertDialog = new AlertDialog.Builder(AdmSeguridadActivity.this).create();
                alertDialog.setTitle("Alert");
                //alertDialog.setMessage(obj.toString());
                boolean estado;

                estado = obj.getBoolean("estado");
                if (estado){
                    String yyyy,mm,dd;
                    //if (selectedUrl==AdmSeguridadActivity.readAllTipoUsuario){
                        JSONArray objList = obj.getJSONArray("listaDeResultados");
                        int length = objList.length();
                        for (int i=0;i<length;i++){
                            TipoUsuario tmp= new TipoUsuario();
                            tmp.setNombre(objList.getJSONObject(i).getString("nombre"));
                            tipoUsuarioList.add(tmp);
                        }

                    //}

                }
                else
                {
                    alertDialog.setMessage(obj.getString("mensaje")+". "+obj.getString("mensajeInterno"));
                    alertDialog.setButton(AlertDialog.BUTTON_NEUTRAL, "OK",
                            new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int which) {
                                    dialog.dismiss();
                                }
                            });
                    alertDialog.show();
                }
                //alertDialog.setMessage(obj.getJSONArray("listaDeResultados").length()+"");
                //for (int i=0;i<)


            }
            catch (Exception ex){
                //Toast.makeText(getApplicationContext(), ex.toString(), Toast.LENGTH_LONG).show();
                AlertDialog alertDialog = new AlertDialog.Builder(AdmSeguridadActivity.this).create();
                alertDialog.setTitle("Alert");
                alertDialog.setMessage(ex.toString()+" "+ex.getMessage()+" "+ex.getLocalizedMessage());
                alertDialog.setButton(AlertDialog.BUTTON_NEUTRAL, "OK",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.dismiss();
                            }
                        });
                alertDialog.show();
            }
        }
    }
    public class MyAsyncTasks3 extends AsyncTask<String, String, String> {


        @Override
        protected void onPreExecute() {
            super.onPreExecute();

        }

        @Override
        protected String doInBackground(String... params) {

            // implement API in background and store the response in current variable
            String current = "";

            try {
                URL url;
                HttpURLConnection urlConnection = null;
                try {
                    url = new URL(apiUrl+AdmSeguridadActivity.readAllSede);

                    urlConnection = (HttpURLConnection) url
                            .openConnection();

                    InputStream in = urlConnection.getInputStream();

                    InputStreamReader isw = new InputStreamReader(in);

                    int data = isw.read();
                    while (data != -1) {
                        current += (char) data;
                        data = isw.read();
                    }
                    Log.w("", current);

                    return current;

                } catch (Exception e) {
                    e.printStackTrace();
                } finally {
                    if (urlConnection != null) {
                        urlConnection.disconnect();
                    }
                }

            } catch (Exception e) {
                e.printStackTrace();
                return "Exception: " + e.getMessage();
            }
            return current;
        }

        @Override
        protected void onPostExecute(String s) {

            try {
                JSONObject obj = new JSONObject(s);

                AlertDialog alertDialog = new AlertDialog.Builder(AdmSeguridadActivity.this).create();
                alertDialog.setTitle("Alert");
                //alertDialog.setMessage(obj.toString());
                boolean estado;

                estado = obj.getBoolean("estado");
                if (estado){
                    String yyyy,mm,dd;
                    //if (selectedUrl==AdmSeguridadActivity.readAllTipoUsuario){
                    JSONArray objList = obj.getJSONArray("listaDeResultados");
                    int length = objList.length();
                    for (int i=0;i<length;i++){
                        Sede tmp= new Sede();
                        tmp.setNombre(objList.getJSONObject(i).getString("nombre"));
                        sedeList.add(tmp);
                    }

                    //}

                }
                else
                {
                    alertDialog.setMessage(obj.getString("mensaje")+". "+obj.getString("mensajeInterno"));
                    alertDialog.setButton(AlertDialog.BUTTON_NEUTRAL, "OK",
                            new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int which) {
                                    dialog.dismiss();
                                }
                            });
                    alertDialog.show();
                }
                //alertDialog.setMessage(obj.getJSONArray("listaDeResultados").length()+"");
                //for (int i=0;i<)


            }
            catch (Exception ex){
                //Toast.makeText(getApplicationContext(), ex.toString(), Toast.LENGTH_LONG).show();
                AlertDialog alertDialog = new AlertDialog.Builder(AdmSeguridadActivity.this).create();
                alertDialog.setTitle("Alert");
                alertDialog.setMessage(ex.toString()+" "+ex.getMessage()+" "+ex.getLocalizedMessage());
                alertDialog.setButton(AlertDialog.BUTTON_NEUTRAL, "OK",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.dismiss();
                            }
                        });
                alertDialog.show();
            }
        }
    }
}