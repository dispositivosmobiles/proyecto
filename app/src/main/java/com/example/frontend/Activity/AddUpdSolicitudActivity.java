package com.example.frontend.Activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.Spinner;

import com.example.frontend.LogicaNegocios.Estado;
import com.example.frontend.LogicaNegocios.Solicitud;
import com.example.frontend.LogicaNegocios.Tipo;
import com.example.frontend.R;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

public class AddUpdSolicitudActivity extends AppCompatActivity {
    private FloatingActionButton fBtn;
    private boolean editable = true;
    private EditText idFld,razonRechazoFld,adquisicionFld;
    private Spinner estadoSpinnerFld,tipoSpinnerFld;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_upd_solicitud);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        idFld = findViewById(R.id.idTxt);
        razonRechazoFld = findViewById(R.id.razonRechazoTxt);
        adquisicionFld = findViewById(R.id.adquisicionTxt);
        estadoSpinnerFld = findViewById(R.id.estadoSpinnerFld);
        tipoSpinnerFld = findViewById(R.id.tipoSpinnerFld);

        fBtn = findViewById(R.id.addUpdSolicitudBtn);

        idFld.setEnabled(false);

        /*FloatingActionButton fab = findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {11
                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }
        });*/
        initSpinner();
        initSpinner2();
        //receiving data from admProfesorActivity
        Bundle extras = getIntent().getExtras();
        if (extras != null) {

            editable = extras.getBoolean("editable");
            if (editable) {   // is editing some row
                Solicitud aux = (Solicitud) getIntent().getSerializableExtra("object");
                idFld.setText(aux.getId());
                razonRechazoFld.setText(aux.getRazonRechazo().toString());
                adquisicionFld.setText(aux.getAdquisicion().toString());
                for (int i=0;i<estadoSpinnerFld.getAdapter().getCount();i++){
                    if(estadoSpinnerFld.getItemAtPosition(i).equals(aux.getEstado().getNombre()))
                    {
                        estadoSpinnerFld.setSelection(i);
                        break;
                    }
                }
                for (int i=0;i<tipoSpinnerFld.getAdapter().getCount();i++){
                    if(tipoSpinnerFld.getItemAtPosition(i).equals(aux.getEstado().getNombre()))
                    {
                        tipoSpinnerFld.setSelection(i);
                        break;
                    }
                }


                //edit action
                fBtn.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        editObject();
                    }
                });
            } else {         // is adding new Carrera object
                //add new action
                fBtn.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        addObject();
                    }
                });
            }
        }
    }
    public void initSpinner() {
        //estadoSpinnerFld = (Spinner) findViewById(R.id.privilegioSpinnerFld);
        List<String> list = new ArrayList<>();
        list.add("Recivido");
        list.add("Por verificar");
        list.add("ninguno");
        ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(this,
                android.R.layout.simple_spinner_item, list);
        dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        estadoSpinnerFld.setAdapter(dataAdapter);


    }
    public void initSpinner2() {
        //estadoSpinnerFld = (Spinner) findViewById(R.id.privilegioSpinnerFld);
        List<String> list = new ArrayList<>();
        list.add("Recivido");
        list.add("Por verificar");
        list.add("ninguno");
        ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(this,
                android.R.layout.simple_spinner_item, list);
        dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        tipoSpinnerFld.setAdapter(dataAdapter);
    }

    public void addObject() {
        if (validateForm()) {
            //do something
            Solicitud user = new Solicitud();
            user.setTipo(new Tipo((String)tipoSpinnerFld.getSelectedItem()));
            user.setEstado(new Estado((String)estadoSpinnerFld.getSelectedItem()));
//            user.setId(Integer.valueOf(idFld.getText().toString()));
            user.setRazonRechazo(razonRechazoFld.getText().toString());
            user.setAdquisicion(adquisicionFld.getText()+"");

            //Intent intent = new Intent(getBaseContext(), AdmSolicitudActivity.class);
            //sending Solicitud data
            //intent.putExtra("addObject", user);
            //startActivity(intent);
            //finish(); //prevent go back
        }
    }

    public void editObject() {
        if (validateForm()) {
            Solicitud user = new Solicitud();
            user.setTipo(new Tipo((String)tipoSpinnerFld.getSelectedItem()));
            user.setEstado(new Estado((String)estadoSpinnerFld.getSelectedItem()));
            user.setId(Integer.valueOf(idFld.getText().toString()));
            user.setRazonRechazo(razonRechazoFld.getText().toString());
            user.setAdquisicion(adquisicionFld.getText().toString());
            /*Solicitud user = new Solicitud(correosetEstado(new Estado((String)estadoSpinnerFld.getSelectedItem()));
            user.setId(Integer.valueOf(idFld.getText().toString()));
            user.setRazonRechazo(razonRechazoFld.getText().toString());
            user.setAdquisicion(adquisicionFld.getText().toString());Fld.getText().toString(), passFld.getText().toString(),
                    spinner.getSelectedItem().toString(), cedulaFld.getText().toString());*/
            Intent intent = new Intent(getBaseContext(), AdmSeguridadActivity.class);
            //sending User data
            intent.putExtra("editObject", user);
            startActivity(intent);
            finish(); //prevent go back
        }
    }

    public boolean validateForm() {
        int error = 0;
        /*if (TextUtils.isEmpty(this.idFld.getText())) {
            idFld.setError("Correo requerido");
            error++;
        }*/
        if (TextUtils.isEmpty(this.adquisicionFld.getText())) {
            adquisicionFld.setError("Contraseña requerida");
            error++;
        }
        if (TextUtils.isEmpty(this.razonRechazoFld.getText())) {
            razonRechazoFld.setError("Cedula requerida");
            error++;
        }

        return true;
    }

}
