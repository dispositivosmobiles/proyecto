package com.example.frontend.Activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import com.example.frontend.LogicaNegocios.Tipo;
import com.example.frontend.R;


public class AddUpdTipoActivity extends AppCompatActivity {
    private FloatingActionButton fBtn;
    private boolean editable = true;
    private EditText nombreFld;
    private Spinner spinner;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_upd_tipo);
        editable = true;

        // button check
        fBtn = findViewById(R.id.addUpdTipoBtn);

        //cleaning stuff
        nombreFld = findViewById(R.id.nombreAddUpdTipo);

        //spinner

        //receiving data from admProfesorActivity
        Bundle extras = getIntent().getExtras();
        if (extras != null) {

            editable = extras.getBoolean("editable");
                     // is adding new Carrera object
                //add new action
                fBtn.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        addTipo();
                    }
                });

        }
    }



    public void addTipo() {
        if (validateForm()) {
            Tipo tip = new Tipo();
            tip.setNombre(nombreFld.getText().toString());

            Intent intent = new Intent(getBaseContext(), AdmTipoActivity.class);
            //sending Usuario data
            intent.putExtra("addTipo", tip);
            startActivity(intent);
            finish(); //prevent go back
        }
    }

    public boolean validateForm() {
        int error = 0;
        if (TextUtils.isEmpty(this.nombreFld.getText())) {
            nombreFld.setError("Nombre Requerido");
            error++;
        }
        if (error > 0) {
            Toast.makeText(getApplicationContext(), "Algunos errores", Toast.LENGTH_LONG).show();
            return false;
        }
        return true;
    }

}
