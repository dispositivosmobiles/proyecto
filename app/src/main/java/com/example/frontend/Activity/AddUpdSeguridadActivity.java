package com.example.frontend.Activity;

import android.app.DatePickerDialog;
import android.content.Intent;
import android.icu.text.SimpleDateFormat;
import android.icu.util.Calendar;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.RequiresApi;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import com.example.frontend.LogicaNegocios.Canton;
import com.example.frontend.LogicaNegocios.Provincia;
import com.example.frontend.LogicaNegocios.Sede;
import com.example.frontend.LogicaNegocios.TipoUsuario;
import com.example.frontend.R;
import com.example.frontend.LogicaNegocios.Usuario;

import java.lang.reflect.Array;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

@RequiresApi(api = Build.VERSION_CODES.N)
public class AddUpdSeguridadActivity extends AppCompatActivity {
    private FloatingActionButton fBtn;
    private boolean editable = true;
    private EditText correoFld;
    private EditText passFld;
    private EditText cedulaFld;
    private EditText telFld;
    private EditText ape1Fld;
    private EditText ape2Fld;
    private EditText nomFld;
   // private EditText sedeFld;
    private EditText nacFld;
    /*private EditText tipusrFld;*/
    private Spinner spinner,sedeSpinnerFld;
    final Calendar myCalendar = Calendar.getInstance();
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_upd_seguridad);
        editable = true;
        nacFld = findViewById(R.id.nacimientoAddUpdSeguridad);
        final DatePickerDialog.OnDateSetListener date = new DatePickerDialog.OnDateSetListener() {

            @Override
            public void onDateSet(DatePicker view, int year, int monthOfYear,
                                  int dayOfMonth) {
                // TODO Auto-generated method stub
                myCalendar.set(Calendar.YEAR, year);
                myCalendar.set(Calendar.MONTH, monthOfYear);
                myCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);
                updateLabel();
            }

        };
        nacFld.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                new DatePickerDialog(AddUpdSeguridadActivity.this, date, myCalendar
                        .get(Calendar.YEAR), myCalendar.get(Calendar.MONTH),
                        myCalendar.get(Calendar.DAY_OF_MONTH)).show();
            }
        });
        // button check
        fBtn = findViewById(R.id.addUpdSeguridadBtn);
        fBtn.setEnabled(true);
                //cleaning stuff
        sedeSpinnerFld = findViewById(R.id.sedeSpinnerFld);
        correoFld = findViewById(R.id.emailAddUpdSeguridad);
        passFld = findViewById(R.id.contrasenaAddUpdSeguridad);
        cedulaFld = findViewById(R.id.cedulaAddUpdSeguridad);
        telFld = findViewById(R.id.telefonoAddUpdSeguridad);
        ape1Fld = findViewById(R.id.apellido1AddUpdSeguridad);
        ape2Fld = findViewById(R.id.apellido2AddUpdSeguridad);
        nomFld = findViewById(R.id.nombreAddUpdSeguridad);
     //   sedeFld = findViewById(R.id.sedeAddUpdSeguridad);

        //tipusrFld = findViewById(R.id.tipousrAddUpdSeguridad);
//        correoFld.setText("");
  //      passFld.setText("");

        //spinner
        //initSpinner();

        //receiving data from admProfesorActivity
        Bundle extras = getIntent().getExtras();
        if (extras != null) {
            ArrayList<TipoUsuario> tipoUsuarioList = (ArrayList<TipoUsuario>) getIntent().getSerializableExtra("tipoUsuarioList");
            ArrayList<Sede> sedeList = (ArrayList<Sede>) getIntent().getSerializableExtra("sedeList");
            initSpinner(tipoUsuarioList);
            initSpinnerSede(sedeList);

            editable = extras.getBoolean("editable");
            if (editable) {   // is editing some row
                Usuario aux = (Usuario) getIntent().getSerializableExtra("usuario");
                cedulaFld.setEnabled(false);
                correoFld.setEnabled(false);
                correoFld.setText(aux.getEmail());
                passFld.setText(aux.getClave());
                cedulaFld.setText(aux.getCedula());
                telFld.setText(aux.getTelefono());
                ape1Fld.setText(aux.getApellido1());
                ape2Fld.setText(aux.getApellido2());
                nomFld.setText(aux.getNombre());
              //  sedeFld.setText(aux.getSede().getNombre());
                nacFld.setText(aux.getNacimiento().toString());
                String _2cmpr=aux.getTipoUsuario().getNombre();
                for (int i=0;i<spinner.getCount();i++){
                    String tmp=(String) spinner.getItemAtPosition(i);

                    if (tmp.equals(_2cmpr))
                    {
                        spinner.setSelection(i);
                        break;
                    }
                }
                _2cmpr=aux.getSede().getNombre();
                for (int i=0;i<sedeSpinnerFld.getCount();i++){
                    String tmp=(String) sedeSpinnerFld.getItemAtPosition(i);

                    if (tmp.equals(_2cmpr))
                    {
                        sedeSpinnerFld.setSelection(i);
                        break;
                    }
                }

              //  tipusrFld.setText(aux.getTipoUsuario().getNombre());
                //edit action
                fBtn.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        editUsuario();
                    }
                });
            } else {         // is adding new Carrera object
                //add new action
                fBtn.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        addUsuario();
                    }
                });
            }
        }
    }
    private void updateLabel() {
        String myFormat = "dd/MM/yyyy"; //In which you need put here
        SimpleDateFormat sdf = new SimpleDateFormat(myFormat, Locale.US);

        nacFld.setText(sdf.format(myCalendar.getTime()));
    }
    public void initSpinner(ArrayList<TipoUsuario> listTU) {
        spinner = (Spinner) findViewById(R.id.privilegioSpinnerFld);
        List<String> list = new ArrayList<String>();
        if (listTU!=null){
            for(int i=0;i<listTU.size();i++)
            {
                list.add(listTU.get(i).getNombre());
            }
        }
        ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(this,
                android.R.layout.simple_spinner_item, list);
        dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner.setAdapter(dataAdapter);
    }
    public void initSpinnerSede(ArrayList<Sede> listTU) {
        spinner = (Spinner) findViewById(R.id.privilegioSpinnerFld);
        List<String> list = new ArrayList<String>();
        if (listTU!=null){
            for(int i=0;i<listTU.size();i++)
            {
                list.add(listTU.get(i).getNombre());
            }
        }
        ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(this,
                android.R.layout.simple_spinner_item, list);
        dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        sedeSpinnerFld.setAdapter(dataAdapter);
    }
    public void initSpinner() {
        spinner = (Spinner) findViewById(R.id.privilegioSpinnerFld);
        List<String> list = new ArrayList<String>();
        list.add("administrador");
        list.add("matriculador");
        list.add("ninguno");
        ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(this,
                android.R.layout.simple_spinner_item, list);
        dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner.setAdapter(dataAdapter);
    }

    public void addUsuario() {
        if (validateForm()) {
            //do something
            Canton c = new Canton("Alajuela", new Provincia("Alajuela"));
            Sede s = new Sede((String) sedeSpinnerFld.getSelectedItem());
            TipoUsuario t = new TipoUsuario((String) spinner.getSelectedItem());
            Usuario user = new Usuario();
            user.setTelefono(telFld.getText().toString());
            user.setSede(s);
            user.setNombre(nomFld.getText().toString());
            user.setApellido1(ape1Fld.getText().toString());
            user.setApellido2(ape2Fld.getText().toString());
            user.setClave(this.passFld.getText().toString());
            user.setEmail(correoFld.getText().toString());
            user.setTipoUsuario(t);
            user.setCedula(cedulaFld.getText().toString());
            user.setNacimiento(nacFld.getText().toString());
            /*Usuario user = new Usuario(correoFld.getText().toString(), passFld.getText().toString(),
                    spinner.getSelectedItem().toString(), cedulaFld.getText().toString());*/
            Intent intent = new Intent(getBaseContext(), AdmSeguridadActivity.class);
            //sending Usuario data
            intent.putExtra("addUsuario", user);
            startActivity(intent);
            finish(); //prevent go back
        }
    }

    public void editUsuario() {
        if (validateForm()) {
            TipoUsuario t = new TipoUsuario((String) spinner.getSelectedItem());
            Usuario user = new Usuario();
            Sede s = new Sede((String) sedeSpinnerFld.getSelectedItem());
            user.setTelefono(telFld.getText().toString());
            user.setSede(s);
            user.setNombre(nomFld.getText().toString());
            user.setApellido1(ape1Fld.getText().toString());
            user.setApellido2(ape2Fld.getText().toString());
            user.setClave(this.passFld.getText().toString());
            user.setEmail(correoFld.getText().toString());
            user.setTipoUsuario(t);
            user.setCedula(cedulaFld.getText().toString());
            user.setNacimiento(nacFld.getText().toString());
            /*Usuario user = new Usuario(correoFld.getText().toString(), passFld.getText().toString(),
                    spinner.getSelectedItem().toString(), cedulaFld.getText().toString());*/
            Intent intent = new Intent(getBaseContext(), AdmSeguridadActivity.class);
            //sending User data
            intent.putExtra("editUsuario", user);
            startActivity(intent);
            finish(); //prevent go back
        }
    }

    public boolean validateForm() {
        int error = 0;
        if (TextUtils.isEmpty(this.correoFld.getText())) {
            correoFld.setError("Correo requerido");
            error++;
        }
        if (TextUtils.isEmpty(this.passFld.getText())) {
            passFld.setError("Contraseña requerida");
            error++;
        }
        if (TextUtils.isEmpty(this.cedulaFld.getText())) {
            cedulaFld.setError("Cedula requerida");
            error++;
        }
        if (spinner.getSelectedItem().toString().equals("")) {
            error++;
        }
        if (error > 0) {
            Toast.makeText(getApplicationContext(), "Algunos errores", Toast.LENGTH_LONG).show();
            return false;
        }
        return true;
    }
}