package com.example.frontend.ModelData;

import com.example.frontend.LogicaNegocios.Canton;
import com.example.frontend.LogicaNegocios.Inventario;
import com.example.frontend.LogicaNegocios.Provincia;
import com.example.frontend.LogicaNegocios.Sede;
import com.example.frontend.LogicaNegocios.Tipo;
import com.example.frontend.LogicaNegocios.TipoUsuario;
import com.example.frontend.LogicaNegocios.Usuario;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by User on 26/03/2018.
 */

public class ModelData {
    List<Usuario> users;
    List<TipoUsuario> tipoUsuarios;
    List<Inventario> inventarios;
    List<Tipo> tipos;

    public ModelData() {

        this.users = new ArrayList<>();
        this.tipoUsuarios = new ArrayList<>();
        this.inventarios = new ArrayList<>();
        this.tipos = new ArrayList<>();
    }

    public List<Usuario> getUsers() {
        return users;
    }

    public void setUsers(List<Usuario> users) {
        this.users = users;
    }

    public List<TipoUsuario> getTipoUsuarios() {
        return tipoUsuarios;
    }

    public void setTipoUsuarios(List<TipoUsuario> tipoUsuarios) {
        this.tipoUsuarios = tipoUsuarios;
    }
    public List<Inventario> getInventarios() { return inventarios; }
    public void setInventarios(List<Inventario> inventarios) {
        this.inventarios = inventarios;
    }

    public List<Tipo> getTipos() {
        return tipos;
    }

    public void setTipos(List<Tipo> tipos) {
        this.tipos = tipos;
    }
}
