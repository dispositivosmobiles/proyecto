/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DAO;



import BussinessLogic.Usuario;
import BussinessLogic.Sede;
import BussinessLogic.TipoUsuario;
import java.sql.CallableStatement;
import java.sql.Date;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Types;
import java.util.ArrayList;
import org.postgresql.util.PSQLException;
import util.CodigoRespuesta;
import util.Respuesta;



/**
 *
 * @author edva5
 */
public class UsuarioDAO extends DAO {
   private static UsuarioDAO INSTANCE;
    private static final String CREATE = "{call schema_inventory.CREATEUsuario(?,?,?,?,?,?,?,?,?,?)}";    
    private static final String READ = "{?=call schema_inventory.READUsuario(?)}";
    private static final String UPDATE = "{call schema_inventory.UPDATEUsuario(?,?,?,?,?,?,?,?)}";
    private static final String READ_ALL = "{?=call schema_inventory.READALLUsuario()}";    
    private static final String DELETE = "{call schema_inventory.DELETEUsuario(?)}";

    private UsuarioDAO(){
         super();
     }

     public static UsuarioDAO getInstance(){
         if (INSTANCE==null)
             INSTANCE=new UsuarioDAO();
         return INSTANCE;
     }

    public Respuesta create(Usuario u) {
        Respuesta tmp = new Respuesta(
                Boolean.FALSE, 
                CodigoRespuesta.ERROR_INTERNO, 
                "Error al crear Usuario", 
                "UsuarioDao/create(Usuario u)");
        try {
            conectar();
            
        } catch (Exception e) {            
            tmp= new Respuesta(
                    Boolean.FALSE, 
                    CodigoRespuesta.ERROR_ACCESO, 
                    "Error de Conexion", 
                    "Error de Conexion a la base de datos: "+e.toString()
            );
//            throw new GlobalException("No se ha localizado el driver");
        }
         CallableStatement pstmt=null;          
         try {
            pstmt = conexion.prepareCall(CREATE);                                                            
            pstmt.setString(1, u.getCedula());                        
            pstmt.setString(2, u.getNombre());                        
            pstmt.setString(3, u.getApellido1());
            pstmt.setString(4, u.getApellido2());
            pstmt.setString(5, u.getTelefono());
            pstmt.setString(6, u.getEmail());
            pstmt.setDate(7, Date.valueOf(u.getNacimiento()));
            pstmt.setString(8, u.getTipoUsuario().getNombre());
            pstmt.setString(9, u.getClave());
            pstmt.setString(10, u.getSede().getNombre());
            
            boolean resultado = pstmt.execute();
            if (resultado == true)
                 tmp= new Respuesta(
                    Boolean.TRUE, 
                    CodigoRespuesta.CORRECTO, 
                    "Usuario exitosamente salvado.", 
                    u.toString()+" exitosamente salvado"
            );
         }catch(PSQLException e){
              tmp= new Respuesta(
                    Boolean.FALSE, 
                    CodigoRespuesta.ERROR_INTERNO, 
                    "Error salvando Usuario", 
                    "Error Postgresql: "+e.toString()
            );      
                    }
            catch (SQLException e) {
            tmp= new Respuesta(
                    Boolean.FALSE, 
                    CodigoRespuesta.ERROR_ACCESO, 
                    "Error salvando Usuario", 
                    e.toString()
            );
          //  throw new GlobalException("Llave duplicada");
        } 
         finally {
            try {
                if (pstmt != null)
                    pstmt.close();                                    
                desconectar();
            } catch (SQLException e) {
                tmp= new Respuesta(
                    Boolean.FALSE, 
                    CodigoRespuesta.ERROR_ACCESO, 
                    "Error cerrando conexion", 
                    e.toString()
            );
              //  throw new GlobalException("Estatutos invalidos o nulos");
            }
        }
         return tmp;
    }

    public Respuesta read(Usuario u) {
        Respuesta tmp = new Respuesta(
                Boolean.FALSE, 
                CodigoRespuesta.ERROR_INTERNO, 
                "Error al leer Usuario", 
                "UsuarioDao/read(Usuario u)");
        try {
            conectar();
            
        } catch (Exception e) {
            tmp= new Respuesta(
                    Boolean.FALSE, 
                    CodigoRespuesta.ERROR_ACCESO, 
                    "Error de Conexion", 
                    "Error de Conexion a la base de datos: "+e.toString()
            );
        }
        ResultSet rs = null;
        ArrayList coleccion = new ArrayList();
        Usuario a = null;
        CallableStatement pstmt=null;
        try {
            this.conexion.setAutoCommit(false);
            
            pstmt = this.conexion.prepareCall(READ);
            pstmt.setFetchSize(1);
            pstmt.registerOutParameter(1, Types.OTHER);
            pstmt.setString(2,u.getCedula());
            
            pstmt.execute();
            rs = (ResultSet)pstmt.getObject(1);
            while (rs.next()) {
               a = new Usuario(rs.getString("cedula"));
               a.setNombre(rs.getString("nombre"));
               a.setApellido1(rs.getString("apellido1"));
               a.setApellido2(rs.getString("apellido2"));
               a.setEmail(rs.getString("email"));
               a.setTelefono(rs.getString("telefono"));
               a.setTipoUsuario(new TipoUsuario(rs.getString("TipoUsuario")));
               a.setNacimiento(rs.getDate("nacimiento").toLocalDate());
               a.setSede(new Sede(rs.getString("Sede")));
               a.setClave(rs.getString("clave"));
               
      
               

                coleccion.add(a);
                tmp= new Respuesta(
                    Boolean.TRUE, 
                    CodigoRespuesta.CORRECTO, 
                    "Usuario exitosamente encontrada.", 
                    a.toString()+" exitosamente encontrado",
                        a
            );
            }
            pstmt.setFetchSize(0);
            rs.close();           
            pstmt.close();
        } catch (SQLException e) {
          return new Respuesta(
                Boolean.FALSE, 
                CodigoRespuesta.ERROR_NOENCONTRADO, 
                "Error al buscar Usuario", 
                e.toString());

            //throw new GlobalException("Sentencia no valida");
        } finally {
            try {
                if (rs != null) {
                    rs.close();
                }
                if (pstmt != null) {
                    pstmt.close();
                }
                desconectar();
            } catch (SQLException e) {
               // throw new GlobalException("Estatutos invalidos o nulos");
            }
        }
        if (coleccion == null || coleccion.size() == 0) {
            tmp = new Respuesta(
                Boolean.FALSE, 
                CodigoRespuesta.ERROR_NOENCONTRADO, 
                "No se encontro Usuario", 
                "UsuarioDao/read(Usuario u) no result"+coleccion);
        }
        return tmp;
    }
    public Respuesta update(Usuario u) {
        Respuesta tmp = new Respuesta(
                Boolean.FALSE, 
                CodigoRespuesta.ERROR_INTERNO, 
                "Error al actualizar Usuario", 
                "UsuarioDao/create(Usuario u)");
        try {
            conectar();
            
        } catch (Exception e) {            
            tmp= new Respuesta(
                    Boolean.FALSE, 
                    CodigoRespuesta.ERROR_ACCESO, 
                    "Error de Conexion", 
                    "Error de Conexion a la base de datos: "+e.toString()
            );
//            throw new GlobalException("No se ha localizado el driver");
        }
         CallableStatement pstmt=null;          
         try {
            pstmt = conexion.prepareCall(UPDATE);                                                            
            pstmt.setString(1, u.getCedula());                        
            pstmt.setString(2, u.getNombre());                        
            pstmt.setString(3, u.getApellido1());
            pstmt.setString(4, u.getApellido2());
            pstmt.setString(5, u.getTelefono());
            pstmt.setString(6, u.getEmail());
            pstmt.setDate(7, Date.valueOf(u.getNacimiento()));
            pstmt.setString(8, u.getTipoUsuario().getNombre());
            pstmt.setString(9, u.getClave());
            pstmt.setString(10, u.getSede().getNombre());
            boolean resultado = pstmt.execute();
            if (resultado == true)
                 tmp= new Respuesta(
                    Boolean.TRUE, 
                    CodigoRespuesta.CORRECTO, 
                    "Usuario exitosamente actualizado.", 
                    u.toString()+" exitosamente actualizado"
            );
         }catch(PSQLException e){
              tmp= new Respuesta(
                    Boolean.FALSE, 
                    CodigoRespuesta.ERROR_INTERNO, 
                    "Error actualizando Usuario", 
                    "Error Postgresql: "+e.toString()
            );      
                    }
            catch (SQLException e) {
            tmp= new Respuesta(
                    Boolean.FALSE, 
                    CodigoRespuesta.ERROR_ACCESO, 
                    "Error actualizando Usuario", 
                    e.toString()
            );
          //  throw new GlobalException("Llave duplicada");
        } 
         finally {
            try {
                if (pstmt != null)
                    pstmt.close();                                    
                desconectar();
            } catch (SQLException e) {
                tmp= new Respuesta(
                    Boolean.FALSE, 
                    CodigoRespuesta.ERROR_ACCESO, 
                    "Error cerrando conexion", 
                    e.toString()
            );
              //  throw new GlobalException("Estatutos invalidos o nulos");
            }
        }
         return tmp;
    }
    
public Respuesta delete(Usuario u) {
        Respuesta tmp = new Respuesta(
                Boolean.FALSE, 
                CodigoRespuesta.ERROR_INTERNO, 
                "Error al eliminar Usuario", 
                "UsuarioDao/delete(Usuario u)");
        try {
            conectar();
        } catch (Exception e) {            
            tmp= new Respuesta(
                    Boolean.FALSE, 
                    CodigoRespuesta.ERROR_ACCESO, 
                    "Error de Conexion", 
                    "Error de Conexion a la base de datos: "+e.toString()
            );
            return tmp;
//            throw new GlobalException("No se ha localizado el driver");
        }
         CallableStatement pstmt=null;          
         try {
            pstmt = conexion.prepareCall(DELETE);                                                
            pstmt.setString(1, u.getCedula());                       
            boolean resultado = pstmt.execute();
            if (resultado == true){
                 tmp= new Respuesta(
                    Boolean.TRUE, 
                    CodigoRespuesta.CORRECTO, 
                    "Usuario exitosamente eliminada.", 
                    '{'+"nombre:"+u.getNombre()+" exitosamente eliminado"
            );}
         }catch(PSQLException e){
              tmp= new Respuesta(
                    Boolean.FALSE, 
                    CodigoRespuesta.ERROR_INTERNO, 
                    "Error eliminando Usuario", 
                    "Error Postgresql: "+e.toString()
            );      
              return tmp;      }
            catch (SQLException e) {
            tmp= new Respuesta(
                    Boolean.FALSE, 
                    CodigoRespuesta.ERROR_ACCESO, 
                    "Error eliminando Usuario", 
                    e.toString()
            );
            return tmp;
          //  throw new GlobalException("Llave duplicada");
        } 
         finally {
            try {
                if (pstmt != null)
                    pstmt.close();                                    
                desconectar();
            } catch (SQLException e) {
                tmp= new Respuesta(
                    Boolean.FALSE, 
                    CodigoRespuesta.ERROR_ACCESO, 
                    "Error cerrando conexion", 
                    e.toString()
            );
              //  throw new GlobalException("Estatutos invalidos o nulos");
            }
        }
         return tmp;
    }

    public Respuesta readAll() {
        Respuesta tmp = new Respuesta(
                Boolean.FALSE, 
                CodigoRespuesta.ERROR_INTERNO, 
                "Error al leer Usuarios", 
                "UsuarioDao/readall(Usuario u)");
        try {
            conectar();
            
        } catch (Exception e) {
            tmp= new Respuesta(
                    Boolean.FALSE, 
                    CodigoRespuesta.ERROR_ACCESO, 
                    "Error de Conexion", 
                    "Error de Conexion a la base de datos: "+e.toString()
            );
        }
        ResultSet rs = null;
        ArrayList coleccion = new ArrayList();
        Usuario a = null;
        CallableStatement pstmt=null;
        try {
            this.conexion.setAutoCommit(false);
            
            pstmt = this.conexion.prepareCall(READ_ALL);
            pstmt.setFetchSize(50);
            pstmt.registerOutParameter(1, Types.OTHER);            
            
            pstmt.execute();
            rs = (ResultSet)pstmt.getObject(1);
            while (rs.next()) {
                a = new Usuario(rs.getString("cedula"));
               a.setNombre(rs.getString("nombre"));
               a.setApellido1(rs.getString("apellido1"));
               a.setApellido2(rs.getString("apellido2"));
               a.setEmail(rs.getString("email"));
               a.setTelefono(rs.getString("telefono"));
               a.setTipoUsuario(new TipoUsuario(rs.getString("TipoUsuario")));
               a.setNacimiento(rs.getDate("nacimiento").toLocalDate());
               a.setSede(new Sede(rs.getString("Sede")));
               a.setClave(rs.getString("clave"));

                coleccion.add(a);                
            }
            tmp= new Respuesta(
                    Boolean.TRUE, 
                    CodigoRespuesta.CORRECTO, 
                    "Usuarios exitosamente encontrada.", 
                    coleccion.size()+" Usuarios exitosamente encontradas."
            );
            tmp.setListaDeResultados(coleccion);
            pstmt.setFetchSize(0);
            rs.close();           
            pstmt.close();
        } catch (PSQLException e){
            tmp= new Respuesta(
                    Boolean.FALSE, 
                    CodigoRespuesta.ERROR_INTERNO, 
                    "Error listando Usuarios", 
                    "Error Postgresql: "+e.toString()
            );      
        } catch (SQLException e) {
          e.printStackTrace();

            //throw new GlobalException("Sentencia no valida");
        } finally {
            try {
                if (rs != null) {
                    rs.close();
                }
                if (pstmt != null) {
                    pstmt.close();
                }
                desconectar();
            } catch (SQLException e) {
               // throw new GlobalException("Estatutos invalidos o nulos");
            }
        }
        /*if (coleccion == null || coleccion.size() == 0) {
            tmp = new Respuesta(
                Boolean.FALSE, 
                CodigoRespuesta.ERROR_NOENCONTRADO, 
                "No se encontro Usuario", 
                "UsuarioDao/readall(Usuario u) no result: "+coleccion.toString());
        }*/
        return tmp;
    }

}
