/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DAO;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author eddy
 */
abstract public class DAO {
    protected Connection conexion= null; 
    protected  final String url = "jdbc:postgresql://localhost/db_inventory";
    protected  final String user = "postgres";
    protected  final String password = "manager";
    
    protected  boolean conectar() {        
        try {
            Class.forName("org.postgresql.Driver");//Necessary for unsing it from a web client
            this.conexion = DriverManager.getConnection(url, user, password);
                System.out.println("Connected to the PostgreSQL server successfully.");
            return true;
        } catch (SQLException e) {
                System.out.println(e.getMessage());
            return false;
        } catch (ClassNotFoundException e) {
            Logger.getLogger(DAO.class.getName()).log(Level.SEVERE, null, e);
        }
        return false;
         
    }
    protected void desconectar() throws SQLException{
        if(!conexion.isClosed())
            conexion.close();       
    }
}
