/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DAO;



import BussinessLogic.Estado;
import BussinessLogic.Solicitud;
import BussinessLogic.Sede;
import BussinessLogic.Tipo;
import BussinessLogic.Usuario;
import java.sql.CallableStatement;
import java.sql.Date;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Types;
import java.util.ArrayList;
import org.postgresql.util.PSQLException;
import util.CodigoRespuesta;
import util.Respuesta;



/**
 *
 * @author edva5
 */
public class SolicitudDAO extends DAO {
   private static SolicitudDAO INSTANCE;
    private static final String CREATE = "{call schema_inventory.CREATESolicitud(?,?,?)}";    
    private static final String READ = "{?=call schema_inventory.READSolicitud(?)}";
    private static final String READ_ALL = "{?=call schema_inventory.READALLSolicitud()}";
    private static final String UPDATE = "{call schema_inventory.UPDATESolicitud(?,?,?)}";
    private static final String DELETE = "{call schema_inventory.DELETESolicitud(?)}";

    private SolicitudDAO(){
         super();
     }

     public static SolicitudDAO getInstance(){
         if (INSTANCE==null)
             INSTANCE=new SolicitudDAO();
         return INSTANCE;
     }

    public Respuesta create(Solicitud u) {
        Respuesta tmp = new Respuesta(
                Boolean.FALSE, 
                CodigoRespuesta.ERROR_INTERNO, 
                "Error al crear Solicitud", 
                "SolicitudDao/create(Solicitud u)");
        try {
            conectar();
        } catch (Exception e) {            
            tmp= new Respuesta(
                    Boolean.FALSE, 
                    CodigoRespuesta.ERROR_ACCESO, 
                    "Error de Conexion", 
                    "Error de Conexion a la base de datos: "+e.toString()
            );
//            throw new GlobalException("No se ha localizado el driver");
        }
         CallableStatement pstmt=null;          
         try {
            pstmt = conexion.prepareCall(CREATE);                                                
            pstmt.setString(1, u.getTipo().getNombre());                                   
            pstmt.setDate(2, Date.valueOf(u.getAdquisicion()));            
            pstmt.setString(3, u.getSolicitante().getCedula());
            
            boolean resultado = pstmt.execute();
            if (resultado == true)
                 tmp= new Respuesta(
                    Boolean.TRUE, 
                    CodigoRespuesta.CORRECTO, 
                    "Solicitud exitosamente salvado.", 
                    " exitosamente salvado"
            );
         }catch(PSQLException e){
              tmp= new Respuesta(
                    Boolean.FALSE, 
                    CodigoRespuesta.ERROR_INTERNO, 
                    "Error salvando Solicitud", 
                    "Error Postgresql: "+e.toString()
            );      
                    }
            catch (SQLException e) {
            tmp= new Respuesta(
                    Boolean.FALSE, 
                    CodigoRespuesta.ERROR_ACCESO, 
                    "Error salvando Solicitud", 
                    e.toString()
            );
          //  throw new GlobalException("Llave duplicada");
        } 
         finally {
            try {
                if (pstmt != null)
                    pstmt.close();                                    
                desconectar();
            } catch (SQLException e) {
                tmp= new Respuesta(
                    Boolean.FALSE, 
                    CodigoRespuesta.ERROR_ACCESO, 
                    "Error cerrando conexion", 
                    e.toString()
            );
              //  throw new GlobalException("Estatutos invalidos o nulos");
            }
        }
         return tmp;
    }

    public Respuesta read(Solicitud u) {
        Respuesta tmp = new Respuesta(
                Boolean.FALSE, 
                CodigoRespuesta.ERROR_INTERNO, 
                "Error al leer Solicitud", 
                "SolicitudDao/read(Solicitud u)");
        try {
            conectar();
            
        } catch (Exception e) {
            tmp= new Respuesta(
                    Boolean.FALSE, 
                    CodigoRespuesta.ERROR_ACCESO, 
                    "Error de Conexion", 
                    "Error de Conexion a la base de datos: "+e.toString()
            );
        }
        ResultSet rs = null;
        ArrayList coleccion = new ArrayList();
        Solicitud a = null;
        CallableStatement pstmt=null;
        try {
            this.conexion.setAutoCommit(false);
            
            pstmt = this.conexion.prepareCall(READ);
            pstmt.setFetchSize(1);
            pstmt.registerOutParameter(1, Types.OTHER);
            pstmt.setInt(2,u.getId());
            
            pstmt.execute();
            rs = (ResultSet)pstmt.getObject(1);
            while (rs.next()) {
               a = new Solicitud();
               a.setId(rs.getInt("id"));               
               a.setTipo(new Tipo(rs.getString("TipoAdquisicion")));
               a.setEstado(new Estado(rs.getString("estado")));
               a.setAdquisicion(rs.getDate("adquisicion").toLocalDate());
               a.setRazonRechazo(rs.getString("razonRechazo"));
               a.setSolicitante(new Usuario(rs.getString("solicitante")));
               

                coleccion.add(a);
                tmp= new Respuesta(
                    Boolean.TRUE, 
                    CodigoRespuesta.CORRECTO, 
                    "Solicitud exitosamente encontrada.", 
                    a.toString()+" exitosamente encontrado",
                        a
            );
            }
            pstmt.setFetchSize(0);
            rs.close();           
            pstmt.close();
        } catch (SQLException e) {
          return new Respuesta(
                    Boolean.TRUE, 
                    CodigoRespuesta.CORRECTO, 
                    "Solicitud exitosamente encontrada.", 
                    e.toString()
            );

            //throw new GlobalException("Sentencia no valida");
        } finally {
            try {
                if (rs != null) {
                    rs.close();
                }
                if (pstmt != null) {
                    pstmt.close();
                }
                desconectar();
            } catch (SQLException e) {
               // throw new GlobalException("Estatutos invalidos o nulos");
            }
        }
        if (coleccion == null || coleccion.size() == 0) {
            tmp = new Respuesta(
                Boolean.FALSE, 
                CodigoRespuesta.ERROR_NOENCONTRADO, 
                "No se encontro Solicitud", 
                "SolicitudDao/read(Solicitud u) no result"+coleccion);
        }
        return tmp;
    }
    public Respuesta update(Solicitud u) {
        Respuesta tmp = new Respuesta(
                Boolean.FALSE, 
                CodigoRespuesta.ERROR_INTERNO, 
                "Error al actualizar Solicitud", 
                "SolicitudDao/update(Solicitud u)");
        try {
            conectar();
        } catch (Exception e) {            
            tmp= new Respuesta(
                    Boolean.FALSE, 
                    CodigoRespuesta.ERROR_ACCESO, 
                    "Error de Conexion", 
                    "Error de Conexion a la base de datos: "+e.toString()
            );
//            throw new GlobalException("No se ha localizado el driver");
        }
         CallableStatement pstmt=null;          
         try {
            pstmt = conexion.prepareCall(UPDATE);  
            pstmt.setInt(1, u.getId());            
            pstmt.setString(2, u.getEstado().getNombre());                                    
            pstmt.setString(3, u.getRazonRechazo());            
            
            boolean resultado = pstmt.execute();
            if (resultado == true){
                
                 tmp= new Respuesta(
                    Boolean.TRUE, 
                    CodigoRespuesta.CORRECTO, 
                    "Solicitud exitosamente actualizada.", 
                    " exitosamente actualizado"
            );}
         }catch(PSQLException e){
              tmp= new Respuesta(
                    Boolean.FALSE, 
                    CodigoRespuesta.ERROR_INTERNO, 
                    "Error ctualizando Solicitud", 
                    "Error Postgresql: "+e.toString()
            );      
                    }
            catch (SQLException e) {
            tmp= new Respuesta(
                    Boolean.FALSE, 
                    CodigoRespuesta.ERROR_ACCESO, 
                    "Error ctualizando Solicitud", 
                    e.toString()
            );
          //  throw new GlobalException("Llave duplicada");
        } 
         finally {
            try {
                if (pstmt != null)
                    pstmt.close();                                    
                desconectar();
            } catch (SQLException e) {
                tmp= new Respuesta(
                    Boolean.FALSE, 
                    CodigoRespuesta.ERROR_ACCESO, 
                    "Error cerrando conexion: update Solicitud", 
                    e.toString()
            );
              //  throw new GlobalException("Estatutos invalidos o nulos");
            }
        }
         return tmp;
    }
public Respuesta delete(Solicitud u) {
        Respuesta tmp = new Respuesta(
                Boolean.FALSE, 
                CodigoRespuesta.ERROR_INTERNO, 
                "Error al eliminar Solicitud", 
                "SolicitudDao/delete(Solicitud u)");
        try {
            conectar();
        } catch (Exception e) {            
            tmp= new Respuesta(
                    Boolean.FALSE, 
                    CodigoRespuesta.ERROR_ACCESO, 
                    "Error de Conexion", 
                    "Error de Conexion a la base de datos:"+e.getLocalizedMessage()
            );
//            throw new GlobalException("No se ha localizado el driver");
        }
         CallableStatement pstmt=null;          
         try {
            pstmt = conexion.prepareCall(DELETE);                                                
            pstmt.setInt(1, u.getId());                       
            boolean resultado = pstmt.execute();
            if (resultado == true){
                 tmp= new Respuesta(
                    Boolean.TRUE, 
                    CodigoRespuesta.CORRECTO, 
                    "Solicitud exitosamente eliminada.", 
                    '{'+" exitosamente eliminado}"
            );}
         }catch(PSQLException e){
              tmp= new Respuesta(
                    Boolean.FALSE, 
                    CodigoRespuesta.ERROR_INTERNO, 
                    "Error eliminando Solicitud", 
                    "Error Postgresql: "+e.toString()
            );      
                    }
            catch (SQLException e) {
            tmp= new Respuesta(
                    Boolean.FALSE, 
                    CodigoRespuesta.ERROR_ACCESO, 
                    "Error eliminando Solicitud", 
                    e.toString()
            );
          //  throw new GlobalException("Llave duplicada");
        } 
         finally {
            try {
                if (pstmt != null)
                    pstmt.close();                                    
                desconectar();
            } catch (SQLException e) {
                tmp= new Respuesta(
                    Boolean.FALSE, 
                    CodigoRespuesta.ERROR_ACCESO, 
                    "Error cerrando conexion", 
                    e.toString()
            );
              //  throw new GlobalException("Estatutos invalidos o nulos");
            }
        }
         return tmp;
    }

    public Respuesta readAll() {
        Respuesta tmp = new Respuesta(
                Boolean.FALSE, 
                CodigoRespuesta.ERROR_INTERNO, 
                "Error al leer Solicituds", 
                "SolicitudDao/readall(Solicitud u)");
        try {
            conectar();
            
        } catch (Exception e) {
            tmp= new Respuesta(
                    Boolean.FALSE, 
                    CodigoRespuesta.ERROR_ACCESO, 
                    "Error de Conexion", 
                    "Error de Conexion a la base de datos:"+e.toString()
            );
        }
        ResultSet rs = null;
        ArrayList coleccion = new ArrayList();
        Solicitud a = null;
        CallableStatement pstmt=null;
        try {
            this.conexion.setAutoCommit(false);
            
            pstmt = this.conexion.prepareCall(READ_ALL);
            pstmt.setFetchSize(50);
            pstmt.registerOutParameter(1, Types.OTHER);            
            
            pstmt.execute();
            rs = (ResultSet)pstmt.getObject(1);
            while (rs.next()) {
                
               a = new Solicitud();
               a.setId(rs.getInt("id"));                             
               a.setTipo(new Tipo(rs.getString("TipoAdquisicion")));
               a.setEstado(new Estado(rs.getString("estado")));
               a.setAdquisicion(rs.getDate("adquisicion").toLocalDate());
               a.setRazonRechazo(rs.getString("razonRechazo"));
               a.setSolicitante(new Usuario(rs.getString("solicitante")));

                coleccion.add(a);                
            }
            tmp= new Respuesta(
                    Boolean.TRUE, 
                    CodigoRespuesta.CORRECTO, 
                    "Solicituds exitosamente encontrada.", 
                    coleccion.size()+" Solicituds exitosamente encontradas."
            );
            tmp.setListaDeResultados(coleccion);
            pstmt.setFetchSize(0);
            rs.close();           
            pstmt.close();
        } catch (PSQLException e){
            tmp= new Respuesta(
                    Boolean.FALSE, 
                    CodigoRespuesta.ERROR_INTERNO, 
                    "Error listando Solicitudes", 
                    "Error Postgresql: "+e.toString()
            );      
        } catch (SQLException e) {
          e.printStackTrace();

            //throw new GlobalException("Sentencia no valida");
        } finally {
            try {
                if (rs != null) {
                    rs.close();
                }
                if (pstmt != null) {
                    pstmt.close();
                }
                desconectar();
            } catch (SQLException e) {
               // throw new GlobalException("Estatutos invalidos o nulos");
            }
        }
        /*if (coleccion == null || coleccion.size() == 0) {
            tmp = new Respuesta(
                Boolean.FALSE, 
                CodigoRespuesta.ERROR_NOENCONTRADO, 
                "No se encontro Solicitud", 
                "SolicitudDao/readall(Solicitud u) no result: "+coleccion.toString());
        }*/
        return tmp;
    }

}
