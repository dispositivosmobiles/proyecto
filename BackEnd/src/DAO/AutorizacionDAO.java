/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DAO;



import BussinessLogic.Autorizacion;
import BussinessLogic.Sede;
import java.sql.CallableStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Types;
import java.util.ArrayList;
import org.postgresql.util.PSQLException;
import util.CodigoRespuesta;
import util.Respuesta;



/**
 *
 * @author edva5
 */
public class AutorizacionDAO extends DAO {
   private static AutorizacionDAO INSTANCE;
    private static final String CREATE = "{call schema_inventory.CREATEautorizacion(?,?)}";    
    private static final String READ = "{?=call schema_inventory.READautorizacion(?)}";
    private static final String READ_ALL = "{?=call schema_inventory.READALLautorizacion()}";
    private static final String UPDATE = "{call schema_inventory.UPDATEautorizacion(?,?)}";
    private static final String DELETE = "{call schema_inventory.DELETEautorizacion(?)}";

    private AutorizacionDAO(){
         super();
     }

     public static AutorizacionDAO getInstance(){
         if (INSTANCE==null)
             INSTANCE=new AutorizacionDAO();
         return INSTANCE;
     }

    public Respuesta create(Autorizacion u) {
        Respuesta tmp = new Respuesta(
                Boolean.FALSE, 
                CodigoRespuesta.ERROR_INTERNO, 
                "Error al crear autorizacion", 
                "AutorizacionDao/create(Autorizacion u)");
        try {
            conectar();
            
        } catch (Exception e) {            
            tmp= new Respuesta(
                    Boolean.FALSE, 
                    CodigoRespuesta.ERROR_ACCESO, 
                    "Error de Conexion: ", 
                    "Error de Conexion a la base de datos: "+e.toString()
            );
//            throw new GlobalException("No se ha localizado el driver");
        }
         CallableStatement pstmt=null;          
         try {
            pstmt = conexion.prepareCall(CREATE);                                                
            pstmt.setInt(1, u.getId());
            pstmt.setString(2, u.getSedeDestino().getNombre());                        
            boolean resultado = pstmt.execute();
            if (resultado == true)
                 tmp= new Respuesta(
                    Boolean.TRUE, 
                    CodigoRespuesta.CORRECTO, 
                    "Autorizacion exitosamente salvado.", 
                    u.toString()+" exitosamente salvado"
            );
         }catch(PSQLException e){
              tmp= new Respuesta(
                    Boolean.FALSE, 
                    CodigoRespuesta.ERROR_INTERNO, 
                    "Error salvando autorizacion", 
                    "Error Postgresql: "+e.toString()
            );      
                    }
            catch (SQLException e) {
            tmp= new Respuesta(
                    Boolean.FALSE, 
                    CodigoRespuesta.ERROR_ACCESO, 
                    "Error salvando autorizacion", 
                    e.toString()
            );
          //  throw new GlobalException("Llave duplicada");
        } 
         finally {
            try {
                if (pstmt != null)
                    pstmt.close();                                    
                desconectar();
            } catch (SQLException e) {
                tmp= new Respuesta(
                    Boolean.FALSE, 
                    CodigoRespuesta.ERROR_ACCESO, 
                    "Error cerrando conexion", 
                    e.toString()
            );
              //  throw new GlobalException("Estatutos invalidos o nulos");
            }
        }
         return tmp;
    }

    public Respuesta read(Autorizacion u) {
        Respuesta tmp = new Respuesta(
                Boolean.FALSE, 
                CodigoRespuesta.ERROR_INTERNO, 
                "Error al leer autorizacion", 
                "AutorizacionDao/read(Autorizacion u)");
        try {
            conectar();
            
        } catch (Exception e) {
            tmp= new Respuesta(
                    Boolean.FALSE, 
                    CodigoRespuesta.ERROR_ACCESO, 
                    "Error de Conexion", 
                    "Error de Conexion a la base de datos: "+e.getMessage()
            );
        }
        ResultSet rs = null;
        ArrayList coleccion = new ArrayList();
        Autorizacion a = null;
        CallableStatement pstmt=null;
        try {
            this.conexion.setAutoCommit(false);
            
            pstmt = this.conexion.prepareCall(READ);
            pstmt.setFetchSize(1);
            pstmt.registerOutParameter(1, Types.OTHER);
            pstmt.setInt(2,u.getId());
            
            pstmt.execute();
            rs = (ResultSet)pstmt.getObject(1);
            while (rs.next()) {
               a = new Autorizacion();
               a.setId(rs.getInt("id"));
               a.setSedeOrigen(new Sede(rs.getString("SedeOrigen")));
               a.setSedeDestino(new Sede(rs.getString("SedeDestino")));
               

                coleccion.add(a);
                tmp= new Respuesta(
                    Boolean.TRUE, 
                    CodigoRespuesta.CORRECTO, 
                    "Autorizacion exitosamente encontrada.", 
                    a.toString()+" exitosamente encontrado",
                        a
            );
            }
            pstmt.setFetchSize(0);
            rs.close();           
            pstmt.close();
        } catch (SQLException e) {
          e.printStackTrace();

            //throw new GlobalException("Sentencia no valida");
        } finally {
            try {
                if (rs != null) {
                    rs.close();
                }
                if (pstmt != null) {
                    pstmt.close();
                }
                desconectar();
            } catch (SQLException e) {
               // throw new GlobalException("Estatutos invalidos o nulos");
            }
        }
        if (coleccion == null || coleccion.size() == 0) {
            tmp = new Respuesta(
                Boolean.FALSE, 
                CodigoRespuesta.ERROR_NOENCONTRADO, 
                "No se encontro autorizacion", 
                "AutorizacionDao/read(Autorizacion u) no result"+coleccion);
        }
        return tmp;
    }
    public Respuesta update(Autorizacion u) {
        Respuesta tmp = new Respuesta(
                Boolean.FALSE, 
                CodigoRespuesta.ERROR_INTERNO, 
                "Error al actualizar autorizacion", 
                "AutorizacionDao/update(Autorizacion u)");
        try {
            conectar();
        } catch (Exception e) {            
            tmp= new Respuesta(
                    Boolean.FALSE, 
                    CodigoRespuesta.ERROR_ACCESO, 
                    "Error de Conexion", 
                    "Error de Conexion a la base de datos: "+e.toString()
            );
//            throw new GlobalException("No se ha localizado el driver");
        }
         CallableStatement pstmt=null;          
         try {
            pstmt = conexion.prepareCall(UPDATE);  
            pstmt.setInt(1, u.getId());
            pstmt.setString(2, u.getSedeDestino().getNombre());
            pstmt.setString(3, u.getSedeOrigen().getNombre());
            
            boolean resultado = pstmt.execute();
            if (resultado == true)
                 tmp= new Respuesta(
                    Boolean.TRUE, 
                    CodigoRespuesta.CORRECTO, 
                    "Autorizacion exitosamente actualizada.", 
                    u.toString()+" exitosamente actualizado"
            );
         }catch(PSQLException e){
              tmp= new Respuesta(
                    Boolean.FALSE, 
                    CodigoRespuesta.ERROR_INTERNO, 
                    "Error Actualizando autorizacion", 
                    "Error Postgresql: "+e.toString()
            );      
                    }
            catch (SQLException e) {
            tmp= new Respuesta(
                    Boolean.FALSE, 
                    CodigoRespuesta.ERROR_ACCESO, 
                    "Error ctualizando autorizacion:Error SQL:", 
                    e.toString()
            );
          //  throw new GlobalException("Llave duplicada");
        } 
         finally {
            try {
                if (pstmt != null)
                    pstmt.close();                                    
                desconectar();
            } catch (SQLException e) {
                tmp= new Respuesta(
                    Boolean.FALSE, 
                    CodigoRespuesta.ERROR_ACCESO, 
                    "Error cerrando conexion: update autorizacion", 
                    e.toString()
            );
              //  throw new GlobalException("Estatutos invalidos o nulos");
            }
        }
         return tmp;
    }
public Respuesta delete(Autorizacion u) {
        Respuesta tmp = new Respuesta(
                Boolean.FALSE, 
                CodigoRespuesta.ERROR_INTERNO, 
                "Error al eliminar autorizacion", 
                "AutorizacionDao/delete(Autorizacion u)");
        try {
            conectar();
        } catch (Exception e) {            
            tmp= new Respuesta(
                    Boolean.FALSE, 
                    CodigoRespuesta.ERROR_ACCESO, 
                    "Error de Conexion", 
                    "Error de Conexion a la base de datos: "+e.toString()
            );
//            throw new GlobalException("No se ha localizado el driver");
        }
         CallableStatement pstmt=null;          
         try {
            pstmt = conexion.prepareCall(DELETE);                                                
            pstmt.setInt(1, u.getId());                       
            boolean resultado = pstmt.execute();
            if (resultado == true){
                 tmp= new Respuesta(
                    Boolean.TRUE, 
                    CodigoRespuesta.CORRECTO, 
                    "Autorizacion exitosamente eliminada.", 
                    '{'+"codigo:"+u.getId()+" exitosamente eliminado"
            );}
         }catch(PSQLException e){
              tmp= new Respuesta(
                    Boolean.FALSE, 
                    CodigoRespuesta.ERROR_INTERNO, 
                    "Error eliminando autorizacion", 
                    "Error Postgresql: "+e.toString()
            );      
                    }
            catch (SQLException e) {
            tmp= new Respuesta(
                    Boolean.FALSE, 
                    CodigoRespuesta.ERROR_ACCESO, 
                    "Error eliminando autorizacion", 
                    e.toString()
            );
          //  throw new GlobalException("Llave duplicada");
        } 
         finally {
            try {
                if (pstmt != null)
                    pstmt.close();                                    
                desconectar();
            } catch (SQLException e) {
                tmp= new Respuesta(
                    Boolean.FALSE, 
                    CodigoRespuesta.ERROR_ACCESO, 
                    "Error cerrando conexion", 
                    e.toString()
            );
              //  throw new GlobalException("Estatutos invalidos o nulos");
            }
        }
         return tmp;
    }

    public Respuesta readAll() {
        Respuesta tmp = new Respuesta(
                Boolean.FALSE, 
                CodigoRespuesta.ERROR_INTERNO, 
                "Error al leer autorizacions", 
                "AutorizacionDao/readall(Autorizacion u)");
        try {
            conectar();
            
        } catch (Exception e) {
            tmp= new Respuesta(
                    Boolean.FALSE, 
                    CodigoRespuesta.ERROR_ACCESO, 
                    "Error de Conexion", 
                    "Error de Conexion a la base de datos: "+e.getMessage()
            );
        }
        ResultSet rs = null;
        ArrayList coleccion = new ArrayList();
        Autorizacion a = null;
        CallableStatement pstmt=null;
        try {
            this.conexion.setAutoCommit(false);
            
            pstmt = this.conexion.prepareCall(READ_ALL);
            pstmt.setFetchSize(50);
            pstmt.registerOutParameter(1, Types.OTHER);            
            
            pstmt.execute();
            rs = (ResultSet)pstmt.getObject(1);
            while (rs.next()) {
                a = new Autorizacion();
               a.setId(rs.getInt("id"));
               a.setSedeOrigen(new Sede(rs.getString("SedeOrigen")));
               a.setSedeDestino(new Sede(rs.getString("SedeDestino")));

                coleccion.add(a);                
            }
            tmp= new Respuesta(
                    Boolean.TRUE, 
                    CodigoRespuesta.CORRECTO, 
                    "Autorizacions exitosamente encontrada.", 
                    coleccion.size()+" autorizacions exitosamente encontradas."
            );
            tmp.setListaDeResultados(coleccion);
            pstmt.setFetchSize(0);
            rs.close();           
            pstmt.close();
        } catch (PSQLException e){
            tmp= new Respuesta(
                    Boolean.FALSE, 
                    CodigoRespuesta.ERROR_INTERNO, 
                    "Error listando autorizacions", 
                    "Error Postgresql: "+e.toString()
            );      
        } catch (SQLException e) {
          e.printStackTrace();

            //throw new GlobalException("Sentencia no valida");
        } finally {
            try {
                if (rs != null) {
                    rs.close();
                }
                if (pstmt != null) {
                    pstmt.close();
                }
                desconectar();
            } catch (SQLException e) {
               // throw new GlobalException("Estatutos invalidos o nulos");
            }
        }
        /*if (coleccion == null || coleccion.size() == 0) {
            tmp = new Respuesta(
                Boolean.FALSE, 
                CodigoRespuesta.ERROR_NOENCONTRADO, 
                "No se encontro autorizacion", 
                "AutorizacionDao/readall(Autorizacion u) no result: "+coleccion.toString());
        }*/
        return tmp;
    }

}
