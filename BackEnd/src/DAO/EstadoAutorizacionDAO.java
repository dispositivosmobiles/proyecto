/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DAO;



import BussinessLogic.EstadoAutorizacion;
import BussinessLogic.Provincia;
import BussinessLogic.Sede;
import java.sql.CallableStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Types;
import java.util.ArrayList;
import org.postgresql.util.PSQLException;
import util.CodigoRespuesta;
import util.Respuesta;



/**
 *
 * @author edva5
 */
public class EstadoAutorizacionDAO extends DAO {
   private static EstadoAutorizacionDAO INSTANCE;    
    private static final String READ_ALL = "{?=call schema_inventory.READALLEstadoAutorizacion()}";        

    private EstadoAutorizacionDAO(){
         super();
     }

     public static EstadoAutorizacionDAO getInstance(){
         if (INSTANCE==null)
             INSTANCE=new EstadoAutorizacionDAO();
         return INSTANCE;
     }

    
    public Respuesta readAll() {
        Respuesta tmp = new Respuesta(
                Boolean.FALSE, 
                CodigoRespuesta.ERROR_INTERNO, 
                "Error al leer estados", 
                "EstadoAutorizacionDao/readall(EstadoAutorizacion u)");
        try {
            conectar();
            
        } catch (Exception e) {
            tmp= new Respuesta(
                    Boolean.FALSE, 
                    CodigoRespuesta.ERROR_ACCESO, 
                    "Error de Conexion", 
                    "Error de Conexion a la base de datos: "+e.getMessage()
            );
        }
        ResultSet rs = null;
        ArrayList coleccion = new ArrayList();
        EstadoAutorizacion a = null;
        CallableStatement pstmt=null;
        try {
            this.conexion.setAutoCommit(false);
            
            pstmt = this.conexion.prepareCall(READ_ALL);
            pstmt.setFetchSize(50);
            pstmt.registerOutParameter(1, Types.OTHER);            
            
            pstmt.execute();
            rs = (ResultSet)pstmt.getObject(1);
            while (rs.next()) {
                a = new EstadoAutorizacion(rs.getString("nombre"));

                coleccion.add(a);                
            }
            tmp= new Respuesta(
                    Boolean.TRUE, 
                    CodigoRespuesta.CORRECTO, 
                    "EstadoAutorizacions exitosamente encontrada.", 
                    coleccion.size()+" estados exitosamente encontradas."
            );
            tmp.setListaDeResultados(coleccion);
            pstmt.setFetchSize(0);
            rs.close();           
            pstmt.close();
        } catch (PSQLException e){
            tmp= new Respuesta(
                    Boolean.FALSE, 
                    CodigoRespuesta.ERROR_INTERNO, 
                    "Error listando estados", 
                    "Error Postgresql: "+e.getMessage()
            );      
        } catch (SQLException e) {
          e.printStackTrace();

            //throw new GlobalException("Sentencia no valida");
        } finally {
            try {
                if (rs != null) {
                    rs.close();
                }
                if (pstmt != null) {
                    pstmt.close();
                }
                desconectar();
            } catch (SQLException e) {
               // throw new GlobalException("Estatutos invalidos o nulos");
            }
        }
        /*if (coleccion == null || coleccion.size() == 0) {
            tmp = new Respuesta(
                Boolean.FALSE, 
                CodigoRespuesta.ERROR_NOENCONTRADO, 
                "No se encontro estado", 
                "EstadoAutorizacionDao/readall(EstadoAutorizacion u) no result: "+coleccion.toString());
        }*/
        return tmp;
    }

}
