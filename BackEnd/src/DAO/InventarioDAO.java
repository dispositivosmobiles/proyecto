/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DAO;



import BussinessLogic.Color;
import BussinessLogic.Inventario;
import BussinessLogic.Sede;
import BussinessLogic.Tipo;
import java.sql.CallableStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Types;
import java.util.ArrayList;
import org.postgresql.util.PSQLException;
import util.CodigoRespuesta;
import util.Respuesta;



/**
 *
 * @author edva5
 */
public class InventarioDAO extends DAO {
   private static InventarioDAO INSTANCE;
    private static final String CREATE = "{call schema_inventory.CREATEInventario(?,?,?,?,?,?)}";    
    private static final String READ = "{?=call schema_inventory.READInventario(?)}";
    private static final String READ_ALL = "{?=call schema_inventory.READALLInventario()}";
    private static final String READ_BY_SOLICITUD = "{?=call schema_inventory.READInventarioBySolicitud()}";
    private static final String UPDATE = "{call schema_inventory.UPDATEInventario(?,?,?,?,?,?,?,?)}";
    private static final String DELETE = "{call schema_inventory.DELETEInventario(?)}";

    private InventarioDAO(){
         super();
     }

     public static InventarioDAO getInstance(){
         if (INSTANCE==null)
             INSTANCE=new InventarioDAO();
         return INSTANCE;
     }

    public Respuesta create(Inventario u) {
        Respuesta tmp = new Respuesta(
                Boolean.FALSE, 
                CodigoRespuesta.ERROR_INTERNO, 
                "Error al crear Inventario", 
                "InventarioDao/create(Inventario u)");
        try {
            conectar();
        } catch (Exception e) {            
            tmp= new Respuesta(
                    Boolean.FALSE, 
                    CodigoRespuesta.ERROR_ACCESO, 
                    "Error de Conexion", 
                    "Error de Conexion a la base de datos"
            );
//            throw new GlobalException("No se ha localizado el driver");
        }
         CallableStatement pstmt=null;          
         try {
            pstmt = conexion.prepareCall(CREATE);                                                
            pstmt.setInt(1, u.getSolicitud());
                                 
            pstmt.setString(2, u.getTipo().getNombre());
            pstmt.setString(3, u.getColor().getNombre());
            pstmt.setString(4, u.getMarca());
            pstmt.setString(5, u.getModelo());
            pstmt.setString(6, u.getNoSerial());
            boolean resultado = pstmt.execute();
            if (resultado == true)
                 tmp= new Respuesta(
                    Boolean.TRUE, 
                    CodigoRespuesta.CORRECTO, 
                    "Inventario exitosamente salvado.", 
                    u.toString()+" exitosamente salvado"
            );
         }catch(PSQLException e){
              tmp= new Respuesta(
                    Boolean.FALSE, 
                    CodigoRespuesta.ERROR_INTERNO, 
                    "Error salvando Inventario", 
                    "Error Postgresql: "+e.toString()
            );      
                    }
            catch (SQLException e) {
            tmp= new Respuesta(
                    Boolean.FALSE, 
                    CodigoRespuesta.ERROR_ACCESO, 
                    "Error salvando Inventario", 
                    e.toString()
            );
          //  throw new GlobalException("Llave duplicada");
        } 
         finally {
            try {
                if (pstmt != null)
                    pstmt.close();                                    
                desconectar();
            } catch (SQLException e) {
                tmp= new Respuesta(
                    Boolean.FALSE, 
                    CodigoRespuesta.ERROR_ACCESO, 
                    "Error cerrando conexion", 
                    e.toString()
            );
              //  throw new GlobalException("Estatutos invalidos o nulos");
            }
        }
         return tmp;
    }

    public Respuesta read(Inventario u) {
        Respuesta tmp = new Respuesta(
                Boolean.FALSE, 
                CodigoRespuesta.ERROR_INTERNO, 
                "Error al leer Inventario", 
                "InventarioDao/read(Inventario u)");
        try {
            conectar();
            
        } catch (Exception e) {
            tmp= new Respuesta(
                    Boolean.FALSE, 
                    CodigoRespuesta.ERROR_ACCESO, 
                    "Error de Conexion", 
                    "Error de Conexion a la base de datos"
            );
        }
        ResultSet rs = null;
        ArrayList coleccion = new ArrayList();
        Inventario a = null;
        CallableStatement pstmt=null;
        try {
            this.conexion.setAutoCommit(false);
            
            pstmt = this.conexion.prepareCall(READ);
            pstmt.setFetchSize(1);
            pstmt.registerOutParameter(1, Types.OTHER);
            pstmt.setInt(2,u.getId());
            
            pstmt.execute();
            rs = (ResultSet)pstmt.getObject(1);
            while (rs.next()) {
               a = new Inventario();
               a.setId(rs.getInt("Solicitud"));
               a.setSede(new Sede(rs.getString("Sede")));
               a.setTipo(new Tipo(rs.getString("Tipo")));
               a.setMarca(rs.getString("marca"));
               a.setModelo(rs.getString("modelo"));
               a.setNoSerial(rs.getString("NoSerial"));
               a.setApproved(rs.getBoolean("approved"));
               

                coleccion.add(a);
                tmp= new Respuesta(
                    Boolean.TRUE, 
                    CodigoRespuesta.CORRECTO, 
                    "Inventario exitosamente encontrada.", 
                    a.toString()+" exitosamente encontrado",
                        a
            );
            }
            pstmt.setFetchSize(0);
            rs.close();           
            pstmt.close();
        } catch (SQLException e) {
              return new Respuesta(
                    Boolean.TRUE, 
                    CodigoRespuesta.CORRECTO, 
                    "Solicitud exitosamente encontrada.", 
                    e.toString()
            );

            //throw new GlobalException("Sentencia no valida");
        } finally {
            try {
                if (rs != null) {
                    rs.close();
                }
                if (pstmt != null) {
                    pstmt.close();
                }
                desconectar();
            } catch (SQLException e) {
               // throw new GlobalException("Estatutos invalidos o nulos");
            }
        }
        if (coleccion == null || coleccion.size() == 0) {
            tmp = new Respuesta(
                Boolean.FALSE, 
                CodigoRespuesta.ERROR_NOENCONTRADO, 
                "No se encontro Inventario", 
                "InventarioDao/read(Inventario u) no result"+coleccion);
        }
        return tmp;
    }
    public Respuesta update(Inventario u) {
        Respuesta tmp = new Respuesta(
                Boolean.FALSE, 
                CodigoRespuesta.ERROR_INTERNO, 
                "Error al actualizar Inventario", 
                "InventarioDao/update(Inventario u)");
        try {
            conectar();
        } catch (Exception e) {            
            tmp= new Respuesta(
                    Boolean.FALSE, 
                    CodigoRespuesta.ERROR_ACCESO, 
                    "Error de Conexion", 
                    "Error de Conexion a la base de datos"
            );
//            throw new GlobalException("No se ha localizado el driver");
        }
         CallableStatement pstmt=null;          
         try {
            pstmt = conexion.prepareCall(UPDATE);  
            pstmt.setInt(1, u.getId());
            pstmt.setString(2, u.getSede().getNombre());                        
            pstmt.setString(3, u.getTipo().getNombre());
            pstmt.setString(4, u.getColor().getNombre());
            pstmt.setString(5, u.getMarca());
            pstmt.setString(6, u.getModelo());
            pstmt.setString(7, u.getNoSerial());
            pstmt.setBoolean(8, u.isApproved());
            
            boolean resultado = pstmt.execute();
            if (resultado == true)
                 tmp= new Respuesta(
                    Boolean.TRUE, 
                    CodigoRespuesta.CORRECTO, 
                    "Inventario exitosamente actualizada.", 
                    " exitosamente actualizado"
            );
         }catch(PSQLException e){
              tmp= new Respuesta(
                    Boolean.FALSE, 
                    CodigoRespuesta.ERROR_INTERNO, 
                    "Error ctualizando Inventario", 
                    "Error Postgresql: "+e.toString()
            );      
                    }
            catch (SQLException e) {
            tmp= new Respuesta(
                    Boolean.FALSE, 
                    CodigoRespuesta.ERROR_ACCESO, 
                    "Error ctualizando Inventario", 
                    e.toString()
            );
          //  throw new GlobalException("Llave duplicada");
        } 
         finally {
            try {
                if (pstmt != null)
                    pstmt.close();                                    
                desconectar();
            } catch (SQLException e) {
                tmp= new Respuesta(
                    Boolean.FALSE, 
                    CodigoRespuesta.ERROR_ACCESO, 
                    "Error cerrando conexion: update Inventario", 
                    e.toString()
            );
              //  throw new GlobalException("Estatutos invalidos o nulos");
            }
        }
         return tmp;
    }
public Respuesta delete(Inventario u) {
        Respuesta tmp = new Respuesta(
                Boolean.FALSE, 
                CodigoRespuesta.ERROR_INTERNO, 
                "Error al eliminar Inventario", 
                "InventarioDao/delete(Inventario u)");
        try {
            conectar();
        } catch (Exception e) {            
            tmp= new Respuesta(
                    Boolean.FALSE, 
                    CodigoRespuesta.ERROR_ACCESO, 
                    "Error de Conexion", 
                    "Error de Conexion a la base de datos: "+e.toString()
            );
//            throw new GlobalException("No se ha localizado el driver");
        }
         CallableStatement pstmt=null;          
         try {
            pstmt = conexion.prepareCall(DELETE);                                                
            pstmt.setInt(1, u.getId());                       
            boolean resultado = pstmt.execute();
            if (resultado == true){
                 tmp= new Respuesta(
                    Boolean.TRUE, 
                    CodigoRespuesta.CORRECTO, 
                    "Inventario exitosamente eliminada.", 
                    '{'+"codigo:"+u.getId()+" exitosamente eliminado"
            );}
         }catch(PSQLException e){
              tmp= new Respuesta(
                    Boolean.FALSE, 
                    CodigoRespuesta.ERROR_INTERNO, 
                    "Error eliminando Inventario", 
                    "Error Postgresql: "+e.toString()
            );      
                    }
            catch (SQLException e) {
            tmp= new Respuesta(
                    Boolean.FALSE, 
                    CodigoRespuesta.ERROR_ACCESO, 
                    "Error eliminando Inventario", 
                    e.toString()
            );
          //  throw new GlobalException("Llave duplicada");
        } 
         finally {
            try {
                if (pstmt != null)
                    pstmt.close();                                    
                desconectar();
            } catch (SQLException e) {
                tmp= new Respuesta(
                    Boolean.FALSE, 
                    CodigoRespuesta.ERROR_ACCESO, 
                    "Error cerrando conexion", 
                    e.toString()
            );
              //  throw new GlobalException("Estatutos invalidos o nulos");
            }
        }
         return tmp;
    }

    public Respuesta readAll() {
        Respuesta tmp = new Respuesta(
                Boolean.FALSE, 
                CodigoRespuesta.ERROR_INTERNO, 
                "Error al leer Inventarios", 
                "InventarioDao/readall(Inventario u)");
        try {
            conectar();
            
        } catch (Exception e) {
            tmp= new Respuesta(
                    Boolean.FALSE, 
                    CodigoRespuesta.ERROR_ACCESO, 
                    "Error de Conexion", 
                    "Error de Conexion a la base de datos: "+e.toString()
            );
        }
        ResultSet rs = null;
        ArrayList coleccion = new ArrayList();
        Inventario a = null;
        CallableStatement pstmt=null;
        try {
            this.conexion.setAutoCommit(false);
            
            pstmt = this.conexion.prepareCall(READ_ALL);
            pstmt.setFetchSize(50);
            pstmt.registerOutParameter(1, Types.OTHER);            
            
            pstmt.execute();
            rs = (ResultSet)pstmt.getObject(1);
            while (rs.next()) {
                
               a = new Inventario();
               a.setId(rs.getInt("id"));
               a.setSolicitud(rs.getInt("Solicitud"));
               a.setSede(new Sede(rs.getString("Sede")));
               a.setTipo(new Tipo(rs.getString("Tipo")));
               a.setMarca(rs.getString("marca"));
               a.setModelo(rs.getString("modelo"));
               a.setNoSerial(rs.getString("NoSerial"));
               a.setApproved(rs.getBoolean("approved"));
               a.setColor(new Color(rs.getString("color")));
                coleccion.add(a);                
            }
            tmp= new Respuesta(
                    Boolean.TRUE, 
                    CodigoRespuesta.CORRECTO, 
                    "Inventarios exitosamente encontrada.", 
                    coleccion.size()+" Inventarios exitosamente encontradas."
            );
            tmp.setListaDeResultados(coleccion);
            pstmt.setFetchSize(0);
            rs.close();           
            pstmt.close();
        } catch (PSQLException e){
            tmp= new Respuesta(
                    Boolean.FALSE, 
                    CodigoRespuesta.ERROR_INTERNO, 
                    "Error listando Inventarios", 
                    "Error Postgresql: "+e.toString()
            );      
        } catch (SQLException e) {
          e.printStackTrace();

            //throw new GlobalException("Sentencia no valida");
        } finally {
            try {
                if (rs != null) {
                    rs.close();
                }
                if (pstmt != null) {
                    pstmt.close();
                }
                desconectar();
            } catch (SQLException e) {
               // throw new GlobalException("Estatutos invalidos o nulos");
            }
        }
        /*if (coleccion == null || coleccion.size() == 0) {
            tmp = new Respuesta(
                Boolean.FALSE, 
                CodigoRespuesta.ERROR_NOENCONTRADO, 
                "No se encontro Inventario", 
                "InventarioDao/readall(Inventario u) no result: "+coleccion.toString());
        }*/
        return tmp;
    }

}
