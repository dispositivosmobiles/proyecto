/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DAO;



import BussinessLogic.Tipo;
import BussinessLogic.Sede;
import java.sql.CallableStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Types;
import java.util.ArrayList;
import org.postgresql.util.PSQLException;
import util.CodigoRespuesta;
import util.Respuesta;



/**
 *
 * @author edva5
 */
public class TipoDAO extends DAO {
   private static TipoDAO INSTANCE;
    private static final String CREATE = "{call schema_inventory.CREATEtipo(?)}";    
    private static final String READ = "{?=call schema_inventory.READtipo(?)}";
    private static final String READ_ALL = "{?=call schema_inventory.READALLtipo()}";    
    private static final String DELETE = "{call schema_inventory.DELETEtipo(?)}";

    private TipoDAO(){
         super();
     }

     public static TipoDAO getInstance(){
         if (INSTANCE==null)
             INSTANCE=new TipoDAO();
         return INSTANCE;
     }

    public Respuesta create(Tipo u) {
        Respuesta tmp = new Respuesta(
                Boolean.FALSE, 
                CodigoRespuesta.ERROR_INTERNO, 
                "Error al crear tipo", 
                "TipoDao/create(Tipo u)");
        try {
            conectar();
            
        } catch (Exception e) {            
            tmp= new Respuesta(
                    Boolean.FALSE, 
                    CodigoRespuesta.ERROR_ACCESO, 
                    "Error de Conexion", 
                    "Error de Conexion a la base de datos: "+e.toString()
            );
//            throw new GlobalException("No se ha localizado el driver");
        }
         CallableStatement pstmt=null;          
         try {
            pstmt = conexion.prepareCall(CREATE);                                                            
            pstmt.setString(1, u.getNombre());                        
            boolean resultado = pstmt.execute();
            if (resultado == true)
                 tmp= new Respuesta(
                    Boolean.TRUE, 
                    CodigoRespuesta.CORRECTO, 
                    "Tipo exitosamente salvado.", 
                    u.toString()+" exitosamente salvado"
            );
         }catch(PSQLException e){
              tmp= new Respuesta(
                    Boolean.FALSE, 
                    CodigoRespuesta.ERROR_INTERNO, 
                    "Error salvando tipo", 
                    "Error Postgresql: "+e.toString()
            );      
                    }
            catch (SQLException e) {
            tmp= new Respuesta(
                    Boolean.FALSE, 
                    CodigoRespuesta.ERROR_ACCESO, 
                    "Error salvando tipo", 
                    e.toString()
            );
          //  throw new GlobalException("Llave duplicada");
        } 
         finally {
            try {
                if (pstmt != null)
                    pstmt.close();                                    
                desconectar();
            } catch (SQLException e) {
                tmp= new Respuesta(
                    Boolean.FALSE, 
                    CodigoRespuesta.ERROR_ACCESO, 
                    "Error cerrando conexion", 
                    e.toString()
            );
              //  throw new GlobalException("Estatutos invalidos o nulos");
            }
        }
         return tmp;
    }

    public Respuesta read(Tipo u) {
        Respuesta tmp = new Respuesta(
                Boolean.FALSE, 
                CodigoRespuesta.ERROR_INTERNO, 
                "Error al leer tipo", 
                "TipoDao/read(Tipo u)");
        try {
            conectar();
            
        } catch (Exception e) {
            tmp= new Respuesta(
                    Boolean.FALSE, 
                    CodigoRespuesta.ERROR_ACCESO, 
                    "Error de Conexion", 
                    "Error de Conexion a la base de datos: "+e.toString()
            );
        }
        ResultSet rs = null;
        ArrayList coleccion = new ArrayList();
        Tipo a = null;
        CallableStatement pstmt=null;
        try {
            this.conexion.setAutoCommit(false);
            
            pstmt = this.conexion.prepareCall(READ);
            pstmt.setFetchSize(1);
            pstmt.registerOutParameter(1, Types.OTHER);
            pstmt.setString(2,u.getNombre());
            
            pstmt.execute();
            rs = (ResultSet)pstmt.getObject(1);
            while (rs.next()) {
               a = new Tipo(rs.getString("nombre"));
               
      
               

                coleccion.add(a);
                tmp= new Respuesta(
                    Boolean.TRUE, 
                    CodigoRespuesta.CORRECTO, 
                    "Tipo exitosamente encontrada.", 
                    a.toString()+" exitosamente encontrado",
                        a
            );
            }
            pstmt.setFetchSize(0);
            rs.close();           
            pstmt.close();
        } catch (SQLException e) {
          e.printStackTrace();

            //throw new GlobalException("Sentencia no valida");
        } finally {
            try {
                if (rs != null) {
                    rs.close();
                }
                if (pstmt != null) {
                    pstmt.close();
                }
                desconectar();
            } catch (SQLException e) {
               // throw new GlobalException("Estatutos invalidos o nulos");
            }
        }
        if (coleccion == null || coleccion.size() == 0) {
            tmp = new Respuesta(
                Boolean.FALSE, 
                CodigoRespuesta.ERROR_NOENCONTRADO, 
                "No se encontro tipo", 
                "TipoDao/read(Tipo u) no result"+coleccion);
        }
        return tmp;
    }
    
public Respuesta delete(Tipo u) {
        Respuesta tmp = new Respuesta(
                Boolean.FALSE, 
                CodigoRespuesta.ERROR_INTERNO, 
                "Error al eliminar tipo", 
                "TipoDao/delete(Tipo u)");
        try {
            conectar();
        } catch (Exception e) {            
            tmp= new Respuesta(
                    Boolean.FALSE, 
                    CodigoRespuesta.ERROR_ACCESO, 
                    "Error de Conexion", 
                    "Error de Conexion a la base de datos: "+e.toString()
            );
//            throw new GlobalException("No se ha localizado el driver");
        }
         CallableStatement pstmt=null;          
         try {
            pstmt = conexion.prepareCall(DELETE);                                                
            pstmt.setString(1, u.getNombre());                       
            boolean resultado = pstmt.execute();
            if (resultado == true){
                 tmp= new Respuesta(
                    Boolean.TRUE, 
                    CodigoRespuesta.CORRECTO, 
                    "Tipo exitosamente eliminada.", 
                    '{'+"nombre:"+u.getNombre()+" exitosamente eliminado"
            );}
         }catch(PSQLException e){
              tmp= new Respuesta(
                    Boolean.FALSE, 
                    CodigoRespuesta.ERROR_INTERNO, 
                    "Error eliminando tipo", 
                    "Error Postgresql: "+e.toString()
            );      
                    }
            catch (SQLException e) {
            tmp= new Respuesta(
                    Boolean.FALSE, 
                    CodigoRespuesta.ERROR_ACCESO, 
                    "Error eliminando tipo", 
                    e.toString()
            );
          //  throw new GlobalException("Llave duplicada");
        } 
         finally {
            try {
                if (pstmt != null)
                    pstmt.close();                                    
                desconectar();
            } catch (SQLException e) {
                tmp= new Respuesta(
                    Boolean.FALSE, 
                    CodigoRespuesta.ERROR_ACCESO, 
                    "Error cerrando conexion", 
                    e.toString()
            );
              //  throw new GlobalException("Estatutos invalidos o nulos");
            }
        }
         return tmp;
    }

    public Respuesta readAll() {
        Respuesta tmp = new Respuesta(
                Boolean.FALSE, 
                CodigoRespuesta.ERROR_INTERNO, 
                "Error al leer tipos", 
                "TipoDao/readall(Tipo u)");
        try {
            conectar();
            
        } catch (Exception e) {
            tmp= new Respuesta(
                    Boolean.FALSE, 
                    CodigoRespuesta.ERROR_ACCESO, 
                    "Error de Conexion", 
                    "Error de Conexion a la base de datos: "+e.toString()
            );
        }
        ResultSet rs = null;
        ArrayList coleccion = new ArrayList();
        Tipo a = null;
        CallableStatement pstmt=null;
        try {
            this.conexion.setAutoCommit(false);
            
            pstmt = this.conexion.prepareCall(READ_ALL);
            pstmt.setFetchSize(50);
            pstmt.registerOutParameter(1, Types.OTHER);            
            
            pstmt.execute();
            rs = (ResultSet)pstmt.getObject(1);
            while (rs.next()) {
                a = new Tipo(rs.getString("nombre"));

                coleccion.add(a);                
            }
            tmp= new Respuesta(
                    Boolean.TRUE, 
                    CodigoRespuesta.CORRECTO, 
                    "Tipos exitosamente encontrada.", 
                    coleccion.size()+" tipos exitosamente encontradas."
            );
            tmp.setListaDeResultados(coleccion);
            pstmt.setFetchSize(0);
            rs.close();           
            pstmt.close();
        } catch (PSQLException e){
            tmp= new Respuesta(
                    Boolean.FALSE, 
                    CodigoRespuesta.ERROR_INTERNO, 
                    "Error listando tipos", 
                    "Error Postgresql: "+e.toString()
            );      
        } catch (SQLException e) {
          e.printStackTrace();

            //throw new GlobalException("Sentencia no valida");
        } finally {
            try {
                if (rs != null) {
                    rs.close();
                }
                if (pstmt != null) {
                    pstmt.close();
                }
                desconectar();
            } catch (SQLException e) {
               // throw new GlobalException("Estatutos invalidos o nulos");
            }
        }
        /*if (coleccion == null || coleccion.size() == 0) {
            tmp = new Respuesta(
                Boolean.FALSE, 
                CodigoRespuesta.ERROR_NOENCONTRADO, 
                "No se encontro tipo", 
                "TipoDao/readall(Tipo u) no result: "+coleccion.toString());
        }*/
        return tmp;
    }

}
