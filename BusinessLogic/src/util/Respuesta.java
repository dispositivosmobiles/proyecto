/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package util;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 *
 * @author Eduardo Sánchez
 */
public class Respuesta implements Serializable {
    
    private static final long serialVersionUID = 1L;
    
    /**
     * Estado de la respuesta (true = Correcto, false = Error).
     */
    private Boolean estado;

    /**
     * Código de la respuesta (enum).
     */
    private CodigoRespuesta codigoRespuesta;   

    /**
     * Mensaje de la respuesta.
     */
    private String mensaje;    

    /**
     * Mensaje interno de la respuesta.
     */
    private String mensajeInterno;

    private HashMap<String, Object> resultado; 
    
    private List<Object> listaDeResultados;

    /**
     * Constructor por defecto.
     */
    public Respuesta() {
        this.resultado = new HashMap<>();
    }

    /**
     * Constructor de la respuesta sin ingresar los resultados.
     * @param estado Estado
     * @param codigoRespuesta Código de la respuesta
     * @param mensaje Mensaje generado
     * @param mensajeInterno Mensaje interno
     */
    public Respuesta(Boolean estado, CodigoRespuesta codigoRespuesta, String mensaje, String mensajeInterno) {
        this.estado = estado;
        this.codigoRespuesta = codigoRespuesta;
        this.mensaje = mensaje;
        this.mensajeInterno = mensajeInterno;
        this.resultado = new HashMap<>();
    }
    
    /**
     * Constructor completo de la respuesta
     * @param estado Estado
     * @param codigoRespuesta Código de la respuesta
     * @param mensaje Mensaje generado
     * @param mensajeInterno Mensaje interno
     * @param nombre Nombre del resultado
     * @param resultado Resultado
     */
    
    
    /**
     * Constructor completo de la respuesta
     * @param estado Estado
     * @param codigoRespuesta Código de la respuesta
     * @param mensaje Mensaje generado
     * @param mensajeInterno Mensaje interno
     * @param resultado Resultado
     */
    public Respuesta(Boolean estado, CodigoRespuesta codigoRespuesta, String mensaje, String mensajeInterno, Object resultado) {
        this.estado = estado;
        //this.codigoRespuesta = codigoRespuesta;
        this.mensaje = mensaje;
        this.mensajeInterno = mensajeInterno;
        this.resultado = new HashMap<>();
        this.resultado.put("[Objeto]", resultado);
    }
    
    /**
     * Obtiene el estado de la respuesta
     * @return Estado
     */
    public Boolean getEstado() {
        return estado;
    }

    /**
     * Asigna un estado
     * @param estado Estado
     */
    public void setEstado(Boolean estado) {
        this.estado = estado;
    }

    /**
     * Obtiene el código de la respuesta
     * @return Código de la respuesta
     */
    public CodigoRespuesta getCodigoRespuesta() {
        return codigoRespuesta;
    }

    /**
     * Asigna el código de la respuesta
     * @param codigoRespuesta Código de la respuesta
     */
    public void setCodigoRespuesta(CodigoRespuesta codigoRespuesta) {
        this.codigoRespuesta = codigoRespuesta;
    }

    /**
     * Obtiene el mensaje de la respuesta
     * @return Mensaje de la respuesta
     */
    public String getMensaje() {
        return mensaje;
    }

    /**
     * Asigna el mensaje de la respuesta
     * @param mensaje Mensaje de la respuesta
     */
    public void setMensaje(String mensaje) {
        this.mensaje = mensaje;
    }

    /**
     * Obtiene el mensaje interno de la respuesta
     * @return Mensaje interno de la respuesta
     */
    public String getMensajeInterno() {
        return mensajeInterno;
    }

    /**
     * Asigna el mensaje interno de la respuesta
     * @param mensajeInterno Mensaje interno de la respuesta
     */
    public void setMensajeInterno(String mensajeInterno) {
        this.mensajeInterno = mensajeInterno;
    }
    
    /**
     * Obtiene un resultado de la respuesta por el nombre ingresado
     * @param nombre Nombre del resultado
     * @return Resultado buscado
     */
    public Object getResultado(String nombre) {
        return resultado.get(nombre);
    }

    /**
     * Asigna un resultado a la respuesta
     * @param nombre Nombre del resultado
     * @param resultado Resultado
     */
    public void setResultado(String nombre, Object resultado) {
        this.resultado.put(nombre, resultado);
    }
    
    /**
     * Obtiene el resultado de la respuesta
     * @return Resultado buscado
     */
    public Object getResultado() {
        return resultado.get("[Objeto]");
    }

    /**
     * Asigna el resultado a la respuesta
     * @param resultado Resultado
     */
    public void setResultado(Object resultado) {
        this.resultado.put("[Objeto]", resultado);
    }
    public void setListaDeResultados(ArrayList<Object> resultados) {
        this.listaDeResultados = new ArrayList<>();
        this.listaDeResultados.addAll(resultados);
    }
    public List<Object> getListaDeResultados() {
        return this.listaDeResultados;
    }
}
