/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package BussinessLogic;

/**
 *
 * @author eddy
 */
public class Canton {
    private String nombre;
    private Provincia provincia;

    public Canton(String nombre, Provincia provincia) {
        this.nombre = nombre;
        this.provincia = provincia;
    }
    public Canton(String nombre) {
        this.nombre = nombre;        
    }

    public Canton() {
    }
    /**
     * @return the nombre
     */
    public String getNombre() {
        return nombre;
    }

    /**
     * @param nombre the nombre to set
     */
    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    /**
     * @return the provincia
     */
    public Provincia getProvincia() {
        return provincia;
    }

    /**
     * @param provincia the provincia to set
     */
    public void setProvincia(Provincia provincia) {
        this.provincia = provincia;
    }
    
    
}
