/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package BussinessLogic;

import java.io.Serializable;

/**
 *
 * @author eddy
 */
public class Inventario implements Serializable {

   
    private int id,Solicitud;    
    private Sede sede;
    private Tipo tipo;
    private Color color;
    private String marca,modelo,NoSerial;
    private boolean approved;

    public Inventario() {
    }

    public Inventario(int id) {
        this.id = id;        
    }
    
    /**
     * @return the sede
     */
    public Sede getSede() {
        return sede;
    }

    /**
     * @param sede the sede to set
     */
    public void setSede(Sede sede) {
        this.sede = sede;
    }

    /**
     * @return the tipo
     */
    public Tipo getTipo() {
        return tipo;
    }

    /**
     * @param tipo the tipo to set
     */
    public void setTipo(Tipo tipo) {
        this.tipo = tipo;
    }

    /**
     * @return the color
     */
    public Color getColor() {
        return color;
    }

    /**
     * @param color the color to set
     */
    public void setColor(Color color) {
        this.color = color;
    }
    
    

    /**
     * @return the id
     */
    public int getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(int id) {
        this.id = id;
    }

    /**
     * @return the Solicitud
     */
    public int getSolicitud() {
        return Solicitud;
    }

    /**
     * @param Solicitud the Solicitud to set
     */
    public void setSolicitud(int Solicitud) {
        this.Solicitud = Solicitud;
    }


    /**
     * @return the marca
     */
    public String getMarca() {
        return marca;
    }

    /**
     * @param marca the marca to set
     */
    public void setMarca(String marca) {
        this.marca = marca;
    }

    /**
     * @return the modelo
     */
    public String getModelo() {
        return modelo;
    }

    /**
     * @param modelo the modelo to set
     */
    public void setModelo(String modelo) {
        this.modelo = modelo;
    }

    /**
     * @return the NoSerial
     */
    public String getNoSerial() {
        return NoSerial;
    }

    /**
     * @param NoSerial the NoSerial to set
     */
    public void setNoSerial(String NoSerial) {
        this.NoSerial = NoSerial;
    }
     /**
     * @return the approved
     */
    public boolean isApproved() {
        return approved;
    }

    /**
     * @param approved the approved to set
     */
    public void setApproved(boolean approved) {
        this.approved = approved;
    }
}
