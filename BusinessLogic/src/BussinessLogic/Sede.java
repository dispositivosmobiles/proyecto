/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package BussinessLogic;

/**
 *
 * @author eddy
 */
public class Sede {
       private String nombre;
       private Canton canton;

    public Sede(String nombre, Canton canton) {
        this.nombre = nombre;
        this.canton = canton;
    }
    public Sede(String nombre) {
        this.nombre = nombre;        
    }
    
    /**
     * @return the nombre
     */
    public String getNombre() {
        return nombre;
    }

    /**
     * @param nombre the nombre to set
     */
    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    /**
     * @return the canton
     */
    public Canton getCanton() {
        return canton;
    }

    /**
     * @param canton the canton to set
     */
    public void setCanton(Canton canton) {
        this.canton = canton;
    }
      @Override
    public String toString() {
        return "{nombre:"+this.nombre+"}"; //To change body of generated methods, choose Tools | Templates.
    }
}
