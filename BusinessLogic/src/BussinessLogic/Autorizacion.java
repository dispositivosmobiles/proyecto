/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package BussinessLogic;

import java.io.Serializable;

/**
 *
 * @author eddy
 */
public class Autorizacion implements Serializable{
    private int id;
    private Inventario inventario;  
    private Sede SedeOrigen,SedeDestino;
    private EstadoAutorizacion estadoAutorizacion;

    public Autorizacion() {
    }

    public Autorizacion(int id) {
        this.id = id;    
    }
    
    /**
     * @return the inventario
     */
    public Inventario getInventario() {
        return inventario;
    }

    /**
     * @param inventario the inventario to set
     */
    public void setInventario(Inventario inventario) {
        this.inventario = inventario;
    }
    
    /**
     * @return the estadoAutorizacion
     */
    public EstadoAutorizacion getEstadoAutoriacion() {
        return estadoAutorizacion;
    }

    /**
     * @param estadoAutorizacion the estadoAutorizacion to set
     */
    public void setEstadoAutoriacion(EstadoAutorizacion estadoAutorizacion) {
        this.estadoAutorizacion = estadoAutorizacion;
    }
    
    
    /**
     * @return the SedeOrigen
     */
    public Sede getSedeOrigen() {
        return SedeOrigen;
    }

    /**
     * @param SedeOrigen the SedeOrigen to set
     */
    public void setSedeOrigen(Sede SedeOrigen) {
        this.SedeOrigen = SedeOrigen;
    }

    /**
     * @return the SedeDestino
     */
    public Sede getSedeDestino() {
        return SedeDestino;
    }

    /**
     * @param SedeDestino the SedeDestino to set
     */
    public void setSedeDestino(Sede SedeDestino) {
        this.SedeDestino = SedeDestino;
    }
    

    /**
     * @return the id
     */
    public int getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(int id) {
        this.id = id;
    }

    
}
