/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package BussinessLogic;

import java.io.Serializable;
import java.time.LocalDate;

/**
 *
 * @author eddy
 */
public class Solicitud implements Serializable{

    
       
    private int id;
    private String razonRechazo;
    private LocalDate adquisicion;
    private Estado estado;
    private Tipo tipo;
    private Usuario solicitante;

    public Solicitud() {
    }

    public Solicitud(int id) {
        this.id = id;
        
    }
    
    /**
     * @return the adquisicion
     */
    public LocalDate getAdquisicion() {
        return adquisicion;
    }
    /**
     * @return the tipo
     */
    public Tipo getTipo() {
        return tipo;
    }

    /**
     * @param tipo the tipo to set
     */
    public void setTipo(Tipo tipo) {
        this.tipo = tipo;
    }
    /**
     * @return the estado
     */
    public Estado getEstado() {
        return estado;
    }

    /**
     * @param estado the estado to set
     */
    public void setEstado(Estado estado) {
        this.estado = estado;
    }
    /**
     * @return the id
     */
    public int getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(int id) {
        this.id = id;
    }

    

  

 

    

    /**
     * @return the razonRechazo
     */
    public String getRazonRechazo() {
        return razonRechazo;
    }

    /**
     * @param razonRechazo the razonRechazo to set
     */
    public void setRazonRechazo(String razonRechazo) {
        this.razonRechazo = razonRechazo;
    }

    /**
     * @param adquisicion the adquisicion to set
     */
    public void setAdquisicion(LocalDate adquisicion) {
        this.adquisicion = adquisicion;
    }

    /**
     * @return the solicitante
     */
    public Usuario getSolicitante() {
        return solicitante;
    }

    /**
     * @param solicitante the solicitante to set
     */
    public void setSolicitante(Usuario solicitante) {
        this.solicitante = solicitante;
    }

    @Override
    public String toString() {
        
        return "{id:"+this.id
                +",razonRechazo:"+this.razonRechazo
                +",adquisicion:"+this.adquisicion.toString()
                +",estado:"+this.estado.toString()
                +",tipo:"+this.tipo.toString()
                +",solicitante:"+this.solicitante.getCedula()+"}"; //To change body of generated methods, choose Tools | Templates.
    }
    
    
}
