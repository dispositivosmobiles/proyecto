/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package BussinessLogic;

/**
 *
 * @author eddy
 */
public class TipoUsuario {
    private String nombre;

    public TipoUsuario(String nombre) {
        this.nombre = nombre;
    }

    /**
     * @return the nombre
     */
    public String getNombre() {
        return nombre;
    }

    @Override
    public String toString() {
        return "{nombre:"+this.nombre+"}"; //To change body of generated methods, choose Tools | Templates.
    }
    
    
}
