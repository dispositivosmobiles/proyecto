/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controller;

import BussinessLogic.Color;

import DAO.UsuarioDAO;
import BussinessLogic.Usuario;
import BussinessLogic.Estado;
import BussinessLogic.Inventario;
import BussinessLogic.Sede;
import BussinessLogic.Tipo;
import BussinessLogic.TipoUsuario;
import com.google.gson.Gson;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.io.Reader;
import java.sql.Date;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import util.CodigoRespuesta;
import util.Respuesta;

/**
 *
 * @author eddy
 */
@WebServlet(name = "controlUsuario", urlPatterns = {"/controlUsuario","/createUsuario","/readUsuario","/readAllUsuario","/adminUsuario","/updateUsuario","/deleteUsuario","/doFormUsuario","/getInfoUsuarioApi"})
public class UsuarioApi extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        /*response.setContentType("text/html;charset=UTF-8");*/
        String servletPath=request.getServletPath();
        switch(servletPath){
            case "/createUsuario":
                this.doCreate(request, response);
                break;
            case "/readUsuario":
                this.doRead(request, response);
                break;
            case "/readAllUsuario":
                this.doReadAll(request, response);
                break;            
            case "/updateUsuario":
                this.doUpdate(request, response);
                break;                
            case "/deleteUsuario":
                this.doDelete(request, response);
                break;  
            case "/getInfoUsuarioApi":
                this.doGetInfo(request, response);
                break;
        }    
            
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }
    
    private void doCreate(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException{
        try{
            //Reader objectReader = new BufferedReader(new InputStreamReader(request.getPart("object").getInputStream()));
            Gson gson = new Gson();
            //Usuario object = gson.fromJson(objectReader, Usuario.class);       
            Usuario object = new Usuario();
            
            //cedula,nombre,apellido1,apellido2,clave,email,telefono,sede,TipoUsuario,nacimiento
            object.setCedula(request.getParameter("cedula"));
            object.setNombre(request.getParameter("nombre"));
            object.setApellido1(request.getParameter("apellido1"));
            object.setApellido2(request.getParameter("apellido2"));
            object.setClave(request.getParameter("clave"));
            object.setEmail(request.getParameter("email"));
            object.setTelefono(request.getParameter("telefono"));
            object.setSede(new Sede(request.getParameter("sede")));
            object.setTipoUsuario(new TipoUsuario(request.getParameter("TipoUsuario")));
            object.setNacimiento(LocalDate.parse(request.getParameter("nacimiento")));
            

            
            PrintWriter out;
            response.setContentType("application/json; charset=UTF-8");
            out = response.getWriter();
            Respuesta respuesta = DAO.UsuarioDAO.getInstance().create(object);                
            
            response.setStatus(respuesta.getCodigoRespuesta().getValue());
            out.write(gson.toJson(respuesta));        
        } catch (IOException ex) {
            Respuesta respuesta2= new Respuesta(
                    Boolean.FALSE, 
                    CodigoRespuesta.ERROR_INTERNO, 
                    ex.getMessage(), 
                    ex.getLocalizedMessage()
            );
            response.setStatus(respuesta2.getCodigoRespuesta().getValue()); 
            Logger.getLogger(Respuesta.class.getName()).log(Level.SEVERE, null, ex);
            Gson gson = new Gson();
            PrintWriter out = response.getWriter();
            out.write(gson.toJson(respuesta2));        
        }
    }
    
    private void doRead(HttpServletRequest request, HttpServletResponse response){
        try{
            //Reader objectReader = new BufferedReader(new InputStreamReader(request.getPart("object").getInputStream()));
            Usuario object = new Usuario(request.getParameter("cedula"));
            Gson gson = new Gson();
            //Usuario object = gson.fromJson(objectReader, Usuario.class);       
            PrintWriter out;
            response.setContentType("application/json; charset=UTF-8");
            out = response.getWriter();
            Respuesta respuesta = DAO.UsuarioDAO.getInstance().read(object);                
            if (respuesta.getEstado()) {
               out.write(gson.toJson(respuesta));        
               response.setStatus(200); // ok with content
            }
            else
            {
               out.write(gson.toJson(respuesta));        
               response.setStatus(respuesta.getCodigoRespuesta().getValue()); // ok with content
               Logger.getLogger(Respuesta.class.getName()).log(Level.SEVERE, null, respuesta.getMensajeInterno());
            }
        } catch (IOException ex) {
            response.setStatus(401); 
            Logger.getLogger(Respuesta.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    private void doReadAll(HttpServletRequest request, HttpServletResponse response){
        try{
            
            Gson gson = new Gson();
              
            PrintWriter out;
            response.setContentType("application/json; charset=UTF-8");
            out = response.getWriter();
            Respuesta respuesta = DAO.UsuarioDAO.getInstance().readAll();            
            if (respuesta.getEstado()) {
                ArrayList<Usuario> l= new ArrayList<>();
                for (Object o:respuesta.getListaDeResultados()) {
                    l.add((Usuario) o);
                }
               out.write(gson.toJson(respuesta));
               response.setStatus(200); // ok with content
            }
            else
            {
               out.write(gson.toJson(respuesta));
               response.setStatus(respuesta.getCodigoRespuesta().getValue()); // ok with content
            }
        } catch (IOException ex) {
            response.setStatus(401); 
            Logger.getLogger(Respuesta.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    private void doUpdate(HttpServletRequest request, HttpServletResponse response) throws ServletException{
        try{
            //Reader objectReader = new BufferedReader(new InputStreamReader(request.getPart("object").getInputStream()));
            Gson gson = new Gson();
            //Usuario object = gson.fromJson(objectReader, Usuario.class);                        
            Usuario object = new Usuario();
            
            
            object.setCedula(request.getParameter("cedula"));
            object.setNombre(request.getParameter("nombre"));
            object.setApellido1(request.getParameter("apellido1"));
            object.setApellido2(request.getParameter("apellido2"));
            object.setClave(request.getParameter("clave"));
            object.setEmail(request.getParameter("email"));
            object.setTelefono(request.getParameter("telefono"));
            object.setSede(new Sede(request.getParameter("sede")));
            object.setTipoUsuario(new TipoUsuario(request.getParameter("TipoUsuario")));
            object.setNacimiento(LocalDate.parse(request.getParameter("nacimiento"),DateTimeFormatter.ofPattern("dd/MM/yyyy")));
            
            PrintWriter out;
            response.setContentType("application/json; charset=UTF-8");
            out = response.getWriter();
            Respuesta respuesta = DAO.UsuarioDAO.getInstance().update((Usuario) object);                
            if (respuesta.getEstado()) {
               out.write(gson.toJson(respuesta));        
               response.setStatus(200); // ok with content
                //doListUsuariosAdmin(request, response);
            }
            else
            {
               out.write(gson.toJson(respuesta));        
               response.setStatus(respuesta.getCodigoRespuesta().getValue()); // ok with content
               Logger.getLogger(Respuesta.class.getName()).log(Level.SEVERE, null, respuesta.getMensajeInterno());
            }
        } catch (IOException ex) {
            response.setStatus(401); 
            Logger.getLogger(Respuesta.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    @Override
    protected void doDelete(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException{
        try{
           // Reader objectReader = new BufferedReader(new InputStreamReader(request.getPart("object").getInputStream()));
            Gson gson = new Gson();
            //Usuario object = gson.fromJson(objectReader, Usuario.class);       
            
            String sid=request.getParameter("cedula");  
            Usuario object2=new Usuario(sid);
            PrintWriter out;
            response.setContentType("application/json; charset=UTF-8");
            out = response.getWriter();
            Respuesta respuesta = DAO.UsuarioDAO.getInstance().delete(object2);                
            out.write(gson.toJson(respuesta));        
            //response.setStatus(respuesta.getCodigoRespuesta().getValue()); // ok with content
        } catch (IOException ex) {
            Gson gson = new Gson();
            PrintWriter out;
            out = response.getWriter();
            out.write(gson.toJson(new Respuesta(false, CodigoRespuesta.ERROR_INTERNO, ex.getMessage(), ex.toString())));  
            response.setStatus(401); 
            Logger.getLogger(Respuesta.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    private void doGetInfo(HttpServletRequest request, HttpServletResponse response){
         try{
            
            Gson gson = new Gson();
              
            PrintWriter out;
            response.setContentType("application/json; charset=UTF-8");
            out = response.getWriter();            
            out.write(gson.toJson(this.getServletInfo()));
        } catch (IOException ex) {
            response.setStatus(401); 
            Logger.getLogger(Respuesta.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
       
    @Override
    public String getServletInfo() {//cedula,nombre,apellido1,apellido2,clave,email,telefono,sede,TipoUsuario,nacimiento
        return "/createUsuario{IN:[cedula,nombre,apellido1,apellido2,clave,email,telefono,sede,TipoUsuario,nacimiento],OUT:[confirmation message]},"
                +"  /readUsuario{IN:[cedula],OUT:[Usuario]},"
                +"  /readAllUsuario{IN:[],OUT:[list of Usuarios]},"                
                +"  /updateUsuario{IN:[cedula,nombre,apellido1,apellido2,clave,email,telefono,sede,TipoUsuario,nacimiento],OUT:[confirmation message]},"
                +"  /deleteUsuario{IN:[cedula],OUT:[Confirmation Message]},";
    }// </editor-fold>
    
    
    
}
