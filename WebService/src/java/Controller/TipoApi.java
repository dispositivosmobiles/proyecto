/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controller;

import BussinessLogic.Color;

import DAO.TipoDAO;
import BussinessLogic.Tipo;

import BussinessLogic.Inventario;
import BussinessLogic.Sede;
import BussinessLogic.Tipo;
import com.google.gson.Gson;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.io.Reader;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import util.CodigoRespuesta;
import util.Respuesta;

/**
 *
 * @author eddy
 */
@WebServlet(name = "controlTipo", urlPatterns = {"/controlTipo","/createTipo","/readTipo","/readAllTipo","/adminTipo","/deleteTipo","/doFormTipo","/getInfoTipoApi"})
public class TipoApi extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        /*response.setContentType("text/html;charset=UTF-8");*/
        String servletPath=request.getServletPath();
        switch(servletPath){
            case "/createTipo":
                this.doCreate(request, response);
                break;
            case "/readTipo":
                this.doRead(request, response);
                break;
            case "/readAllTipo":
                this.doReadAll(request, response);
                break;            
                          
            case "/deleteTipo":
                this.doDelete(request, response);
                break; 
            case "/getInfoTipoApi":
                this.doGetInfo(request, response);
                break;
        }    
            
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }
    
    private void doCreate(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException{
        try{
            //Reader objectReader = new BufferedReader(new InputStreamReader(request.getPart("object").getInputStream()));
            Gson gson = new Gson();
            //Tipo object = gson.fromJson(objectReader, Tipo.class);       
            Tipo object = new Tipo(request.getParameter("nombre"));                        
            
            PrintWriter out;
            response.setContentType("application/json; charset=UTF-8");
            out = response.getWriter();
            Respuesta respuesta = DAO.TipoDAO.getInstance().create(object);                
            
            response.setStatus(respuesta.getCodigoRespuesta().getValue());
            out.write(gson.toJson(respuesta));        
        } catch (IOException ex) {
            Respuesta respuesta2= new Respuesta(
                    Boolean.FALSE, 
                    CodigoRespuesta.ERROR_INTERNO, 
                    ex.getMessage(), 
                    ex.getLocalizedMessage()
            );
            response.setStatus(respuesta2.getCodigoRespuesta().getValue()); 
            Logger.getLogger(Respuesta.class.getName()).log(Level.SEVERE, null, ex);
            Gson gson = new Gson();
            PrintWriter out = response.getWriter();
            out.write(gson.toJson(respuesta2));        
        }
    }
    
    private void doRead(HttpServletRequest request, HttpServletResponse response){
        try{
            //Reader objectReader = new BufferedReader(new InputStreamReader(request.getPart("object").getInputStream()));
            Tipo object = new Tipo(request.getParameter("nombre"));
            Gson gson = new Gson();
            //Tipo object = gson.fromJson(objectReader, Tipo.class);       
            PrintWriter out;
            response.setContentType("application/json; charset=UTF-8");
            out = response.getWriter();
            Respuesta respuesta = DAO.TipoDAO.getInstance().read(object);                
            if (respuesta.getEstado()) {
               out.write(gson.toJson(respuesta));        
               response.setStatus(200); // ok with content
            }
            else
            {
               out.write(gson.toJson(respuesta.getMensaje()));        
               response.setStatus(respuesta.getCodigoRespuesta().getValue()); // ok with content
               Logger.getLogger(Respuesta.class.getName()).log(Level.SEVERE, null, respuesta.getMensajeInterno());
            }
        } catch (IOException ex) {
            response.setStatus(401); 
            Logger.getLogger(Respuesta.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    private void doReadAll(HttpServletRequest request, HttpServletResponse response){
        try{
            
            Gson gson = new Gson();
              
            PrintWriter out;
            response.setContentType("application/json; charset=UTF-8");
            out = response.getWriter();
            Respuesta respuesta = DAO.TipoDAO.getInstance().readAll();            
            if (respuesta.getEstado()) {
                ArrayList<Tipo> l= new ArrayList<>();
                for (Object o:respuesta.getListaDeResultados()) {
                    l.add((Tipo) o);
                }
               out.write(gson.toJson(respuesta));
               response.setStatus(200); // ok with content
            }
            else
            {
               out.write(gson.toJson(respuesta));
               response.setStatus(respuesta.getCodigoRespuesta().getValue()); // ok with content
            }
        } catch (IOException ex) {
            response.setStatus(401); 
            Logger.getLogger(Respuesta.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    
    @Override
    protected void doDelete(HttpServletRequest request, HttpServletResponse response) throws ServletException{
        try{
           // Reader objectReader = new BufferedReader(new InputStreamReader(request.getPart("object").getInputStream()));
            Gson gson = new Gson();
            //Tipo object = gson.fromJson(objectReader, Tipo.class);       
            
            String sid=request.getParameter("nombre");  
            Tipo object2=new Tipo(sid);
            PrintWriter out;
            response.setContentType("application/json; charset=UTF-8");
            out = response.getWriter();
            Respuesta respuesta = DAO.TipoDAO.getInstance().delete(object2);                
            if (respuesta.getEstado()) {
               out.write(gson.toJson(respuesta.getMensajeInterno()));        
               response.setStatus(200); // ok with content
                //doListTiposAdmin(request, response);
            }
            else
            {
               out.write(gson.toJson(respuesta.getMensaje()));        
               response.setStatus(respuesta.getCodigoRespuesta().getValue()); // ok with content
               Logger.getLogger(Respuesta.class.getName()).log(Level.SEVERE, null, respuesta.getMensajeInterno());
            }
        } catch (IOException ex) {
            response.setStatus(401); 
            Logger.getLogger(Respuesta.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    private void doGetInfo(HttpServletRequest request, HttpServletResponse response){
         try{
            
            Gson gson = new Gson();
              
            PrintWriter out;
            response.setContentType("application/json; charset=UTF-8");
            out = response.getWriter();            
            out.write(gson.toJson(this.getServletInfo()));
        } catch (IOException ex) {
            response.setStatus(401); 
            Logger.getLogger(Respuesta.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
       
    @Override
    public String getServletInfo() {
        return "/createTipo{IN:[nombre],OUT:[Confirmation Message]},"
                +"  /readTipo{IN:[nombre],OUT:[Tipo]},"
                +"  /readAllTipo{IN:[],OUT:[list of tipo]},"                
                +"  /deleteTipo{IN:[id_solicitud],OUT:[Confirmation Message]}";
    }// </editor-fold>
    
    
    
}
