/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controller;

import BussinessLogic.Color;


import BussinessLogic.Inventario;
import BussinessLogic.Canton;
import BussinessLogic.Estado;
import BussinessLogic.EstadoAutorizacion;
import BussinessLogic.Sede;
import BussinessLogic.Tipo;
import BussinessLogic.TipoUsuario;
import com.google.gson.Gson;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.io.Reader;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import util.CodigoRespuesta;
import util.Respuesta;

/**
 *
 * @author eddy
 */
@WebServlet(name = "controlCatalogos", urlPatterns = {"/controlCatalogos","/readAllProvincia","/readAllCanton","/readAllSede","/readAllTipoUsuario","/readAllEstado","/readAllColor","/readAllEstadoAutorizacion","/getInfoCatalgosApi"})
public class CatalogosApi extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        /*response.setContentType("text/html;charset=UTF-8");*/
        String servletPath=request.getServletPath();
        switch(servletPath){
            case "/readAllCanton":
                this.doReadAllCanton(request, response);                
                break;
            case "/readAllProvincia":
                this.doReadAllProvncia(request, response);
                break;
            case "/readAllSede":
                this.doReadAllSede(request, response);
                break;            
            case "/readAllTipoUsuario":
                this.doReadAllTipoUsuario(request, response);
                break;                
            case "/readAllEstado":
                this.doReadAllEstado(request, response);
                break;     
            case "/readAllColor":
                this.doReadAllColor(request, response);
                break;  
            case "/readAllEstadoAutorizacion":
                this.doReadAllEstadoAutorizacion(request, response);
                break;  
            case "/getInfoCatalgosApi":
                this.doGetInfo(request, response);
                break;
        }    
            
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }
    
   
    
    private void doReadAllProvncia(HttpServletRequest request, HttpServletResponse response){
        try{
            
            Gson gson = new Gson();
              
            PrintWriter out;
            response.setContentType("application/json; charset=UTF-8");
            out = response.getWriter();
            Respuesta respuesta = DAO.CantonDAO.getInstance().readAll();
            if (respuesta.getEstado()) {
                ArrayList<Canton> l= new ArrayList<>();
                for (Object o:respuesta.getListaDeResultados()) {
                    l.add((Canton) o);
                }
               out.write(gson.toJson(respuesta));
               response.setStatus(200); // ok with content
            }
            else
            {
               out.write(gson.toJson(respuesta));
               response.setStatus(respuesta.getCodigoRespuesta().getValue()); // ok with content
            }
        } catch (IOException ex) {
            response.setStatus(401); 
            Logger.getLogger(Respuesta.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    private void doReadAllCanton(HttpServletRequest request, HttpServletResponse response){
        try{
            
            Gson gson = new Gson();
              
            PrintWriter out;
            response.setContentType("application/json; charset=UTF-8");
            out = response.getWriter();
            Respuesta respuesta = DAO.CantonDAO.getInstance().readAll();
            if (respuesta.getEstado()) {
                ArrayList<Canton> l= new ArrayList<>();
                for (Object o:respuesta.getListaDeResultados()) {
                    l.add((Canton) o);
                }
               out.write(gson.toJson(respuesta));
               response.setStatus(200); // ok with content
            }
            else
            {
               out.write(gson.toJson(respuesta));
               response.setStatus(respuesta.getCodigoRespuesta().getValue()); // ok with content
            }
        } catch (IOException ex) {
            response.setStatus(401); 
            Logger.getLogger(Respuesta.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    private void doReadAllSede(HttpServletRequest request, HttpServletResponse response){
        try{
            
            Gson gson = new Gson();
              
            PrintWriter out;
            response.setContentType("application/json; charset=UTF-8");
            out = response.getWriter();
            Respuesta respuesta = DAO.SedeDAO.getInstance().readAll();
            if (respuesta.getEstado()) {
                ArrayList<Sede> l= new ArrayList<>();
                for (Object o:respuesta.getListaDeResultados()) {
                    l.add((Sede) o);
                }
               out.write(gson.toJson(respuesta));
               response.setStatus(200); // ok with content
            }
            else
            {
               out.write(gson.toJson(respuesta));
               response.setStatus(respuesta.getCodigoRespuesta().getValue()); // ok with content
            }
        } catch (IOException ex) {
            response.setStatus(401); 
            Logger.getLogger(Respuesta.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    private void doReadAllTipoUsuario(HttpServletRequest request, HttpServletResponse response){
        try{
            
            Gson gson = new Gson();
              
            PrintWriter out;
            response.setContentType("application/json; charset=UTF-8");
            out = response.getWriter();
            Respuesta respuesta = DAO.TipoUsuarioDAO.getInstance().readAll();
            if (respuesta.getEstado()) {
                ArrayList<TipoUsuario> l= new ArrayList<>();
                for (Object o:respuesta.getListaDeResultados()) {
                    l.add((TipoUsuario) o);
                }
               out.write(gson.toJson(respuesta));
               response.setStatus(200); // ok with content
            }
            else
            {
               out.write(gson.toJson(respuesta));
               response.setStatus(respuesta.getCodigoRespuesta().getValue()); // ok with content
            }
        } catch (IOException ex) {
            response.setStatus(401); 
            Logger.getLogger(Respuesta.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    private void doReadAllEstado(HttpServletRequest request, HttpServletResponse response){
        try{
            
            Gson gson = new Gson();
              
            PrintWriter out;
            response.setContentType("application/json; charset=UTF-8");
            out = response.getWriter();
            Respuesta respuesta = DAO.EstadoDAO.getInstance().readAll();
            if (respuesta.getEstado()) {
                ArrayList<Estado> l= new ArrayList<>();
                for (Object o:respuesta.getListaDeResultados()) {
                    l.add((Estado) o);
                }
               out.write(gson.toJson(respuesta));
               response.setStatus(200); // ok with content
            }
            else
            {
               out.write(gson.toJson(respuesta));
               response.setStatus(respuesta.getCodigoRespuesta().getValue()); // ok with content
            }
        } catch (IOException ex) {
            response.setStatus(401); 
            Logger.getLogger(Respuesta.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    private void doReadAllColor(HttpServletRequest request, HttpServletResponse response){
        try{
            
            Gson gson = new Gson();
              
            PrintWriter out;
            response.setContentType("application/json; charset=UTF-8");
            out = response.getWriter();
            Respuesta respuesta = DAO.ColorDAO.getInstance().readAll();
            if (respuesta.getEstado()) {
                ArrayList<Color> l= new ArrayList<>();
                for (Object o:respuesta.getListaDeResultados()) {
                    l.add((Color) o);
                }
               out.write(gson.toJson(respuesta));
               response.setStatus(200); // ok with content
            }
            else
            {
               out.write(gson.toJson(respuesta));
               response.setStatus(respuesta.getCodigoRespuesta().getValue()); // ok with content
            }
        } catch (IOException ex) {
            response.setStatus(401); 
            Logger.getLogger(Respuesta.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    private void doReadAllEstadoAutorizacion(HttpServletRequest request, HttpServletResponse response){
        try{
            
            Gson gson = new Gson();
              
            PrintWriter out;
            response.setContentType("application/json; charset=UTF-8");
            out = response.getWriter();
            Respuesta respuesta = DAO.EstadoAutorizacionDAO.getInstance().readAll();
            if (respuesta.getEstado()) {
                ArrayList<EstadoAutorizacion> l= new ArrayList<>();
                for (Object o:respuesta.getListaDeResultados()) {
                    l.add((EstadoAutorizacion) o);
                }
               out.write(gson.toJson(respuesta));
               response.setStatus(200); // ok with content
            }
            else
            {
               out.write(gson.toJson(respuesta));
               response.setStatus(respuesta.getCodigoRespuesta().getValue()); // ok with content
            }
        } catch (IOException ex) {
            response.setStatus(401); 
            Logger.getLogger(Respuesta.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    private void doGetInfo(HttpServletRequest request, HttpServletResponse response){
         try{
            
            Gson gson = new Gson();
              
            PrintWriter out;
            response.setContentType("application/json; charset=UTF-8");
            out = response.getWriter();            
            out.write(gson.toJson(this.getServletInfo()));
        } catch (IOException ex) {
            response.setStatus(401); 
            Logger.getLogger(Respuesta.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
       
    @Override
    public String getServletInfo() {
        return "readAllProvincia{IN:[],OUT:list of provincias},"
                +"  readAllCanton{IN:[],OUT:list of cantones},"
                +"  readAllSede{IN:[],OUT:list of sedes},"
                +"  readAllTipoUsuario{IN:[],OUT:list of TipoUsuario},"
                +"  readAllColor{IN:[],OUT:list of colores},"
                +"  readAllEstado{IN:[],OUT:list of estados},"
                +"  readAllEstadoAutorizacion{IN:[],OUT:list of EstadoAutorizacion}";
    }// </editor-fold>
    
    
    
}
