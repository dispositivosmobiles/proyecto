/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controller;

import BussinessLogic.Color;

import DAO.InventarioDAO;
import BussinessLogic.Inventario;
import BussinessLogic.Sede;
import BussinessLogic.Tipo;
import com.google.gson.Gson;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.io.Reader;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import util.CodigoRespuesta;
import util.Respuesta;

/**
 *
 * @author eddy
 */
@WebServlet(name = "controlInventario", urlPatterns = {"/controlInventario","/createInventario","/readInventario","/readAllInventario","/adminInventario","/updateInventario","/deleteInventario","/doFormInventario","/getInfoInventarioApi"})
public class InventarioApi extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        /*response.setContentType("text/html;charset=UTF-8");*/
        String servletPath=request.getServletPath();
        switch(servletPath){
            case "/createInventario":
                this.doCreate(request, response);
                break;
            case "/readInventario":
                this.doRead(request, response);
                break;
            case "/readAllInventario":
                this.doReadAll(request, response);
                break;            
            case "/updateInventario":
                this.doUpdate(request, response);
                break;                
            case "/deleteInventario":
                this.doDelete(request, response);
                break; 
            case "/getInfoInventarioApi":
                this.doGetInfo(request, response);
                break;
        }    
            
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }
    
    private void doCreate(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException{
        try{
            //Reader objectReader = new BufferedReader(new InputStreamReader(request.getPart("object").getInputStream()));
            Gson gson = new Gson();
            //Inventario object = gson.fromJson(objectReader, Inventario.class);       
            Inventario object = new Inventario();
            object.setApproved(false);
            object.setColor(new Color(request.getParameter("color")));
            object.setMarca(request.getParameter("marca"));
            object.setModelo(request.getParameter("modelo"));
            object.setNoSerial(request.getParameter("noSerial"));
            object.setSede(new Sede(request.getParameter("sede")));
            object.setSolicitud(Integer.valueOf(request.getParameter("id_solicitud")));
            object.setTipo(new Tipo(request.getParameter("tipo")));
            PrintWriter out;
            response.setContentType("application/json; charset=UTF-8");
            out = response.getWriter();
            Respuesta respuesta = DAO.InventarioDAO.getInstance().create(object);                
            
            response.setStatus(respuesta.getCodigoRespuesta().getValue());
            out.write(gson.toJson(respuesta));        
        } catch (IOException ex) {
            Respuesta respuesta2= new Respuesta(
                    Boolean.FALSE, 
                    CodigoRespuesta.ERROR_INTERNO, 
                    ex.getMessage(), 
                    ex.getLocalizedMessage()
            );
            response.setStatus(respuesta2.getCodigoRespuesta().getValue()); 
            Logger.getLogger(Respuesta.class.getName()).log(Level.SEVERE, null, ex);
            Gson gson = new Gson();
            PrintWriter out = response.getWriter();
            out.write(gson.toJson(respuesta2));        
        }
    }
    
    private void doRead(HttpServletRequest request, HttpServletResponse response){
        try{
            //Reader objectReader = new BufferedReader(new InputStreamReader(request.getPart("object").getInputStream()));
            Inventario object = new Inventario(Integer.valueOf(request.getParameter("id_inventario")));
            Gson gson = new Gson();
            //Inventario object = gson.fromJson(objectReader, Inventario.class);       
            PrintWriter out;
            response.setContentType("application/json; charset=UTF-8");
            out = response.getWriter();
            Respuesta respuesta = DAO.InventarioDAO.getInstance().read(object);                
            if (respuesta.getEstado()) {
               out.write(gson.toJson(respuesta));        
               response.setStatus(200); // ok with content
            }
            else
            {
               out.write(gson.toJson(respuesta));        
               response.setStatus(respuesta.getCodigoRespuesta().getValue()); // ok with content
               Logger.getLogger(Respuesta.class.getName()).log(Level.SEVERE, null, respuesta.getMensajeInterno());
            }
        } catch (IOException ex) {
            response.setStatus(401); 
            Logger.getLogger(Respuesta.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    private void doReadAll(HttpServletRequest request, HttpServletResponse response){
        try{
            
            Gson gson = new Gson();
              
            PrintWriter out;
            response.setContentType("application/json; charset=UTF-8");
            out = response.getWriter();
            Respuesta respuesta = DAO.InventarioDAO.getInstance().readAll();            
            if (respuesta.getEstado()) {
                ArrayList<Inventario> l= new ArrayList<>();
                for (Object o:respuesta.getListaDeResultados()) {
                    l.add((Inventario) o);
                }
               out.write(gson.toJson(respuesta));
               response.setStatus(200); // ok with content
            }
            else
            {
               out.write(gson.toJson(respuesta));
               response.setStatus(respuesta.getCodigoRespuesta().getValue()); // ok with content
            }
        } catch (IOException ex) {
            response.setStatus(401); 
            Logger.getLogger(Respuesta.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    private void doUpdate(HttpServletRequest request, HttpServletResponse response) throws ServletException{
        try{
            //Reader objectReader = new BufferedReader(new InputStreamReader(request.getPart("object").getInputStream()));
            Gson gson = new Gson();
            //Inventario object = gson.fromJson(objectReader, Inventario.class);       
            Inventario object = new Inventario(Integer.valueOf(request.getParameter("id_inventario")));
            Respuesta exists = DAO.InventarioDAO.getInstance().read(object);                
            PrintWriter out;
            response.setContentType("application/json; charset=UTF-8");
            out = response.getWriter();
            if (exists.getEstado()) {
                    object.setApproved(Boolean.getBoolean(request.getParameter("approved")));
                object.setColor(new Color(request.getParameter("color")));
                object.setMarca(request.getParameter("marca"));
                object.setModelo(request.getParameter("modelo"));
                object.setNoSerial(request.getParameter("noSerial"));
                object.setSede(new Sede(request.getParameter("sede")));
                object.setSolicitud(Integer.valueOf(request.getParameter("id_solicitud")));
                object.setTipo(new Tipo(request.getParameter("tipo")));
            
                Respuesta respuesta = DAO.InventarioDAO.getInstance().update((Inventario) object);                
                if (respuesta.getEstado()) {
                   out.write(gson.toJson(respuesta));        
                   response.setStatus(200); // ok with content
                //doListInventariosAdmin(request, response);
                }
                else
                {
                   out.write(gson.toJson(respuesta));        
                   response.setStatus(respuesta.getCodigoRespuesta().getValue()); // ok with content
                   Logger.getLogger(Respuesta.class.getName()).log(Level.SEVERE, null, respuesta.getMensajeInterno());
                }
            }
            else
            {
                out.write(gson.toJson(exists));        
                   response.setStatus(200); // ok with content
            }
            
        } catch (IOException ex) {
            response.setStatus(401); 
            Logger.getLogger(Respuesta.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    @Override
    protected void doDelete(HttpServletRequest request, HttpServletResponse response) throws ServletException{
        try{
           // Reader objectReader = new BufferedReader(new InputStreamReader(request.getPart("object").getInputStream()));
            Gson gson = new Gson();
            //Inventario object = gson.fromJson(objectReader, Inventario.class);       
            PrintWriter out;
            response.setContentType("application/json; charset=UTF-8");
            out = response.getWriter();
            
            String sid=request.getParameter("id_inventario");  
            Inventario object2=new Inventario(Integer.valueOf(sid));
            Respuesta exist = DAO.InventarioDAO.getInstance().read(object2);                
            if (exist.getEstado()) {
                Respuesta respuesta = DAO.InventarioDAO.getInstance().delete(object2);                
                if (respuesta.getEstado()) {
                   out.write(gson.toJson(respuesta));        
                   response.setStatus(200); // ok with content
                    //doListInventariosAdmin(request, response);
                }
                else
                {
                out.write(gson.toJson(respuesta));        
                response.setStatus(respuesta.getCodigoRespuesta().getValue()); // ok with content
                Logger.getLogger(Respuesta.class.getName()).log(Level.SEVERE, null, respuesta.getMensajeInterno());
                }
            }
            else{
                   out.write(gson.toJson(exist));        
                   response.setStatus(exist.getCodigoRespuesta().getValue()); // ok with content
            }
            
        } catch (IOException ex) {
            response.setStatus(401); 
            Logger.getLogger(Respuesta.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    private void doGetInfo(HttpServletRequest request, HttpServletResponse response){
         try{
            
            Gson gson = new Gson();
              
            PrintWriter out;
            response.setContentType("application/json; charset=UTF-8");
            out = response.getWriter();            
            out.write(gson.toJson(this.getServletInfo()));
        } catch (IOException ex) {
            response.setStatus(401); 
            Logger.getLogger(Respuesta.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
       
    @Override
    public String getServletInfo() {
        return "/createInventario{IN:[color,marca,modelo,noSerial,sede,id_solicitud,tipo],OUT:[Menjase de confirmacion]},"
                +"  /readInventario{IN:[id_inventario],OUT:[Inventario]},"
                +"  /readAllInventario{IN:[],OUT:[list of Inventario]},"                
                +"  /updateInventario{IN:[id_inventario,approved,color,marca,modelo,noSerial,sede,id_solicitud,tipo],OUT:[Menjase de confirmacion]},"
                +"  /deleteInventario{IN:[id_inventario],OUT:[Menjase de confirmacion]}";
    }// </editor-fold>
    
    
    
}
