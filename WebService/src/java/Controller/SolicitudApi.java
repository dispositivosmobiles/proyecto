/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controller;

import BussinessLogic.Color;

import DAO.SolicitudDAO;
import BussinessLogic.Solicitud;
import BussinessLogic.Estado;
import BussinessLogic.Inventario;
import BussinessLogic.Sede;
import BussinessLogic.Tipo;
import BussinessLogic.Usuario;
import com.google.gson.Gson;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.io.Reader;
import java.sql.Date;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import util.CodigoRespuesta;
import util.Respuesta;

/**
 *
 * @author eddy
 */
@WebServlet(name = "controlSolicitud", urlPatterns = {"/controlSolicitud","/createSolicitud","/readSolicitud","/readAllSolicitud","/adminSolicitud","/updateSolicitud","/deleteSolicitud","/doFormSolicitud","/getInfoSolicitudApi"})
public class SolicitudApi extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        /*response.setContentType("text/html;charset=UTF-8");*/
        String servletPath=request.getServletPath();
        switch(servletPath){
            case "/createSolicitud":
                this.doCreate(request, response);
                break;
            case "/readSolicitud":
                this.doRead(request, response);
                break;
            case "/readAllSolicitud":
                this.doReadAll(request, response);
                break;            
            case "/updateSolicitud":
                this.doUpdate(request, response);
                break;                
            case "/deleteSolicitud":
                this.doDelete(request, response);
                break;   
            case "/getInfoSolicitudApi":
                this.doGetInfo(request, response);
                break;
        }    
            
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }
    
    private void doCreate(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException{
        try{
            //Reader objectReader = new BufferedReader(new InputStreamReader(request.getPart("object").getInputStream()));
            Gson gson = new Gson();
            //Solicitud object = gson.fromJson(objectReader, Solicitud.class);       
            Solicitud object = new Solicitud();
            //adquisicion,estado,tipo
            object.setAdquisicion(LocalDate.parse(request.getParameter("adquisicion")));
            //object.setEstado(new Estado(request.getParameter("estado")));
            object.setTipo(new Tipo(request.getParameter("tipo")));
            object.setSolicitante(new Usuario(request.getParameter("solicitante")));
            
            PrintWriter out;
            response.setContentType("application/json; charset=UTF-8");
            out = response.getWriter();
            Respuesta respuesta = DAO.SolicitudDAO.getInstance().create(object);                
            
            response.setStatus(respuesta.getCodigoRespuesta().getValue());
            out.write(gson.toJson(respuesta));        
        } catch (IOException ex) {
            Respuesta respuesta2= new Respuesta(
                    Boolean.FALSE, 
                    CodigoRespuesta.ERROR_INTERNO, 
                    ex.getMessage(), 
                    ex.getLocalizedMessage()
            );
            response.setStatus(respuesta2.getCodigoRespuesta().getValue()); 
            Logger.getLogger(Respuesta.class.getName()).log(Level.SEVERE, null, ex);
            Gson gson = new Gson();
            PrintWriter out = response.getWriter();
            out.write(gson.toJson(respuesta2));        
        }
    }
    
    private void doRead(HttpServletRequest request, HttpServletResponse response){
        try{
            //Reader objectReader = new BufferedReader(new InputStreamReader(request.getPart("object").getInputStream()));
            Solicitud object = new Solicitud(Integer.valueOf(request.getParameter("id_solicitud")));
            Gson gson = new Gson();
            //Solicitud object = gson.fromJson(objectReader, Solicitud.class);       
            PrintWriter out;
            response.setContentType("application/json; charset=UTF-8");
            out = response.getWriter();
            Respuesta respuesta = DAO.SolicitudDAO.getInstance().read(object);                
            if (respuesta.getEstado()) {
               out.write(gson.toJson(respuesta));        
               response.setStatus(200); // ok with content
            }
            else
            {
               out.write(gson.toJson(respuesta));        
               response.setStatus(respuesta.getCodigoRespuesta().getValue()); // ok with content
               Logger.getLogger(Respuesta.class.getName()).log(Level.SEVERE, null, respuesta.getMensajeInterno());
            }
        } catch (IOException ex) {
            response.setStatus(401); 
            Logger.getLogger(Respuesta.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    private void doReadAll(HttpServletRequest request, HttpServletResponse response){
        try{
            
            Gson gson = new Gson();
              
            PrintWriter out;
            response.setContentType("application/json; charset=UTF-8");
            out = response.getWriter();
            Respuesta respuesta = DAO.SolicitudDAO.getInstance().readAll();            
            if (respuesta.getEstado()) {
                ArrayList<Solicitud> l= new ArrayList<>();
                for (Object o:respuesta.getListaDeResultados()) {
                    l.add((Solicitud) o);
                }
               out.write(gson.toJson(respuesta));
               response.setStatus(200); // ok with content
            }
            else
            {
               out.write(gson.toJson(respuesta));
               response.setStatus(respuesta.getCodigoRespuesta().getValue()); // ok with content
            }
        } catch (IOException ex) {
            response.setStatus(401); 
            Logger.getLogger(Respuesta.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    private void doUpdate(HttpServletRequest request, HttpServletResponse response) throws ServletException{
        try{
            //Reader objectReader = new BufferedReader(new InputStreamReader(request.getPart("object").getInputStream()));
            Gson gson = new Gson();
            //id_solicitud,adquisicion,estado,tipo,razonRechazo     
            Solicitud object = new Solicitud(Integer.valueOf(request.getParameter("id_solicitud")));                        
            
            object.setEstado(new Estado(request.getParameter("estado")));
            
            object.setRazonRechazo(request.getParameter("razonRechazo"));
            
            
            
            PrintWriter out;
            response.setContentType("application/json; charset=UTF-8");
            out = response.getWriter();
            Respuesta respuesta = DAO.SolicitudDAO.getInstance().update((Solicitud) object);                
            if (respuesta.getEstado()) {
               out.write(gson.toJson(respuesta));        
               response.setStatus(200); // ok with content
                //doListSolicitudsAdmin(request, response);
            }
            else
            {
               out.write(gson.toJson(respuesta));        
               response.setStatus(respuesta.getCodigoRespuesta().getValue()); // ok with content
               Logger.getLogger(Respuesta.class.getName()).log(Level.SEVERE, null, respuesta.getMensajeInterno());
            }
        } catch (IOException ex) {
            response.setStatus(401); 
            Logger.getLogger(Respuesta.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    @Override
    protected void doDelete(HttpServletRequest request, HttpServletResponse response) throws ServletException{
        try{
           // Reader objectReader = new BufferedReader(new InputStreamReader(request.getPart("object").getInputStream()));
            Gson gson = new Gson();
            //Solicitud object = gson.fromJson(objectReader, Solicitud.class);       
            //id_solicitud
            
            String sid=request.getParameter("id_solicitud");  
            Solicitud object2=new Solicitud(Integer.valueOf(sid));
            Respuesta exists=DAO.SolicitudDAO.getInstance().read(object2);
            PrintWriter out;
            response.setContentType("application/json; charset=UTF-8");
            out = response.getWriter();
            if (exists.getEstado()) {
                
                Respuesta respuesta = DAO.SolicitudDAO.getInstance().delete(object2);                
                if (respuesta.getEstado()) {
                   out.write(gson.toJson(respuesta));        
                   response.setStatus(200); // ok with content
                    //doListSolicitudsAdmin(request, response);
                }   
                else
                {
                out.write(gson.toJson(respuesta));        
                response.setStatus(respuesta.getCodigoRespuesta().getValue()); // ok with content
                Logger.getLogger(Respuesta.class.getName()).log(Level.SEVERE, null, respuesta.getMensajeInterno());
                }
            }
            else
            {
                out.write(gson.toJson(exists));        
                   response.setStatus(exists.getCodigoRespuesta().getValue());
            }
            
        } catch (IOException ex) {
            response.setStatus(401); 
            Logger.getLogger(Respuesta.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    private void doGetInfo(HttpServletRequest request, HttpServletResponse response){
         try{
            
            Gson gson = new Gson();
              
            PrintWriter out;
            response.setContentType("application/json; charset=UTF-8");
            out = response.getWriter();            
            out.write(gson.toJson(this.getServletInfo()));
        } catch (IOException ex) {
            response.setStatus(401); 
            Logger.getLogger(Respuesta.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
        
    @Override
    public String getServletInfo() {
        return "/createSolicitud{IN:[adquisicion,estado,tipo,solicitante],OUT:[Confirmation Message]},"
                +"  /readSolicitud{IN:[id_solicitud],OUT:[Inventario]},"
                +"  /readAllSolicitud{IN:[],OUT:[list of Inventario]},"                
                +"  /updateSolicitud{IN:[id_solicitud,estado,tipo],OUT:[Confirmation Message]},"
                +"  /deleteSolicitud{IN:[id_inventario],OUT:[Confirmation Message]}";
    }// </editor-fold>
    
    
    
}
