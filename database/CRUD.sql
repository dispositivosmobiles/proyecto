--Solicitud
CREATE OR REPLACE FUNCTION schema_inventory.CREATEsolicitud (
  TipoAdquisicion_IN VARCHAR,
  adquisicion_IN DATE,
  solicitante_IN VARCHAR
)
RETURNS VOID
AS
'
   INSERT INTO schema_inventory.Solicitud(TipoAdquisicion,adquisicion,solicitante)
    VALUES(TipoAdquisicion_IN,adquisicion_IN,solicitante_IN);
'
LANGUAGE SQL;
CREATE OR REPLACE FUNCTION schema_inventory.READsolicitud (
  id_IN INTEGER
)
RETURNS refcursor
AS
'
DECLARE
  ref refcursor;
BEGIN
   OPEN ref FOR SELECT id,TipoAdquisicion,estado,adquisicion,razonRechazo,solicitante FROM schema_inventory.Solicitud WHERE id=id_IN;
   RETURN ref;
END;
'
LANGUAGE plpgsql;

CREATE OR REPLACE FUNCTION schema_inventory.READALLsolicitud ()
RETURNS refcursor
AS
'
DECLARE
  ref refcursor;
BEGIN
   OPEN ref FOR SELECT id,TipoAdquisicion,estado,adquisicion,razonRechazo,solicitante FROM schema_inventory.Solicitud;
RETURN ref;
END;
'
LANGUAGE plpgsql;

CREATE OR REPLACE FUNCTION schema_inventory.UPDATEsolicitud (
  id_IN INTEGER,
  estado_IN VARCHAR,
  razonRechazo_IN VARCHAR
)
RETURNS VOID
AS
'
  UPDATE schema_inventory.Solicitud
    SET estado=estado_IN,razonRechazo=razonRechazo_IN
  WHERE id=id_IN;
'
LANGUAGE SQL;

CREATE OR REPLACE FUNCTION schema_inventory.DELETEsolicitud (
  id_IN INTEGER
)
RETURNS VOID
AS
'
   DELETE FROM schema_inventory.Solicitud WHERE id=id_IN;
'
LANGUAGE SQL;
--Inventario
CREATE OR REPLACE FUNCTION schema_inventory.CREATEInventario (
  Solicitud_IN INTEGER,
  Tipo_IN VARCHAR,
  Color_IN VARCHAR,
  marca_IN VARCHAR,
  modelo_IN VARCHAR,
  NoSerial_IN VARCHAR
)
RETURNS VOID
AS
'
   INSERT INTO schema_inventory.Inventario(Solicitud,Tipo,marca,modelo,NoSerial)
    VALUES(Solicitud_IN,Tipo_IN,marca_IN,modelo_IN,NoSerial_IN);
'
LANGUAGE SQL;

CREATE OR REPLACE FUNCTION schema_inventory.READInventario (
  id_IN INTEGER
)
RETURNS refcursor
AS
'
DECLARE
  ref refcursor;
BEGIN
   OPEN ref FOR SELECT id,Solicitud,Sede,Tipo,marca,modelo,NoSerial,approved FROM schema_inventory.Inventario WHERE id=id_IN;
   RETURN ref;
END;
'
LANGUAGE plpgsql;

CREATE OR REPLACE FUNCTION schema_inventory.READALLInventario ()
RETURNS refcursor
AS
'
DECLARE
  ref refcursor;
BEGIN
   OPEN ref FOR SELECT id,Solicitud,Sede,Tipo,Color,marca,modelo,NoSerial,approved FROM schema_inventory.Inventario;
RETURN ref;
END;
'
LANGUAGE plpgsql;
CREATE OR REPLACE FUNCTION schema_inventory.READInventarioBySolicitud (
  Solicitud_IN INTEGER
)
RETURNS refcursor
AS
'
DECLARE
  ref refcursor;
BEGIN
   OPEN ref FOR SELECT id,Solicitud,Sede,Tipo,razonRechazo,marca,modelo,NoSerial,approved FROM schema_inventory.Inventario WHERE Solicitud=Solicitud_IN;
   RETURN ref;
END;
'
LANGUAGE plpgsql;
CREATE OR REPLACE FUNCTION schema_inventory.UPDATEInventario (
  id_IN INTEGER,
  Sede_IN VARCHAR,
  Tipo_IN VARCHAR,
  Color_IN VARCHAR,
  marca_IN VARCHAR,
  modelo_IN VARCHAR,
  NoSerial_IN VARCHAR,
  approved_IN BOOLEAN
)
RETURNS VOID
AS
'
  UPDATE schema_inventory.Inventario
    SET Sede=Sede_IN,Tipo=Tipo_IN,Color=Color_IN,marca=marca_IN,modelo=modelo_IN,NoSerial=NoSerial_IN,approved=approved_IN
  WHERE id=id_IN;
'
LANGUAGE SQL;

CREATE OR REPLACE FUNCTION schema_inventory.DELETEInventario (
  id_IN INTEGER
)
RETURNS VOID
AS
'
   DELETE FROM schema_inventory.Inventario WHERE id=id_IN;
'
LANGUAGE SQL;
--Autorizacion
CREATE OR REPLACE FUNCTION schema_inventory.CREATEAutorizacion (
  Inventario_IN INTEGER,
  SedeDestino_IN VARCHAR
)
RETURNS VOID
AS
'
   INSERT INTO schema_inventory.Autorizacion(Inventario,SedeDestino)
    VALUES(Inventario_IN,SedeDestino_IN);
'
LANGUAGE SQL;

CREATE OR REPLACE FUNCTION schema_inventory.READAutorizacion (
  id_IN INTEGER
)
RETURNS refcursor
AS
'
DECLARE
  ref refcursor;
BEGIN
   OPEN ref FOR SELECT id,Inventario,EstadoAutorizacion,SedeOrigen,SedeDestino FROM schema_inventory.Autorizacion WHERE id=id_IN;
   RETURN ref;
END;
'
LANGUAGE plpgsql;

CREATE OR REPLACE FUNCTION schema_inventory.READALLAutorizacion ()
RETURNS refcursor
AS
'
DECLARE
  ref refcursor;
BEGIN
   OPEN ref FOR SELECT id,Inventario,EstadoAutorizacion,SedeOrigen,SedeDestino FROM schema_inventory.Autorizacion;
RETURN ref;
END;
'
LANGUAGE plpgsql;

CREATE OR REPLACE FUNCTION schema_inventory.UPDATEAutorizacion (
  id_IN INTEGER,
  EstadoAutorizacion_IN VARCHAR
)
RETURNS VOID
AS
'
  UPDATE schema_inventory.Autorizacion
    SET EstadoAutorizacion=EstadoAutorizacion_IN
  WHERE id=id_IN;
'
LANGUAGE SQL;

CREATE OR REPLACE FUNCTION schema_inventory.DELETEAutorizacion (
  id_IN INTEGER
)
RETURNS VOID
AS
'
   DELETE FROM schema_inventory.Autorizacion WHERE id=id_IN;
'
LANGUAGE SQL;

--ItemSolicitud
/*
  CREATE OR REPLACE FUNCTION schema_inventory.CREATEItemSolicitud (
    Solicitud_IN INTEGER,
    marca_IN VARCHAR,
    modelo_IN VARCHAR,
    NoSerial_IN VARCHAR,
    precio_IN DOUBLE PRECISION,
    unidades_IN INT
  )
  RETURNS VOID
  AS
  '
     INSERT INTO schema_inventory.ItemSolicitud(Solicitud,marca,modelo,NoSerial,precio,unidades)
      VALUES(Solicitud_IN,marca_IN,modelo_IN,NoSerial_IN,precio_IN,unidades_IN);
  '
  LANGUAGE SQL;
  CREATE OR REPLACE FUNCTION schema_inventory.READItemSolicitud (
    id_IN INTEGER
  )
  RETURNS refcursor
  AS
  '
  DECLARE
    ref refcursor;
  BEGIN
     OPEN ref FOR SELECT id,Solicitud,marca,modelo,NoSerial,precio,unidades FROM schema_inventory.ItemSolicitud WHERE id=id_IN;
     RETURN ref;
  END;
  '
  LANGUAGE plpgsql;

  CREATE OR REPLACE FUNCTION schema_inventory.READALLItemSolicitud ()
  RETURNS refcursor
  AS
  '
  DECLARE
    ref refcursor;
  BEGIN
     OPEN ref FOR SELECT id,Solicitud,marca,modelo,NoSerial,precio,unidades FROM schema_inventory.ItemSolicitud;
  RETURN ref;
  END;
  '
  LANGUAGE plpgsql;
  CREATE OR REPLACE FUNCTION schema_inventory.READItemSolicitudBySolicitud (
    Solicitud_IN INTEGER
  )
  RETURNS refcursor
  AS
  '
  DECLARE
    ref refcursor;
  BEGIN
     OPEN ref FOR SELECT id,Solicitud,marca,modelo,NoSerial,precio,unidades FROM schema_inventory.ItemSolicitud WHERE Solicitud=Solicitud_IN;
     RETURN ref;
  END;
  '
  LANGUAGE plpgsql;
  CREATE OR REPLACE FUNCTION schema_inventory.UPDATEItemSolicitud (
    id_IN INTEGER,
    Solicitud_IN INTEGER,
    marca_IN VARCHAR,
    modelo_IN VARCHAR,
    NoSerial_IN VARCHAR,
    precio_IN DOUBLE PRECISION,
    unidades_IN INT
  )
  RETURNS VOID
  AS
  '
    UPDATE schema_inventory.ItemSolicitud
      SET Solicitud=Solicitud_IN,marca=marca_IN,modelo=modelo_IN,NoSerial=NoSerial_IN,precio=precio_IN,unidades=unidades_IN
    WHERE id=id_IN;
  '
  LANGUAGE SQL;

  CREATE OR REPLACE FUNCTION schema_inventory.DELETEItemSolicitud (
    id_IN INTEGER
  )
  RETURNS VOID
  AS
  '
     DELETE FROM schema_inventory.ItemSolicitud WHERE id=id_IN;
  '
  LANGUAGE SQL;
  */
--Usuario


  CREATE OR REPLACE FUNCTION schema_inventory.CREATEUsuario (
    cedula_IN VARCHAR,
    nombre_IN VARCHAR,
    apellido1_IN VARCHAR,
    apellido2_IN VARCHAR,
    telefono_IN VARCHAR,
    email_IN VARCHAR,
    nacimiento_IN DATE,
    TipoUsuario_IN VARCHAR,
    clave_IN VARCHAR,
    Sede_IN VARCHAR
  )
  RETURNS VOID
  AS
  '
     INSERT INTO schema_inventory.Usuario(cedula,nombre,apellido1,apellido2,telefono,email,nacimiento,TipoUsuario,clave,Sede)
      VALUES(cedula_IN,nombre_IN,apellido1_IN,apellido2_IN,telefono_IN,email_IN,nacimiento_IN,TipoUsuario_IN,clave_IN,Sede_IN);
  '
  LANGUAGE SQL;

  CREATE OR REPLACE FUNCTION schema_inventory.READUsuario (
    cedula_IN VARCHAR
  )
  RETURNS refcursor
  AS
  '
  DECLARE
    ref refcursor;
  BEGIN
     OPEN ref FOR SELECT cedula,nombre,apellido1,apellido2,telefono,email,nacimiento,TipoUsuario,Sede,Clave FROM schema_inventory.Usuario WHERE cedula=cedula_IN;
     RETURN ref;
  END;
  '
  LANGUAGE plpgsql;

  CREATE OR REPLACE FUNCTION schema_inventory.READALLUsuario ()
  RETURNS refcursor
  AS
  '
  DECLARE
    ref refcursor;
  BEGIN
     OPEN ref FOR SELECT cedula,nombre,apellido1,apellido2,telefono,email,nacimiento,TipoUsuario,Sede,clave FROM schema_inventory.Usuario;
     RETURN ref;
  END;
  '
  LANGUAGE plpgsql;

  CREATE OR REPLACE FUNCTION schema_inventory.UPDATEUsuario (
    cedula_IN VARCHAR,
    nombre_IN VARCHAR,
    apellido1_IN VARCHAR,
    apellido2_IN VARCHAR,
    telefono_IN VARCHAR,
    email_IN VARCHAR,
    nacimiento_IN DATE,
    TipoUsuario_IN VARCHAR,
    clave_IN VARCHAR,
    Sede_IN VARCHAR
  )
  RETURNS VOID
  AS
  '
     UPDATE schema_inventory.Usuario SET nombre=nombre_IN,apellido1=apellido1_IN,apellido2=apellido2_IN,telefono=telefono_IN,email=email_IN,nacimiento=nacimiento_IN,TipoUsuario=TipoUsuario_IN,Sede=Sede_IN,clave=clave_IN WHERE cedula=cedula_IN;
  '
  LANGUAGE SQL;

  CREATE OR REPLACE FUNCTION schema_inventory.DELETEUsuario (
    cedula_IN VARCHAR
  )
  RETURNS VOID
  AS
  '
  DELETE FROM schema_inventory.Usuario WHERE cedula = cedula_IN;
  '
  LANGUAGE SQL;
--Tipo
CREATE OR REPLACE FUNCTION schema_inventory.CREATETipo (
  nombre_IN VARCHAR
)
RETURNS VOID
AS
'
   INSERT INTO schema_inventory.Tipo(nombre)
    VALUES(nombre_IN);
'
LANGUAGE SQL;

CREATE OR REPLACE FUNCTION schema_inventory.READTipo (
  nombre_IN INTEGER
)
RETURNS refcursor
AS
'
DECLARE
  ref refcursor;
BEGIN
   OPEN ref FOR SELECT nombre FROM schema_inventory.Tipo WHERE nombre=nombre_IN;
   RETURN ref;
END;
'
LANGUAGE plpgsql;

CREATE OR REPLACE FUNCTION schema_inventory.READALLTipo ()
RETURNS refcursor
AS
'
DECLARE
  ref refcursor;
BEGIN
   OPEN ref FOR SELECT nombre FROM schema_inventory.Tipo;
RETURN ref;
END;
'
LANGUAGE plpgsql;


CREATE OR REPLACE FUNCTION schema_inventory.DELETETipo (
  nombre_IN VARCHAR
)
RETURNS VOID
AS
'
   DELETE FROM schema_inventory.Tipo WHERE nombre = nombre_IN;
'
LANGUAGE SQL;

--Catalogos
CREATE OR REPLACE FUNCTION schema_inventory.READALLProvincia ()
RETURNS refcursor
AS
'
DECLARE
  ref refcursor;
BEGIN
   OPEN ref FOR SELECT nombre FROM schema_inventory.Provincia;
   RETURN ref;
END;
'
LANGUAGE plpgsql;
CREATE OR REPLACE FUNCTION schema_inventory.READALLCanton()
RETURNS refcursor
AS
'
DECLARE
  ref refcursor;
BEGIN
   OPEN ref FOR SELECT nombre,Provincia FROM schema_inventory.Canton;
   RETURN ref;
END;
'
LANGUAGE plpgsql;

CREATE OR REPLACE FUNCTION schema_inventory.READALLSede()
RETURNS refcursor
AS
'
DECLARE
  ref refcursor;
BEGIN
   OPEN ref FOR SELECT nombre,Canton FROM schema_inventory.Sede;
   RETURN ref;
END;
'
LANGUAGE plpgsql;
CREATE OR REPLACE FUNCTION schema_inventory.READALLTipoUsuario ()
RETURNS refcursor
AS
'
DECLARE
  ref refcursor;
BEGIN
   OPEN ref FOR SELECT nombre FROM schema_inventory.TipoUsuario;
   RETURN ref;
END;
'
LANGUAGE plpgsql;
CREATE OR REPLACE FUNCTION schema_inventory.READALLEstado ()
RETURNS refcursor
AS
'
DECLARE
  ref refcursor;
BEGIN
   OPEN ref FOR SELECT nombre FROM schema_inventory.Estado;
   RETURN ref;
END;
'
LANGUAGE plpgsql;
CREATE OR REPLACE FUNCTION schema_inventory.READALLColor ()
RETURNS refcursor
AS
'
DECLARE
  ref refcursor;
BEGIN
   OPEN ref FOR SELECT nombre FROM schema_inventory.Color;
   RETURN ref;
END;
'
LANGUAGE plpgsql;
CREATE OR REPLACE FUNCTION schema_inventory.READALLEstadoAutorizacion ()
RETURNS refcursor
AS
'
DECLARE
  ref refcursor;
BEGIN
   OPEN ref FOR SELECT nombre FROM schema_inventory.EstadoAutorizacion;
   RETURN ref;
END;
'
LANGUAGE plpgsql;
