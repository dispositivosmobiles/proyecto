\c postgres
DROP DATABASE db_inventory;
DROP USER inventoryUsr;

DROP USER defaultRole;



CREATE DATABASE db_inventory;

\c db_inventory


CREATE SCHEMA schema_inventory;

CREATE ROLE defaultRole WITH LOGIN;
GRANT SELECT,INSERT,UPDATE,DELETE,REFERENCES ON ALL TABLES IN SCHEMA schema_inventory TO defaultRole;

CREATE USER inventoryUsr WITH PASSWORD 'inventoryRoot';
GRANT CONNECT ON DATABASE db_inventory TO inventoryUsr;
--ALTER TABLE db_inventory OWNER TO inventoryUsr;
GRANT USAGE ON SCHEMA schema_inventory TO inventoryUsr;
GRANT defaultRole TO inventoryUsr;

CREATE TABLE schema_inventory.Provincia(
        nombre VARCHAR(30),

        CONSTRAINT PKProvincia PRIMARY KEY(nombre)
);

CREATE TABLE schema_inventory.Canton(
        nombre VARCHAR(30),
        Provincia VARCHAR(30),

        CONSTRAINT PKCanton PRIMARY KEY(nombre),
        CONSTRAINT FKCanton FOREIGN KEY(Provincia) REFERENCES schema_inventory.Provincia(nombre)
);

CREATE TABLE schema_inventory.Sede(
        nombre VARCHAR(30),
        Canton VARCHAR(30),

        CONSTRAINT PKSede PRIMARY KEY(nombre),
        CONSTRAINT FKSede FOREIGN KEY(Canton) REFERENCES schema_inventory.Canton(nombre)
);

CREATE TABLE schema_inventory.TipoUsuario(
        nombre VARCHAR(30),
        CONSTRAINT PKTipoUsuario PRIMARY KEY(nombre)
);
CREATE TABLE schema_inventory.Usuario(
        cedula VARCHAR(30),
        nombre VARCHAR(30),
        apellido1 VARCHAR(30),
        apellido2 VARCHAR(30),
        telefono VARCHAR(30),
        email VARCHAR(30),
        nacimiento DATE,
        clave VARCHAR(30),
        TipoUsuario VARCHAR(30),
        Sede VARCHAR(30),
        CONSTRAINT PKUsuario PRIMARY KEY(cedula),
        CONSTRAINT FKUsuario1 FOREIGN KEY(TipoUsuario) REFERENCES schema_inventory.TipoUsuario(nombre),
        CONSTRAINT FKUsuario2 FOREIGN KEY(Sede) REFERENCES schema_inventory.Sede(nombre)
);
CREATE TABLE schema_inventory.Estado(
        nombre VARCHAR(30),
        CONSTRAINT PKEstado PRIMARY KEY(nombre)
);
CREATE TABLE schema_inventory.TipoAdquisicion(
        nombre VARCHAR(30),
        CONSTRAINT PKTipoAdquisicion PRIMARY KEY(nombre)
);
CREATE TABLE schema_inventory.Solicitud(
        id SERIAL,
        TipoAdquisicion VARCHAR(30),
        estado VARCHAR(30),
        adquisicion DATE,
        razonRechazo VARCHAR(100),
        solicitante VARCHAR(30),
        CONSTRAINT PKSolicitud PRIMARY KEY(id),
        CONSTRAINT FKSolicitud1 FOREIGN KEY(estado) REFERENCES schema_inventory.Estado(nombre),
        CONSTRAINT FKSolicitud2 FOREIGN KEY(solicitante) REFERENCES schema_inventory.Usuario(cedula),
        CONSTRAINT FKSolicitud3 FOREIGN KEY(TipoAdquisicion) REFERENCES schema_inventory.TipoAdquisicion(nombre)
);
CREATE TABLE schema_inventory.Tipo(
        nombre VARCHAR(30),
        CONSTRAINT PKTipo PRIMARY KEY(nombre)
);

CREATE TABLE schema_inventory.Color(
        nombre VARCHAR(30),
        CONSTRAINT PKColor PRIMARY KEY(nombre)
);
/*CREATE TABLE schema_inventory.ItemSolicitud(
        id SERIAL,
        Solicitud INTEGER,

        marca VARCHAR(30),
        modelo VARCHAR(30),
        NoSerial VARCHAR(30),
        precio DOUBLE PRECISION,
        unidades INTEGER,
        CONSTRAINT PKItemSolicitud PRIMARY KEY(id),
        CONSTRAINT FKItemSolicitud1 FOREIGN KEY(Solicitud) REFERENCES schema_inventory.Solicitud(id)
);*/
CREATE TABLE schema_inventory.Inventario(
        id SERIAL,
        Solicitud SERIAL,
        Sede VARCHAR(30),
        Tipo VARCHAR(30),
        Color VARCHAR(30),
        marca VARCHAR(30),
        modelo VARCHAR(30),
        NoSerial VARCHAR(30),
        approved BOOLEAN,
        CONSTRAINT PKInventario PRIMARY KEY(id),
        CONSTRAINT FKInventario1 FOREIGN KEY(Solicitud) REFERENCES schema_inventory.Solicitud(id),
        CONSTRAINT FKInventario2 FOREIGN KEY(Tipo) REFERENCES schema_inventory.Tipo(nombre),
        CONSTRAINT FKInventario3 FOREIGN KEY(Color) REFERENCES schema_inventory.Color(nombre),
        CONSTRAINT FKInventario4 FOREIGN KEY(Sede) REFERENCES schema_inventory.Sede(nombre)
);
CREATE TABLE schema_inventory.EstadoAutorizacion(
        nombre VARCHAR(30),
        CONSTRAINT PKEstadoAutorizacion PRIMARY KEY(nombre)
);
CREATE TABLE schema_inventory.Autorizacion(
        id SERIAL,
        Inventario INTEGER,
        EstadoAutorizacion VARCHAR(30),
        SedeOrigen VARCHAR(30),
        SedeDestino VARCHAR(30),
        CONSTRAINT PKAutorizacion PRIMARY KEY(id),
        CONSTRAINT FKAutorizacion1 FOREIGN KEY(Inventario) REFERENCES schema_inventory.Inventario(id),
        CONSTRAINT FKAutorizacion2 FOREIGN KEY(EstadoAutorizacion) REFERENCES schema_inventory.EstadoAutorizacion(nombre),
        CONSTRAINT FKAutorizacion3 FOREIGN KEY(SedeOrigen) REFERENCES schema_inventory.Sede(nombre),
        CONSTRAINT FKAutorizacion4 FOREIGN KEY(SedeDestino) REFERENCES schema_inventory.Sede(nombre)
);
/*
CREATE OR REPLACE FUNCTION schema_inventory.INSERTAutorizacion() RETURNS TRIGGER AS $example_table$
DECLARE
   auxSede    VARCHAR;
   BEGIN
      SELECT Sede INTO auxSede FROM schema_inventory.Inventario WHERE id = new.Inventario;
      UPDATE Autorizacion SET SedeOrigen=auxSede WHERE id=new.id;
      RETURN NEW;
   END;
$example_table$ LANGUAGE plpgsql;

CREATE OR REPLACE FUNCTION schema_inventory.INVALIDAutorizacion() RETURNS TRIGGER AS $example_table$
DECLARE
   auxSede    VARCHAR;
   auxapproved    BOOLEAN;
   BEGIN
      SELECT Sede INTO auxSede FROM schema_inventory.Inventario WHERE id = new.Inventario;
      SELECT approved INTO auxapproved FROM schema_inventory.Inventario WHERE id = new.Inventario;
      IF auxSede=new.SedeDestino THEN
        RAISE EXCEPTION 'Origin and Destiny are the same';
      IF auxapproved='false' THEN
          RAISE EXCEPTION 'Origin and Destiny are the same';
      RETURN NEW;
   END;
$example_table$ LANGUAGE plpgsql;

CREATE TRIGGER INSERTAutorizacion_tgr BEFORE INSERT ON schema_inventory.Autorizacion
  FOR EACH ROW EXECUTE PROCEDURE schema_inventory.INVALIDAutorizacion();
CREATE TRIGGER INSERTAutorizacion_tgr AFTER INSERT ON schema_inventory.Autorizacion
  FOR EACH ROW EXECUTE PROCEDURE schema_inventory.INSERTAutorizacion();
*/
