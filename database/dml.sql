\c db_inventory
TRUNCATE schema_inventory.Provincia CASCADE;
TRUNCATE schema_inventory.Canton CASCADE;
TRUNCATE schema_inventory.TipoUsuario CASCADE;
TRUNCATE schema_inventory.Estado CASCADE;
TRUNCATE schema_inventory.Color CASCADE;
TRUNCATE schema_inventory.EstadoAutorizacion CASCADE;
INSERT INTO schema_inventory.Provincia(nombre) VALUES('San José'),
('Alajuela'),
('Cartago'),
('Guanacaste'),
('Heredia'),
('Limon'),
('Puntarenas');

INSERT INTO schema_inventory.Canton(nombre, Provincia) VALUES('Escazu','San José'),
('Desamparados','San José'),
('Puriscal','San José'),
('Tarrazu','San José'),
('Aserri','San José'),
('Mora','San José'),
('Goicochea','San José'),
('Santa Ana','San José'),
('Alajuelita','San José'),
('Vazquez de Coronado','San José'),
('Acosta','San José'),
('Tibas','San José'),
('Moravia','San José'),
('Montes de Oca','San José'),
('Turrubares','San José'),
('Dota','San José'),
('Curridabat','San José'),
('Perez Zeledón','San José'),
('Leon Cortez','San José'),
--Alajuela
('Alajuela','Alajuela'),
('San Ramon','Alajuela'),
('Grecia','Alajuela'),
('San Mateo','Alajuela'),
('Atenas','Alajuela'),
('Naranjo','Alajuela'),
('Palmares','Alajuela'),
('Poas','Alajuela'),
('Orotina','Alajuela'),
('CALL San Carlos','Alajuela'),
('Zarcero','Alajuela'),
('Valverde Vega','Alajuela'),
('Upala','Alajuela'),
('Los CHiles','Alajuela'),
('Guatuso','Alajuela'),
('Río Cuarto','Alajuela'),
--Cartago
('Cartago','Cartago'),
('Paraiso','Cartago'),
('Lau Unión','Cartago'),
('Jimenez','Cartago'),
('Turrialba','Cartago'),
('Alvarado','Cartago'),
('Oreamundo','Cartago'),
('El Guarco','Cartago'),

--Guanacaste
('Liberia','Guanacaste'),
('Nicoya','Guanacaste'),
('Santa Cruz','Guanacaste'),
('Bagaces','Guanacaste'),
('Carrillo','Guanacaste'),
('Cañas','Guanacaste'),
('Abangares','Guanacaste'),
('Tilaran','Guanacaste'),
('Nandayure','Guanacaste'),
('La Cruz','Guanacaste'),
('Hojancha','Guanacaste'),
--Heredia
('Heredia','Heredia'),
('Barva','Heredia'),
('Santo Domingo','Heredia'),
('Santa Barbara','Heredia'),
('San Rafael','Heredia'),
('San Isidro','Heredia'),
('Belen','Heredia'),
('Flores','Heredia'),
('San Pablo','Heredia'),
('Sarapiqui','Heredia'),
--Limon
('Limon','Limon'),
('Pococi','Limon'),
('Siquirres','Limon'),
('Talamanca','Limon'),
('Matina','Limon'),
('Guacimo','Limon'),
--Puntarenas
('Buenos Aires','Puntarenas'),
('Corredores','Puntarenas'),
('Coto Brus','Puntarenas'),
('Esparza','Puntarenas'),
('Garabito','Puntarenas'),
('Golfito','Puntarenas'),
('Monte de Oro','Puntarenas'),
('Osa','Puntarenas'),
('Parrita','Puntarenas'),
('Puntarenas','Puntarenas'),
('Quepos','Puntarenas');

INSERT INTO schema_inventory.Sede(Canton,nombre) VALUES
('Liberia','Liberia'),
('Nicoya','Nicoya'),
('Coto Brus','Coto'),
('Heredia','Campus Benjamín Núñez'),
('Heredia','Omar Dengo'),
('Sarapiqui','Sarapiqui'),
('Alajuela','Interuniversitaria');

INSERT INTO schema_inventory.TipoUsuario(nombre) VALUES
('Admnstrcn'),
('Scrtr'),
('Jefe'),
('Registrador'),
('DirectorSede');

INSERT INTO schema_inventory.Usuario(cedula,nombre,apellido1,apellido2,telefono,email,nacimiento,TipoUsuario,Sede)
 VALUES
 ('Admnstrcn','Admnstrcn','Admnstrcn','Admnstrcn','Admnstrcn','Admnstrcn@Admnstrcn.com',NOW(),'Admnstrcn','Omar Dengo'),
 ('Scrtr','Scrtr','Scrtr','Scrtr','Scrtr','Scrtr@Scrtr.com',NOW(),'Scrtr','Omar Dengo'),
 ('Jefe','Jefe','Jefe','Jefe','Jefe','Jefe@Jefe.com',NOW(),'Jefe','Omar Dengo'),
 ('Registrador','Registrador','Registrador','Registrador','Registrador','Registrador@Registrador.com',NOW(),'Registrador','Omar Dengo'),
 ('DirectorSede','DirectorSede','DirectorSede','DirectorSede','DirectorSede','DirectorSede@DirectorSede.com',NOW(),'DirectorSede','Omar Dengo'),
 ('AdmnstrcnCoto','Admnstrcn','Admnstrcn','Admnstrcn','Admnstrcn','Admnstrcn@Admnstrcn.com',NOW(),'Admnstrcn','Coto'),
 ('ScrtrCoto','Scrtr','Scrtr','Scrtr','Scrtr','Scrtr@Scrtr.com',NOW(),'Scrtr','Coto'),
 ('JefeCoto','Jefe','Jefe','Jefe','Jefe','Jefe@Jefe.com',NOW(),'Jefe','Coto'),
 ('RegistradorCoto','Registrador','Registrador','Registrador','Registrador','Registrador@Registrador.com',NOW(),'Registrador','Coto'),
 ('DirectorSedeCoto','DirectorSede','DirectorSede','DirectorSede','DirectorSede','DrctrSd@Drctr.com',NOW(),'DirectorSede','Coto');

INSERT INTO schema_inventory.Estado(nombre) VALUES
('recivido'),
('cancelado'),
('por verificar'),
('rechazada'),
('por rotular'),
('procesada');

INSERT INTO schema_inventory.Color(nombre) VALUES
('Verde'),
('Amarillo'),
('Rojo'),
('Azul'),
('Negro'),
('Blanco'),
('Celeste'),
('Cafe'),
('Magenta'),
('Marron'),
('Violeta'),
('Naranja');

INSERT INTO schema_inventory.EstadoAutorizacion(nombre) VALUES
('Por Definir'),
('Aceptada'),
('Rechazada');


ALTER TABLE schema_inventory.Autorizacion ALTER COLUMN EstadoAutorizacion SET DEFAULT 'Por Definir';
ALTER TABLE schema_inventory.Solicitud ALTER COLUMN estado SET DEFAULT 'recivido';
ALTER TABLE schema_inventory.Inventario ALTER COLUMN approved SET DEFAULT 'f';

INSERT INTO schema_inventory.TipoAdquisicion(nombre) VALUES
('Donación'),
('Compra'),
('Producción');

INSERT INTO schema_inventory.Tipo(nombre) VALUES
('Edificio'),
('Componente Electronico'),
('Limpieza'),
('Aire Acondicionado'),
('Articulo de Agricultura'),
('Articulo de Laboratorio');

INSERT INTO schema_inventory.Solicitud(TipoAdquisicion,adquisicion,solicitante) VALUES('Donación',NOW(),'DirectorSedeCoto');
INSERT INTO schema_inventory.Solicitud(TipoAdquisicion,adquisicion,solicitante) VALUES('Compra',NOW(),'DirectorSede');
INSERT INTO schema_inventory.Solicitud(TipoAdquisicion,adquisicion,solicitante) VALUES('Producción',NOW(),'DirectorSedeCoto');

INSERT INTO schema_inventory.Inventario(Solicitud,Tipo,marca,modelo,NoSerial) VALUES(1,'Limpieza','patito','patito5','seral123');
INSERT INTO schema_inventory.Inventario(Solicitud,Tipo,marca,modelo,NoSerial) VALUES(1,'Articulo de Agricultura','Tupper','Pala','seral1234');
